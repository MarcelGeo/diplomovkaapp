#!/bin/sh
cd ../v3.4.0/v3.4.0/
python closure-library/closure/bin/build/closurebuilder.py --root=closure-library/ --root=ol/ --root=app --root=ol.ext --root=appUrl --root=api --namespace=app --output_mode=compiled --compiler_jar=closure-library/closure/bin/build/compiler-latest/compiler.jar --compiler_flags="--compilation_level=SIMPLE_OPTIMIZATIONS" --compiler_flags="--externs=proj4.js" > build/appcompiledsimple.js 
