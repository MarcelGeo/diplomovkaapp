	goog.provide('appUrl');
	goog.require('goog.string');
	goog.require('goog.Uri');
	/**
	Uri returner with proxy url.
	@constructor
	*/
	var appUrl = function(url){
		this.defaultWps = "http://gis.fns.uniba.sk/geoserver/wps";
		this.url = goog.string.isEmptySafe(url) && this.defaultWps || url;
	    this.uri = new goog.Uri(this.url); // uri added with user or default
		this.proxyUri = new goog.Uri("../proxy.php");
		/**
		Key value pairs with users:password
		*/
		this.users = {
			"wps_ez": "wps_ez",
			"wps_vector": "wps_vector"
		}
		this.auth = false;
	}
	/**
	Create url from app.uri and appUrl.proxyUri with query string in app.uri. If first and second argument is undefined, don't set query to uri, to proxy set only uri. 
	@param keys {array|undefined} Keys for query string in user defined url
	@param values {array|undefined} Values for query string in user defined url
	@param route {string|undefined} Optional key for proxy url query string
	*/
	appUrl.prototype.proxySet = function(keys, values, route){
		route = route ? route+"=" : ""; // define route key to proxy url
		var query = keys && values ? goog.Uri.QueryData.createFromKeysValues(keys, values) : undefined;
		var proxyQuery = new goog.Uri.QueryData(route + this.uri.setQueryData(query).toString());
		this.proxyUri.setQuery(proxyQuery);
		return this;
	}
	appUrl.prototype.setUserAuth = function(username){
		var user = this.users[username];
		if(user){
			this.auth = {"Authorization": "Basic " + btoa(username + ":" + user)};
		}
	}