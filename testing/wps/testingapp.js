goog.provide('testingapp');
goog.require('goog.events');
goog.require('goog.events.EventType');
goog.require('goog.net.XhrIo');
goog.require('ol.format.WPS');
goog.require('api.goog_');

testingapp.txts = ["text.txt"];
testingapp.xmls = ["boundingbox.xml", "62_wpsExecute_response_reference.xml", "62_wpsExecute_response_wkt.xml", "62_wpsExecute_response_geojson.xml", "62_wpsExecute_response_text.xml", "testgml.gml"]
testingapp.jsons = ["body.geojson"]
testingapp.wps = new ol.format.WPS()

for (var i = 0; i < testingapp.xmls.length; i++){
	var execute = new goog.net.XhrIo();
	goog.events.listen(execute, goog.net.EventType.COMPLETE, function(e){
				var reqq = e.target;
				if(reqq.isSuccess()){
					var data = reqq.getResponseXml();
					console.log(data)
					var result = this.wps.readExecuteResponse(data);
					console.log(result);
				}else{
					console.error("It was not success")
				}
	}, false, testingapp);
	console.log(testingapp.xmls[i]);
	execute.send(testingapp.xmls[i]);
}
for (var i = 0; i < testingapp.jsons.length; i++){
	var execute = new goog.net.XhrIo();
	goog.events.listen(execute, goog.net.EventType.COMPLETE, function(e){
				var reqq = e.target;
				if(reqq.isSuccess()){
					var data = reqq.getResponseJson();
					console.log(data)
					var result = this.wps.readExecuteResponse(data);
					console.log(result);
				}else{
					console.error("It was not success")
				}
	}, false, testingapp);
	console.log(testingapp.jsons[i]);
	execute.send(testingapp.jsons[i]);
}
for (var i = 0; i < testingapp.txts.length; i++){
	var execute = new goog.net.XhrIo();
	goog.events.listen(execute, goog.net.EventType.COMPLETE, function(e){
				var reqq = e.target;
				if(reqq.isSuccess()){
					var data = reqq.getResponseText();
					console.log(data)
					var result = this.wps.readExecuteResponse(data);
					console.log(result);
				}else{
					console.error("It was not success")
				}
	}, false, testingapp);
	console.log(testingapp.txts[i]);
	execute.send(testingapp.txts[i]);
}