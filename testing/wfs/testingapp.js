goog.provide('testingapp');
goog.require('goog.events');
goog.require('goog.events.EventType');
goog.require('goog.net.XhrIo');
goog.require('ol.format.WPS');
goog.require('api.goog_');
goog.require('api.customs');
goog.require('api.dynamic');
goog.require('api.dynamicTypes');
goog.require('api.htmlblock');
goog.require('ol.format.WFSCapabilities');
goog.require('ol.format.WFS');
goog.require('goog.dom');


testingapp.txts = [];
testingapp.xmls = ['geoserverWFSGetCapabilities.xml']
testingapp.jsons = []

testingapp.olWFS = new ol.format.WFS();
testingapp.wfs = new ol.format.WFSCapabilities({
	featureNamespaces: ["http://gis.fns.uniba.sk/geoserver/10sykora"]
});
testingapp.wps = new ol.format.WPS();

testingapp.test = testingapp.wps.writeExecute({"objectExecute": {
Identifier: "JTS:buffer",
ComplexData: {
 geom:{
 MimeType: "text/xml; subtype=wfs-collection/1.1.0",
 value: null
 },
 geom2:{
 MimeType: "text/xml; subtype=wfs-collection/1.1.0",
 value: null
 }
},
LiteralData: {
 distance:{
 DataType: "xs:double",
 value: 1000
 },
 quadrantSegments:{
 DataType: "xs:int",
 value: 3
 }
},
ComplexOutput: {
 result:{MimeType: "text/xml; subtype=gml/3.1.1"},
 result2: {MimeType: "text/xml; subtype=gml/3.1.1"}
 },
LiteralOutput: {result:{uom: "text/xml; subtype=gml/3.1.1"}}
}, "ResponseDocument": true, "ResponseDocumentAttributes": {status: "true"}})


for (var i = 0; i < testingapp.xmls.length; i++){
	var execute = new goog.net.XhrIo();
	goog.events.listen(execute, goog.net.EventType.COMPLETE, function(e){
				var reqq = e.target;
				if(reqq.isSuccess()){
					var data = reqq.getResponseXml();
					console.log(data)
					var result = this.wfs.read(data);
					console.log(result);
				}else{
					console.error("It was not success")
				}
	}, false, testingapp);
	console.log(testingapp.xmls[i]);
	execute.send(testingapp.xmls[i]);
}
for (var i = 0; i < testingapp.jsons.length; i++){
	var execute = new goog.net.XhrIo();
	goog.events.listen(execute, goog.net.EventType.COMPLETE, function(e){
				var reqq = e.target;
				if(reqq.isSuccess()){
					var data = reqq.getResponseJson();
					console.log(data)
					var result = this.wps.readExecuteResponse(data);
					console.log(result);
				}else{
					console.error("It was not success")
				}
	}, false, testingapp);
	console.log(testingapp.jsons[i]);
	execute.send(testingapp.jsons[i]);
}
for (var i = 0; i < testingapp.txts.length; i++){
	var execute = new goog.net.XhrIo();
	goog.events.listen(execute, goog.net.EventType.COMPLETE, function(e){
				var reqq = e.target;
				if(reqq.isSuccess()){
					var data = reqq.getResponseText();
					console.log(data)
					var result = this.wps.readExecuteResponse(data);
					console.log(result);
				}else{
					console.error("It was not success")
				}
	}, false, testingapp);
	console.log(testingapp.txts[i]);
	execute.send(testingapp.txts[i]);
}

testingapp.WFSconnections = ['geoserverWFSGetCapabilities.xml'];

testingapp.additionalWFS = new api.dynamic();

goog.events.listen(testingapp.additionalWFS, api.dynamicTypes.PUSH, function(e){
		var item = e.element;
		console.log(item);
		var selects = this.selectorContainerWFS.arr;
		for(var i = 0, ii = selects.length; i < ii; i++){
			var option = goog.dom.createDom("option", {'value': this.additionalWFS.arr.length - 1}, goog.dom.createTextNode(item["title"]));
			goog.dom.appendChild(selects[i].element, option);
		}
	}, 
false, testingapp);

goog.events.listen(testingapp.additionalWFS, api.dynamicTypes.REMOVE, function(e){
		
	}, 
false, testingapp);

testingapp.selectWFS = new api.htmlblock(goog.dom.createDom("select", {'class': 'selecter'}, goog.dom.createDom('option', {'value': 'Select WFS layer'})), testingapp.additionalWFS,
	null, 
	function(){
		
	},	
	function(){
		
	},
	function(){
		
	}
);

testingapp.selectWFS_two = new api.htmlblock(goog.dom.createDom("select", {'class': 'selecter'}, goog.dom.createDom('option', {'value': 'Select WFS layer'})), testingapp.additionalWFS,
	null, 
	function(){
		
	},	
	function(){
		
	},
	function(){
		
	}
);


testingapp.selectorContainerWFS = new api.dynamic();
testingapp.selectorContainerWFS.push(testingapp.selectWFS);
goog.dom.appendChild(document.querySelector('div#container'), testingapp.selectWFS.element);
testingapp.selectorContainerWFS.push(testingapp.selectWFS_two);
goog.dom.appendChild(document.querySelector('div#container'), testingapp.selectWFS_two.element);




testingapp.getFeaturesFromWFSCapabilities = function(capabilityObject){
	ftypelist = capabilityObject['FeatureTypeList'];
	for(var j = 0, jj = ftypelist.length; j < jj; j++){
		var capability = ftypelist[j];
		var ftype = capability["Name"];
		var ftitle = capability["Title"] || ftype;
		var splitted = ftype.split(':');
		var name = splitted.length > 1 ? splitted[1] : splitted[0];
		var getfeature = this.olWFS.writeGetFeature({
			featureNS: "http://gis.fns.uniba.sk/geoserver/10sykora",
			featurePrefix: "sykora10",
			featureTypes: [name],
			srsName: "EPSG:3857"
		});
		if(getfeature){
			var execute = new goog.net.XhrIo();
			goog.events.listen(execute, goog.net.EventType.COMPLETE, function(e){
				var reqq = e.target;
				if(reqq.isSuccess()){
					var data = reqq.getResponseXml();
					if(data){
						this["wfsdata"] = data;
						testingapp.additionalWFS.push(this);
					}
				}else{
					console.error("It was not success");
				}
			}, false, {'title': ftitle});
			execute.send("http://localhost/wpsapp/dev/proxy.php?http://gis.fns.uniba.sk/geoserver/sykora10/wfs", "POST", api.customs.xmlToString(getfeature), {"Content-type": "text/xml; charset=UTF-8;"});
		}
		// console.log(getfeature);
	}
}

testingapp.writeAdditionalWFS = function(){
	var conns = this.WFSconnections;
	for(var i = 0, ii = conns.length; i < ii; i++){
		var execute = new goog.net.XhrIo();
		goog.events.listen(execute, goog.net.EventType.COMPLETE, function(e){
			var reqq = e.target;
			if(reqq.isSuccess()){
				var data = reqq.getResponseXml();
				console.log(data)
				var source = this.wfs.read(data);
				console.log(source);
				if(source){
					this.getFeaturesFromWFSCapabilities(source);
				}
			}else{
				console.error("It was not success");
			}
		}, false, testingapp);
		execute.send(conns[i]);
	}
	
}

testingapp.writeAdditionalWFS();