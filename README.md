
Web processing services map client for environmental health

The purpose of this web mapping application is to provide access to selected analyses online. It is designed to execute computational processes published by WPS map service; besides, it enables to import and export your own data and create geometries as inputs into the processes. Moreover, not only the built-in WPS processes, but also processes from any WPS server can be executed in the application.
Authors

Marcel Kočíšek, Richard Feciskanin

Department of Cartography, Geoinformatics and Remote Sensing
