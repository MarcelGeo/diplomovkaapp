{"type":"FeatureCollection",
 "features":[
{"type":"Feature","properties":{"Madari":20, "Slovaci": 50},"geometry":{"type":"Point","coordinates":[17.44656794714337,48.457516689235554]},"crs":{"type":"name","properties":{"name":"urn:ogc:def:crs:OGC:1.3:CRS84"}}},
{"type":"Feature","properties":{"Madari":10, "Slovaci": 100},"geometry":{"type":"Point","coordinates":[17.442018920654284,48.45233663366353]},"crs":{"type":"name","properties":{"name":"urn:ogc:def:crs:OGC:1.3:CRS84"}}},
{"type":"Feature","properties":{"Madari":5, "Slovaci": 200},"geometry":{"type":"Point","coordinates":[17.419016296142537,48.494672354859155]},"crs":{"type":"name","properties":{"name":"urn:ogc:def:crs:OGC:1.3:CRS84"}}},
{"type":"Feature","properties":{"Madari":0, "Slovaci": 50},"geometry":{"type":"Point","coordinates":[17.397730285400446,48.48261254912143]},"crs":{"type":"name","properties":{"name":"urn:ogc:def:crs:OGC:1.3:CRS84"}}},
{"type":"Feature","properties":{"Madari":40, "Slovaci": 10},"geometry":{"type":"Point","coordinates":[17.40270846533217,48.468387394824134]},"crs":{"type":"name","properties":{"name":"urn:ogc:def:crs:OGC:1.3:CRS84"}}},
{"type":"Feature","properties":{"Madari":20, "Slovaci": 5},"geometry":{"type":"Point","coordinates":[17.49420397924835,48.465541885338986]},"crs":{"type":"name","properties":{"name":"urn:ogc:def:crs:OGC:1.3:CRS84"}}}
 ]
}