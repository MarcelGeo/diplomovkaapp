goog.provide('ol.format.WPS');
goog.require('ol.format.XML');
goog.require('ol.xml');
goog.require('ol.format.XSD');
goog.require('ol.format.GML');
goog.require('ol.format.WFS');
goog.require('ol.format.GeoJSON');
goog.require('ol.format.WKT');
goog.require('ol.format.Ows');
goog.require('goog.asserts');
goog.require('goog.object');
goog.require('goog.array');
goog.require('goog.dom');
goog.require('goog.dom.xml');
goog.require('goog.dom.NodeType');
goog.require('api.goog_');
	/**
	@consructor
	@extends {ol.format.XML}
	*/
	ol.format.WPS = function(){ 
		// --for jquery parsers not now this.url = options.url ? options.url : null;
		goog.base(this);
		this.process = null;
		this.desc_process = null;
	} 
	goog.inherits(ol.format.WPS, ol.format.XML);
	ol.format.WPS.EX_BBOXCONST = new ol.format.Ows(); //  --new constructor for write and read bbox data
		  // namespaces -----------------------------------------------------------------------
		  ol.format.WPS.GMLNS = [null, 'http://www.opengis.net/gml'];
		   ol.format.WPS.NAMESPACE_URIS_ = [null,'http://www.opengis.net/wps/1.0.0', 'http://www.opengeospatial.net/wps'];
		  ol.format.WPS.NAMESPACE_URIS_OWS_ = [null, 'http://www.opengis.net/ows/1.1', "http://www.opengis.net/ows", 'http://www.opengeospatial.net/ows'];
		  ol.format.WPS.NAMESPACE_URIS_ALL_ = [null, 'http://www.opengis.net/ows/1.1', "http://www.opengis.net/ows", 'http://www.opengeospatial.net/ows', 'http://www.opengis.net/wps/1.0.0', 'http://www.opengeospatial.net/wps'];
		  ol.format.WPS.NAMESPACE_NOTHING_ = [null];
		  
		  // get capabilities parser
		  ol.format.WPS.describeObj = { // object for parsing identifier... of som process, inputs...
		   'Identifier': ol.xml.makeObjectPropertySetter(ol.format.XSD.readString),
			'Title': ol.xml.makeObjectPropertySetter(ol.format.XSD.readString),
			'Abstract': ol.xml.makeObjectPropertySetter(ol.format.XSD.readString)
			}
		  // proces capabilities format ol --------------------------------------------------------------------
		    ol.format.WPS.PROCESS_DESC_formatS_ = ol.xml.makeParsersNS( // proces offerings format
			ol.format.WPS.NAMESPACE_URIS_OWS_, ol.format.WPS.describeObj);    
		ol.format.WPS.parseProcess = function(node, objectStack){
		    return ol.xml.pushParseAndPop({},
			     ol.format.WPS.PROCESS_DESC_formatS_, node, objectStack);
		   }  
		  ol.format.WPS.PROCESS_formatS_ = ol.xml.makeParsersNS( // proces offerings format
			ol.format.WPS.NAMESPACE_URIS_, {
			'Process': ol.xml.makeArrayPusher(ol.format.WPS.parseProcess)
			});
		ol.format.WPS.readProces = function(node, objectStack){
		   return ol.xml.pushParseAndPop([],
			    ol.format.WPS.PROCESS_formatS_ , node, objectStack);
		}
		   ol.format.WPS.PROCESSOFF_formatS_ = ol.xml.makeParsersNS( // proces offerings parser
			   ol.format.WPS.NAMESPACE_URIS_, {
			   'ProcessOfferings': ol.xml.makeObjectPropertySetter(ol.format.WPS.readProces)
			}); 
			// --section --name describe process parser ol -----------------------------------------------------------------------------
                   // allowed values      
	   ol.format.WPS.ALLOWED = ol.xml.makeParsersNS( // proces input format - allowed
			ol.format.WPS.NAMESPACE_URIS_OWS_, {
			'Value': ol.xml.makeArrayPusher(ol.format.XSD.readValue) 			
			});
		
		ol.format.WPS.readAllowed = function(node, objectStack){
		    return ol.xml.pushParseAndPop([], ol.format.WPS.ALLOWED, node, objectStack)
		}
		            // data type
		ol.format.WPS.readDataType = function(node, objectStack){
		    var topush = node.getAttribute('ows:reference') && {'reference':node.getAttribute('ows:reference'), 'format': ol.format.XSD.readString(node)} || ol.format.XSD.readString(node);
			return topush;
			}
		ol.format.WPS.LiteralDataDESC  = ol.xml.makeParsersNS( // proces input parser - Literal data
			ol.format.WPS.NAMESPACE_URIS_OWS_, { // TREBA DOROBIT UOMS!!!!!!!!!!!!!!!
			'DataType': ol.xml.makeObjectPropertySetter(ol.format.WPS.readDataType),
			'AnyValue': ol.xml.makeObjectPropertySetter(ol.format.XSD.trueReturner),
			'AllowedValues': ol.xml.makeObjectPropertySetter(ol.format.WPS.readAllowed),
            'DefaultValue': ol.xml.makeObjectPropertySetter(ol.format.XSD.readValue) 			
			});
           // complex Data	
        ol.format.WPS.COMPLEXDATA_FORMAT_READER = ol.xml.makeParsersNS( // proces input format - Complex Data - Format - MimeType
			ol.format.WPS.NAMESPACE_NOTHING_, {
			'MimeType': ol.xml.makeObjectPropertySetter(ol.format.XSD.readString),
            'Encoding': ol.xml.makeObjectPropertySetter(ol.format.XSD.readString),
            'Schema': ol.xml.makeObjectPropertySetter(ol.format.XSD.readString)			
			});
	   ol.format.WPS.readFormatCapabilities = function(node, objectStack){
	         return ol.xml.pushParseAndPop({}, ol.format.WPS.COMPLEXDATA_FORMAT_READER, node, objectStack);   
	   }	
	        // Bbox complex data
	   ol.format.WPS.readCRSs = function(node, objectStack){
	       var crs = ol.xml.makeParsersNS( // proces input parser - Complex Data - Format
			ol.format.WPS.NAMESPACE_NOTHING_, {
            'CRS': ol.xml.makeObjectPropertySetter(ol.format.XSD.readString)		
			});
          return ol.xml.pushParseAndPop({}, crs, node, objectStack);   			
	   }
       ol.format.WPS.COMPLEXDATA_FORMAT = ol.xml.makeParsersNS( // proces input parser - Complex Data - Format
			ol.format.WPS.NAMESPACE_NOTHING_, {
			'Format': ol.xml.makeArrayPusher(ol.format.WPS.readFormatCapabilities),
            'CRS': ol.xml.makeArrayPusher(ol.format.XSD.readString),
            'CRSsType': ol.xml.makeArrayPusher(ol.format.WPS.readCRSs)			
			});
       ol.format.WPS.formatReader = function(node, objectStack){
	      return ol.xml.pushParseAndPop([], ol.format.WPS.COMPLEXDATA_FORMAT, node, objectStack); 
	   }			
	   ol.format.WPS.COMPLEXDATA = ol.xml.makeParsersNS( // proces input parser - Complex Data
			 ol.format.WPS.NAMESPACE_URIS_OWS_, {
			'Default': ol.xml.makeObjectPropertySetter(ol.format.WPS.formatReader),
            'Supported': ol.xml.makeObjectPropertySetter(ol.format.WPS.formatReader) 			
			});  
            // input description			
	   ol.format.WPS.readComplexBBOXData = function(node, objectStack){
		   return ol.xml.pushParseAndPop({}, ol.format.WPS.COMPLEXDATA, node, objectStack);   
		}			
		ol.format.WPS.readLiteralData = function(node, objectStack){
		     return ol.xml.pushParseAndPop({}, ol.format.WPS.LiteralDataDESC, node, objectStack);
		}
		ol.format.WPS.readREFERENCE = function(node, objectStack){
			var attrs = node.attributes;
			var returner = {};
			for(var i = 0, ii = attrs.length; i < ii; i++) {
				returner[attrs[i].name] = attrs[i].value;
			}
			return returner;
		}
		ol.format.WPS.INPUT_OUTPUT_DESC_formatS_ = ol.xml.makeParsersNS( // proces input format
			ol.format.WPS.NAMESPACE_URIS_ALL_,{
			'Reference': ol.xml.makeObjectPropertySetter(ol.format.WPS.readREFERENCE),
			  'Identifier': ol.xml.makeObjectPropertySetter(ol.format.XSD.readString),
			'Title': ol.xml.makeObjectPropertySetter(ol.format.XSD.readString),
			'Abstract': ol.xml.makeObjectPropertySetter(ol.format.XSD.readString),
			'LiteralData': ol.xml.makeObjectPropertySetter(ol.format.WPS.readLiteralData),
			'ComplexData': ol.xml.makeObjectPropertySetter(ol.format.WPS.readComplexBBOXData),
			'BoundingBoxData': ol.xml.makeObjectPropertySetter(ol.format.WPS.readComplexBBOXData),
			'ComplexOutput': ol.xml.makeObjectPropertySetter(ol.format.WPS.readComplexBBOXData),
			'BoundingBoxOutput': ol.xml.makeObjectPropertySetter(ol.format.WPS.readComplexBBOXData),
            'LiteralOutput': ol.xml.makeObjectPropertySetter(ol.format.WPS.readLiteralData)			
			});    
		ol.format.WPS.readInputDescription = function(node, objectStack){
		    return ol.xml.pushParseAndPop({'max':node.getAttribute('maxOccurs') || null, 
			                           'min':node.getAttribute('minOccurs') || null}, ol.format.WPS.INPUT_OUTPUT_DESC_formatS_, node, objectStack);	
		}   
		ol.format.WPS.readOutputDescription = function(node, objectStack){ // output description
		     return ol.xml.pushParseAndPop({}, ol.format.WPS.INPUT_OUTPUT_DESC_formatS_, node, objectStack);
		}
		ol.format.WPS.INPUTS_OUTPUTS_format_ = ol.xml.makeParsersNS( //  make object in array of inputs
			ol.format.WPS.NAMESPACE_URIS_, {
			'Input': ol.xml.makeArrayPusher(ol.format.WPS.readInputDescription),
			'Output': ol.xml.makeArrayPusher(ol.format.WPS.readOutputDescription) 
			});    		
        ol.format.WPS.readInputsOutputs = function(node, objectStack){
		    return ol.xml.pushParseAndPop([], ol.format.WPS.INPUTS_OUTPUTS_format_, node, objectStack);
		}
		ol.format.WPS.DESCPROCES_DESC_formatS_ = ol.xml.makeParsersNS( // proces dexription format
			ol.format.WPS.NAMESPACE_URIS_OWS_, {
			'Identifier': ol.xml.makeObjectPropertySetter(ol.format.XSD.readString),
			'Title': ol.xml.makeObjectPropertySetter(ol.format.XSD.readString),
			'Abstract': ol.xml.makeObjectPropertySetter(ol.format.XSD.readString),
			'DataInputs': ol.xml.makeObjectPropertySetter(ol.format.WPS.readInputsOutputs),
			'ProcessOutputs': ol.xml.makeObjectPropertySetter(ol.format.WPS.readInputsOutputs)
			});    
		ol.format.WPS.parseDescription = function(node, objectStack){
		    return ol.xml.pushParseAndPop({},
			     ol.format.WPS.DESCPROCES_DESC_formatS_, node, objectStack);
		   }  
		   ol.format.WPS.PROCESDESC_formatS_ = ol.xml.makeParsersNS( // make item in array on each proces description
			   ol.format.WPS.NAMESPACE_URIS_, {
			   'ProcessDescription': ol.xml.makeObjectPropertyPusher(ol.format.WPS.parseDescription, 'ProcessDescriptions')
			});
			//-----------------end of formats -----------------------------------
		 	ol.format.WPS.readFromNode = {
			"getCapabilities":function(node, objectStack){
			       return ol.xml.pushParseAndPop({},
			         ol.format.WPS.PROCESSOFF_formatS_, node, objectStack);						 
			},
			"describeProcess":function(node, objectStack){
			      if(node.firstChild.localName === "ProcessDescription"){ 
				   return ol.xml.pushParseAndPop({'ProcessDescriptions':[]},
			         ol.format.WPS.PROCESDESC_formatS_, node, objectStack);		
				  }else{
			      	return ol.format.WPS.parseDescription(node, objectStack);	
                    }					 
			}
		}
		/**
		DEPRACTED BY PRODUCTION.
		*/
		
		/** Parse getCapabilities to JSON.
		* @param {Object} options:
		* doc (Document): xml doc
		* node (Node): node with capabilities
		* @notypecheck
		*/
        ol.format.WPS.prototype.readGetCapabilities = function(options){
			goog.asserts.assertObject(options);
			if(goog.isDef(options.doc)){
			    this.process = ol.format.WPS.readFromNode["getCapabilities"](options.doc.firstChild, []); 
			}
			if(goog.isDef(options.node)){
			    this.process = ol.format.WPS.readFromNode["getCapabilities"](options.node, []); 
			}
			return this.process;
		}; 		
	    /**
		Read describe process wps response and convert it to object.
		@param {Object} options: 
		doc(Document|undefined)
		node(Node|undefined)
		oneprocessoutput {boolean=} if I have one process output, I wanna return only first object with output, not all object)
		@return Describe proces object
		*/
		ol.format.WPS.prototype.readDescribeProcess = function(options){
			goog.asserts.assertObject(options);
			var oneprocessoutput = options["oneprocessoutput"] == null ? true : false;
			if(goog.isDef(options.doc)){
			    this.desc_process = ol.format.WPS.readFromNode["describeProcess"](options.doc.firstChild, []); 
			}
			else if(goog.isDef(options.node)){
				this.desc_process = ol.format.WPS.readFromNode["describeProcess"](options.node, []);
			}else{
			    return false;
			}
			if(this.desc_process["ProcessDescriptions"] && this.desc_process["ProcessDescriptions"].length < 2 && oneprocessoutput){
				return this.desc_process["ProcessDescriptions"][0];
			}else{
				return this.desc_process;
			}
		}
		//{ --section --name Output parsers
		ol.format.WPS.EXTERNSBBOX = function(node, objectStack){
			
			var bbox = ol.format.WPS.EX_BBOXCONST.readBoundingBox(node);
			return goog.array.join(bbox["LowerCorner"], bbox["UpperCorner"]);
		}
		ol.format.WPS.readLiteralData_reponse = function(node, objectStack){
			var value = ol.format.XSD.readString(node) || null;
			return{
				"value": value,
				"dataType": node.getAttribute("dataType") || null,
				"uom": node.getAttribute("uom") || null
			}
		}
		
		ol.format.WPS.readComplexData_response = function(node, objectStack){
			var formats = objectStack[0], matchedtext, text, textoutput;
			var myRegexp = /<!\[CDATA\[(.*)]]>/;
			var firstChild = node.firstChild;
			var defattrs = {
				"mimeType": node.getAttribute("mimeType"),
				"encoding": node.getAttribute("encoding"),
				"schema": node.getAttribute("schema")
			}
			var externsdata = formats["prefixParser"](firstChild);
			if(externsdata){
				defattrs["value"] = externsdata;
				return defattrs;
			}
			if(firstChild.nodeType == goog.dom.NodeType.CDATA_SECTION) {
				text = firstChild.nodeValue;
				var match = myRegexp.exec(text);
				if(match){ // no CDATA
					matchedtext = match[1];
				}else{
					matchedtext = text;
				}
			}else if(firstChild.nodeType == goog.dom.NodeType.TEXT){
				matchedtext = firstChild.nodeValue;
			}else{
				defattrs["value"] = null;
			}
			var geojson = api.goog_.trygetResponseJson(matchedtext); // parse json or wkt?
			if(geojson){
				defattrs["value"] = formats["geojson"](geojson);
			}else{
				try{
					defattrs["value"] = formats["wkt"](matchedtext);
				}catch(e){
					defattrs["value"] = matchedtext;
				}
			}
			return defattrs; // features, bbox, null
		}
		
		ol.format.WPS.EXTERNSDATA = ol.xml.makeParsersNS( // make item in array on each proces description
			   ol.format.WPS.NAMESPACE_URIS_, {
			   'BoundingBoxData': ol.xml.makeObjectPropertySetter(ol.format.WPS.EXTERNSBBOX),
			   'LiteralData': ol.xml.makeObjectPropertySetter(ol.format.WPS.readLiteralData_reponse),
			   'ComplexData': ol.xml.makeObjectPropertySetter(ol.format.WPS.readComplexData_response)
			});
		ol.format.WPS.readDATA = function(node, objectStack){ // read data from CDATA and parse object from string, or parse response in node.
			
			if(ol.xml.isNode(node.firstChild)){ // wps, wfs, gml ...
				var externs = ol.xml.pushParseAndPop({}, ol.format.WPS.EXTERNSDATA, node, objectStack);
				if(externs){
					if(!goog.object.isEmpty(externs)){
						return externs;
					}else{
						return null;
					}
				}
			}else{
				return null;
			}
		}
		ol.format.WPS.ProcessPAUSEDSTARTED = function(node, objectStack){
			var percent = node.getAttribute('percentCompleted') || null;
			var status = ol.format.XSD.readString(node);
			return {"parcentCompleted": percent, "status": status};
		}

		ol.format.WPS.ProcessFailed = function(node, objectStack){
			return ol.format.WPS.EX_BBOXCONST.readException({
				node: node.firstChild
			});
		}

		ol.format.WPS.STATUSS = ol.xml.makeParsersNS( // make item in array on each proces description
			   ol.format.WPS.NAMESPACE_URIS_, {
			   'ProcessAccepted': ol.xml.makeObjectPropertySetter(ol.format.XSD.readString),
			   'ProcessStarted': ol.xml.makeObjectPropertySetter(ol.format.WPS.ProcessPAUSEDSTARTED),
			   'ProcessSucceeded': ol.xml.makeObjectPropertySetter(ol.format.XSD.readString),
			   'ProcessPaused': ol.xml.makeObjectPropertySetter(ol.format.WPS.ProcessPAUSEDSTARTED),
			   'ProcessFailed': ol.xml.makeObjectPropertySetter(ol.format.WPS.ProcessFailed)
			});
		ol.format.WPS.OUTPUT_ = ol.xml.makeParsersNS( // make item in array on each proces description
			   ol.format.WPS.NAMESPACE_URIS_ALL_, {
			   'Identifier': ol.xml.makeObjectPropertySetter(ol.format.XSD.readString),
			   'Title': ol.xml.makeObjectPropertySetter(ol.format.XSD.readString),
			   'Abstract': ol.xml.makeObjectPropertySetter(ol.format.XSD.readString),
			   'Data': ol.xml.makeObjectPropertySetter(ol.format.WPS.readDATA),
			   'Reference': ol.xml.makeObjectPropertySetter(ol.format.WPS.readREFERENCE)
			});
		ol.format.XSD.parseOutput = function(node, objectStack){
			return ol.xml.pushParseAndPop({}, ol.format.WPS.OUTPUT_, node, objectStack);	
		}
		ol.format.WPS.OUTPUTS = ol.xml.makeParsersNS( // make item in array on each proces description
			   ol.format.WPS.NAMESPACE_URIS_, {
			   'Output': ol.xml.makeArrayPusher(ol.format.XSD.parseOutput)
			});
		ol.format.WPS.parseStatus = function(node, objectStack){
		    return ol.xml.pushParseAndPop({'creationTime': node.getAttribute('creationTime') || null}, ol.format.WPS.STATUSS , node, objectStack);
		}
		ol.format.WPS.parseProcessOutputs = function(node, objectStack){
			return ol.xml.pushParseAndPop([], ol.format.WPS.OUTPUTS, node, objectStack);
		}
		ol.format.WPS.EXECUTEREPONSE_ = ol.xml.makeParsersNS( // make item in array on each proces description
			   ol.format.WPS.NAMESPACE_URIS_, {
			   'Process': ol.xml.makeObjectPropertySetter(ol.format.WPS.parseProcess),
			   'Status': ol.xml.makeObjectPropertySetter(ol.format.WPS.parseStatus),
			   'ProcessOutputs': ol.xml.makeObjectPropertySetter(ol.format.WPS.parseProcessOutputs),
			   'DataInputs': ol.xml.makeObjectPropertySetter(ol.format.WPS.readInputsOutputs)
			});
		ol.format.WPS.readExecuteResponse = function(node, formatOutputObject){
			if(node.localName === "ExecuteResponse"){
				return ol.xml.pushParseAndPop({'statusLocation': node.getAttribute('statusLocation') || null},
					ol.format.WPS.EXECUTEREPONSE_, node, [formatOutputObject]);
			}else{
				return null;
			}
		}
		 
		ol.format.WPS.Supported = {
			"wps": function(executeResponse){
				return ol.format.WPS.readExecuteResponse(executeResponse, ol.format.WPS.Supported);
			},
			"gml": function(gmlGeometry){
				var format = ol.format.WPS.mainConstrunctors["gmlFormat"]; // gml
				if(gmlGeometry.localName === "featureMembers"){
					return format.readFeatures(gmlGeometry, {
						dataProjection: "EPSG:4326",
						featureProjection: "EPSG:3857"
					});
				}
				/**
				Create geometry node and push geometries nodes to this node. Read geometry from created geometry node.
				@param gmlGeometry {node} geometry to parse
				@return ol.Feature
				*/
				var geometryElement = ol.xml.createElementNS("http://www.opengis.net/gml", "geometry");
				gmlGeometry.setAttribute("srsName", "http://www.opengis.net/gml/srs/epsg.xml#4326");
				geometryElement.appendChild(gmlGeometry); // append gml result to geometry container
				var olGeometry = format.readGeometryFromNode(geometryElement, {
					dataProjection: "EPSG:4326",
					featureProjection: "EPSG:3857"
				}); // read geometry from geometryElement and reaturns ol.geom	
				var olfeature = new ol.Feature({geometry:olGeometry}); // create ol.Feature
				return olfeature;
			},
			"ows": function(bboxNode){
			/**
			Create extent Array from response of bounding box
			@param bboxNode {node}
			@return {<ol.Extent>|null}
			*/
				var response = ol.format.WPS.EX_BBOXCONST.readBoundingBox(bboxNode);
				if (goog.object.isEmpty(response)){
					return null;
				}
				else{
					var extent = goog.array.join(response["LowerCorner"], response["UpperCorner"]);
					return extent;
				}
			},
			"wfs": function(collectionNode){
			/**
			@param collectionNode {node} WFS collection node
			@return {array.<ol.Feature>}
			*/
				var features = ol.format.WPS.mainConstrunctors["wfsFormat"].readFeatures(collectionNode, {
					dataProjection: "EPSG:4326",
					featureProjection: "EPSG:3857"
				});
				return features;
			},
			"wkt":function(wktstring){
				var features = ol.format.WPS.mainConstrunctors["wktFormat"].readFeatures(wktstring);
				return features;
			},
			"geojson": function(geojsonObject){
				if(!goog.object.containsKey(geojsonObject, "type")){
					return null;
				}
				var features = ol.format.WPS.mainConstrunctors["GeoJSONFormat"].readFeatures(geojsonObject, {
					dataProjection: "EPSG:4326",
					featureProjection: "EPSG:3857"
				});
				return features;
			},
			"pattNode": /gml|ows|wfs|wps/,
			"prefixParser": function(node){
				var prefixns = ol.format.WPS.Supported["pattNode"].exec(node.prefix) || ol.format.WPS.Supported["pattNode"].exec(node.namespaceURI);
				if(prefixns != null){
					//alert(prefixns);
					return ol.format.WPS.Supported[prefixns[0]](node); // -- completed Regexped gml, ows or wfs wps bounding box data. Returns features or extent, or more wps results
				}else{
					return prefixns; // null
				}
			}
		}
		/**
		Read response from WPS process (source) and returns ol.Features (Complex Output), ol.Extent (Bounding box output) or string (Literal data output).
		This method supports GML, WKT, GeoJSON and ows:BoundingBox as inputs. More formats you can defined in opt_options (pattNode also for xml formats).
		@param source {Node|Document|object.<GeoJSON>} data returned from WPS process
		@param opt_options {Object=} or ol.formatWPS.Supported. If xml formats keys depend on prefix of first node (wfs:FeatureCollection) or namespaceURI. 
		@return {<ol.Feature>|array.<ol.Feature>|<ol.Extent>|string|null}
		@api
		*/
        ol.format.WPS.prototype.readExecuteResponse = function(source, opt_options){
			goog.asserts.assert(source != null);
			var opt = goog.object.clone(ol.format.WPS.Supported);
			if(goog.isDef(opt_options)){
				goog.object.extend(opt, opt_options);
			}
			if(ol.xml.isDocument(source)){ // gml, ows, wfs, wps
				return opt["prefixParser"](source.firstChild);
			}
			else if(ol.xml.isNode(source)){ // gml, ows, wfs, wps
				return opt["prefixParser"](source);
			}
			else if(goog.isString(source)){
				var featuresorplain;
			    try{
					featuresorplain = opt["wkt"](source);// --completed wkt, or simple string
				}catch(e){
					featuresorplain = source;
				} 
				return featuresorplain;
			}
			else if(goog.isObject(source)){
				return opt["geojson"](source)// --completed geojson
			}
			return null; // else return null
		}		//} --end output parsers
			// ------------------------ WRITING EXECUTE XML------------------------------------ //
	      /* writing features to execute proces
		    inputs: featureCollection or feature
              		  
		  */
		// opt object to execute process
		/**
		Options object for writing execute xml.
		*/
		  ol.format.WPS.OPT_OBJECT_write = {
		     "BBOXCOMPLEX_value": "value",
			 "LiteralData": "LiteralData",
			 "BBOX_Data": "BoundingBoxData",
			 "ComplexData": "ComplexData",
			 "ComplexOutput": "ComplexOutput",
			 "BBOX_Output": "BoundingBoxOutput",
			 "LiteralData_Output": "LiteralOutput",
			 "OutputsTypes": function(){return [this["BBOX_Output"], this["ComplexOutput"], this["LiteralData_Output"]]}, // array of names of outputs types
			 "InputsTypes": function(){return [this["LiteralData"],this["ComplexData"],this["BBOX_Data"]]},
			 "object_keys_support": !!Object.keys,
			 "default":"none" // --def value for which nothing write to Input
		  } 
	ol.format.WPS.EX_objectKeysReturner = function(obj){
		var exobject_keys = ol.format.WPS.OPT_OBJECT_write["object_keys_support"] ? Object.keys(obj) : goog.object.getKeys(obj); // loop cez "ComplexData atd."
		return exobject_keys;
	}
		
	    // functions for create Elements
	  ol.format.WPS.writeExecuteNode = function(simple_execute){// method for creating header Execute element - simple to wps:Body and header not simple
		       var Execute = ol.xml.createElementNS('http://www.opengis.net/wps/1.0.0', 'wps:Execute'); // prvy tag wps:Execute hlavickovy
			   ol.xml.setAttributeNS(Execute, "http://www.w3.org/2001/XMLSchema-instance", "xsi:schemaLocation", "http://www.opengis.net/wps/1.0.0 " + "http://schemas.opengis.net/wps/" + "1.0.0/wpsAll.xsd");
			   Execute.setAttribute("version", "1.0.0");
			   Execute.setAttribute("service", "WPS");
			   if(!simple_execute){
               // Execute.setAttribute("xmlns:wfs", "http://www.opengis.net/wfs/1.1.0");
			   ol.xml.setAttributeNS(Execute, "http://www.w3.org/2000/xmlns/", "xmlns:wfs", "http://www.opengis.net/wfs/1.1.0");
			   // Execute.setAttribute("xmlns:ows", "http://www.opengis.net/ows/1.1");
			   ol.xml.setAttributeNS(Execute, "http://www.w3.org/2000/xmlns/", "xmlns:ows", "http://www.opengis.net/ows/1.1");
			   // Execute.setAttribute("xmlns:gml", "http://www.opengis.net/gml");
			   ol.xml.setAttributeNS(Execute, "http://www.w3.org/2000/xmlns/", "xmlns:gml", "http://www.opengis.net/gml");
			   // Execute.setAttribute("xmlns:ogc", "http://www.opengis.net/ogc");
			   ol.xml.setAttributeNS(Execute, "http://www.w3.org/2000/xmlns/", "xmlns:ogc", "http://www.opengis.net/ogc");
			   // Execute.setAttribute("xmlns:wcs", "http://www.opengis.net/wcs/1.1.1");
			   ol.xml.setAttributeNS(Execute, "http://www.w3.org/2000/xmlns/", "xmlns:wcs", "http://www.opengis.net/wcs/1.1.1");
			   // Execute.setAttribute("xmlns:xlink", "http://www.w3.org/1999/xlink");
			   ol.xml.setAttributeNS(Execute, "http://www.w3.org/2000/xmlns/", "xmlns:xlink", "http://www.w3.org/1999/xlink");
		    }
			return Execute;
		} // we must see, how namespaces are implement in WPS execute.
	
	ol.format.WPS.EX_writeElementIFEXISTS = function(elementName){
	/**
    Check if value in data is null, false, NaN, undefined, null, OPT_OBJECT_write.default - if true return undefined, else return element with elementName  
    @param elementName {string} 
	*/
		return (function(value, objectStack){
		var dataValue = objectStack[objectStack.length - 1]['dataObject'][value][ol.format.WPS.OPT_OBJECT_write["BBOXCOMPLEX_value"]];
		// if input is null, undefined, NaN or OPT_OBJECT_write.default, but is not number 0 (0 is valid value)
		//var features = (objectStack[0].features != null && goog.isArray(objectStack[0].features)) && objectStack[0].features[0];
		if(dataValue && dataValue !== ol.format.WPS.OPT_OBJECT_write["default"] || dataValue === 0){ // --if dataValue --and --not default value defined in OPT --or dataValue --is 0 
			return ol.xml.createElementNS('http://www.opengis.net/wps/1.0.0',elementName);			
		}else{ // --else --return undefined
			return;
		};
	  });
	} 
	ol.format.WPS.EX_GMLGeometryPusher = function(xml, appendNode){
	/**
	@param xml {Node} where find geometry
	@param appendNode {Node} where append geometry element
	*/
		var childrens = goog.dom.getChildren(xml); // childrens of feature members and features
		for(var i = 0, ii = childrens.length; i < ii; i++){
			var geom = goog.dom.findNode(childrens[i], function(n){
				return n.localName === "geometry";
			});
			appendNode.appendChild(geom.firstChild);
		}
	}
	ol.format.WPS.mainConstrunctors = {
		"gmlFormat": new ol.format.GML({featureNS:"http://gis.fns.uniba.sk/projekty/mgr/15/kocisek/", featureType:"wpsapp", srsName: "http://www.opengis.net/gml/srs/epsg.xml#4326"}),
		"wfsFormat": new ol.format.WFS({
			gmlFormat: new ol.format.GML({
				srsName: "http://www.opengis.net/gml/srs/epsg.xml#4326"
			})
		}),
		"wktFormat": new ol.format.WKT(),
		"GeoJSONFormat": new ol.format.GeoJSON()
	}
	ol.format.WPS.EX_mimetype_functions = {
	/**
	   What to do with features in specifed format as key. 
	*/
		// "gmlFormat": new ol.format.GML({featureNS:"http://localhost/OL3", featureType:"wpsapp", multiSurface:true, multiCurve:true}), 
	  "gml": function(features, node){
			var gml_xml = ol.format.WPS.mainConstrunctors["gmlFormat"].writeFeaturesNode(features, {
				dataProjection: "EPSG:4326",
				featureProjection: "EPSG:3857"
			});
			ol.format.WPS.EX_GMLGeometryPusher(gml_xml,node); // push geomery to node
		},
		"wfs-collection": function(features, node){
		    var Collection = ol.xml.createElementNS('http://www.opengis.net/wfs', 'wfs:FeatureCollection');
			// push bounded by to FeatureCollection
			/*pushSerializeAndPop({node: Collection}, ol.format.WPS.GML_boundedBy,
				ol.format.WPS.WPS_EX_IDNODE_FACTORY_, [null], [], [["gml:boundedBy", "http://www.opengis.net/ows/1.1"]]);
			*/
			ol.xml.setAttributeNS(Collection, "http://www.w3.org/2001/XMLSchema-instance", "xsi:schemaLocation", "http://www.opengis.net/wfs " + "http://schemas.opengis.net/wfs/" + "1.1.0/wfs.xsd");
			var gml_xml = ol.format.WPS.mainConstrunctors["gmlFormat"].writeFeaturesNode(features, {
				dataProjection: "EPSG:4326",
				featureProjection: "EPSG:3857"
			});
			Collection.appendChild(gml_xml);
			node.appendChild(Collection);
		}
	}
	ol.format.WPS.complex_data_attr = {
	    "mimeType": "text/xml; subtype=gml/3.1.1"
	}
	ol.format.WPS.dataType_EX_Convertor = { 
	 /**  
	    functions fo converting datatypes of input OBJ
	 */
		"int": function(node, number){ol.format.XSD.writeIntegerTextNode(node, number);},
		"integer": function(node, number){ol.format.XSD.writeIntegerTextNode(node, number);},
		"double": function(node, number){ol.format.XSD.writeDecimalTextNode(node, number);},
		"decimal": function(node, number){ol.format.XSD.writeDecimalTextNode(node, number);},
		"float":  function(node, number){ol.format.XSD.writeDecimalTextNode(node, number);}
	}
	ol.format.WPS.EX_dataPUSHER = function(node, value, objectStack){
	    var obj = objectStack[objectStack.length - 1];
	    var typeOfData = obj['key']; // complex data ...
		var values = obj['inputObject'][ol.format.WPS.OPT_OBJECT_write["BBOXCOMPLEX_value"]]; // returns value of input, for example: {value:ol.extent} returns ol.extent
		goog.object.remove(obj['inputObject'], ol.format.WPS.OPT_OBJECT_write["BBOXCOMPLEX_value"]);
		var patt = /int$|integer$|double$|float$|decimal$/; // if xs:float or float is true
		var reg = obj['inputObject']['DataType'] && patt.exec(obj['inputObject']['DataType']);
		var Literal_DataType = reg ? reg : null;  
		ol.xml.pushSerializeAndPop({node: node}, ol.format.WPS.EX_INPUTSERIALIZER_DATATYPE_PUSH,
				ol.xml.makeSimpleNodeFactory("wps:" + typeOfData), [[values, Literal_DataType, obj['inputObject']]], objectStack);
	}
	/**
		Returns object with replaced keys to lower based on first place in key´s string. Replace all argument is object which contains keys to replace whole key´s string to lower.
		@param obj {Object}
		@param replaceall_obj{Object|undefined} object contains keys, which replace all key´s string to lower.
	*/
	ol.format.WPS.A_REPLACE_KEYS = function(obj, replaceall_obj){
		goog.asserts.assertObject(obj);
		replaceall_obj = replaceall_obj || {};
		var nob = {};
		var oob = ol.format.WPS.EX_objectKeysReturner(obj), key;
		for(var i = 0, ii = oob.length; i<ii; i++){
			key = oob[i];
			var key_lower = key.toLowerCase();
			var first = key.charAt(0);
			if(first !== first.toLowerCase()){
				if(goog.object.containsKey(replaceall_obj, key_lower)){
					goog.object.set(nob, key_lower, obj[key]);
					continue;
				}else{
					var others = key.slice(1);
					goog.object.set(nob, first.toLowerCase()+others, obj[key]);
				}
			}else{ // if on first place is Lower, simply copy key:value
				goog.object.set(nob, key, obj[key]);
			}
		};
		return nob;
	}
	/**
	@const 
	object to replace all characters to lower
	*/
	ol.format.WPS.EX_COMPLEXDATA_ATTR = {
		'encoding': 1,
		'schema': 1
	};
	ol.format.WPS.EX_COMPLEXDATA_PUSHER = function(node, value, objectStack){
	/**
	Append ComplexData element with geometryin GML or other supported format (wfs-collection) to node. Accept also node, document and string.
	*/
		var source = value[0];
		var cloner = goog.object.clone(ol.format.WPS.complex_data_attr);
		var lower = ol.format.WPS.A_REPLACE_KEYS(value[2], ol.format.WPS.EX_COMPLEXDATA_ATTR); // converts input object to lower on the first place MimeType -> mimeType.
		goog.object.extend(cloner,lower);
		var regexped = /gml|wfs-collection/.exec(cloner["mimeType"]);// we must test for feature mimetype
		goog.dom.xml.setAttributes(node, cloner);
		if (ol.xml.isDocument(source)) {
			for (var n = source.firstChild; n; n = n.nextSibling) {
				if (n.nodeType == goog.dom.NodeType.ELEMENT) {
					node.appendChild(n);
					break;
				}
			}
		} else if (ol.xml.isNode(source)) {
			node.appendChild(source);
		} else if (goog.isString(source)) {
			var doc = ol.xml.parse(source);
			for (var n = doc.firstChild; n; n = n.nextSibling) {
				if (n.nodeType == goog.dom.NodeType.ELEMENT) {
					node.appendChild(n);
					break;
				}
			}
		} else {
			ol.format.WPS.EX_mimetype_functions[regexped[0]](source, node);
		}
	}
	ol.format.WPS.EX_BBOXDATA_PUSHER = function(node, value, objectStack){
	    /**
		Converts ol.extent in "value" input to LowerCase and UpperCase nodes.
		*/
		goog.asserts.assert(value[0].length === 4);
		var cloner = goog.object.clone(ol.format.WPS.EX_BBOXCONST["attr"]);
		var lower = ol.format.WPS.A_REPLACE_KEYS(value[2], cloner); // converts input object to lower on the first place CRS -> crs.
		goog.object.extend(cloner,lower);
		 // value[0] is ol.extent<Array> in this case
		// set attributes to BBOX element, which are needed
		goog.dom.xml.setAttributes(node, cloner);
		return ol.format.WPS.EX_BBOXCONST.writeBoundingBox(value[0], node, objectStack);
	}
	ol.format.WPS.EX_XSDFORMAT_PUSHER = function(node, value, objectStack){
	    if(ol.format.WPS.dataType_EX_Convertor[value[1]] == null){
			ol.format.XSD.writeStringTextNode(node, value[0]);
		}else{
			ol.format.WPS.dataType_EX_Convertor[value[1]](node, value[0]);
		}
	}
	// methods for input and output
	
	ol.format.WPS.ResponseDocumentReturner = function(value, objectStack){
		return ol.format.WPS.ResponseDocumentNode;
	}
	
	ol.format.WPS.EX_writeInputOutput = function(node, value, objectStack){
	    /**
		   if input is true, return Input element, else RawDataOutput/Response Document. Loop trought object keys inputs or outputs.  
		*/
		var stack = objectStack[objectStack.length - 1];
		var ex_object = stack["executeObject"];
		var responseDocument = stack["responseDocument"];
		var responseDocumentAttributes = stack["responseDocumentAttr"];
		var object_keys = ol.format.WPS.EX_objectKeysReturner(ex_object),object_val,key;
		for(var i = 0; i < object_keys.length; i++){
		    key = object_keys[i];
			object_val = ex_object[key];
			if(!goog.object.isEmpty(object_val) && goog.array.contains(this["types"], key)){
			    // keys of input = data input Identifier  
				var value_o_keys = ol.format.WPS.EX_objectKeysReturner(object_val);
				// into object stack we send dataInputnode, key (if LiteralData and so on), and identifier of input, which is possible
				var object_to_send = {node: node, 'dataObject': ex_object[key], 'dataObjectKey': key, 'responseDocumentAttr': responseDocumentAttributes};
				if(this["input"]){// if type is input type 
				  ol.xml.pushSerializeAndPop(object_to_send, ol.format.WPS.EX_INPUTS_OUTPUTSERIALIZER,
					ol.format.WPS.EX_writeElementIFEXISTS("wps:Input")/*if exists or is 0 value, so create Input, elese nothing*/, value_o_keys, objectStack);   // iterate over dataInputs (Identifier), in child appender function for Input - its now value arg
				}else{ // if type is output type - literal data, complex data...
					if (responseDocument){
						ol.xml.pushSerializeAndPop(object_to_send, ol.format.WPS.EX_INPUTS_OUTPUTSERIALIZER,
							ol.format.WPS.ResponseDocumentReturner, [value_o_keys], objectStack);   // iterate over dataOutputs (Identifier), in child appender function for Input - its now value arg
					}else{
					ol.xml.pushSerializeAndPop(object_to_send, ol.format.WPS.EX_INPUTS_OUTPUTSERIALIZER,
					ol.xml.makeSimpleNodeFactory("wps:RawDataOutput"), value_o_keys, objectStack);   // iterate over dataOutputs (Identifier), in child appender function for Input - its now value arg
					}
				}
			}
		}
	}
	// data output and input
	/* --depracted --merged ol.format.WPS.EX_writeInputOutput
	ol.format.WPS.EX_InputOutput = {
	    "Input": function(node, value, objectStack){
			        var ex_object = objectStack[objectStack.length - 1]["executeObject"];
					// --this var inputTypes = ol.format.WPS.OPT_OBJECT_write.InputsTypes();
					ol.format.WPS.EX_writeInputOutput(node, value, objectStack, [ex_object, inputTypes],  true);
				},
		"Output": function(node, value, objectStack){
		            var ex_object = objectStack[objectStack.length - 1]["executeObject"];
					// --this var outputTypes = ol.format.WPS.OPT_OBJECT_write.OutputsTypes();
					/*var new_arr = [];
					for(var i = 0; i < ex_objectkeys[1].length; i++){
					   if(goog.array.contains(ol.format.WPS.OPT_OBJECT_write.OutputsTypes(), ex_objectkeys[1][i])){
						    new_arr.push(ex_objectkeys[1][i]);
						};
					};
					goog.array.insertAt(ex_objectkeys, new_arr, 1);
					ol.format.WPS.EX_writeInputOutput(node, value, objectStack, [ex_object, outputTypes], false);
				}
			}
	*/
	ol.format.WPS.EX_RAWDATAPUSHER = function(node, value, objectStack){
	/**
	Get object with data for output and push attributes to raw data output node. Write identifier of output to Identifier node.
	*/
	     //goog.asserts.assert(node.nodeType == goog.dom.NodeType.ELEMENT);
		 var outputObject = objectStack[objectStack.length - 1]['dataObject'][value];
		 var cloner = goog.object.clone(ol.format.WPS.EX_BBOXCONST["attr"]);
		 var lower = ol.format.WPS.A_REPLACE_KEYS(outputObject, cloner); // converts output object to lower on the first place CRS -> crs. 
		 // prida atributy do raw data output node.
		 goog.dom.xml.setAttributes(node, lower);
		 ol.xml.pushSerializeAndPop({node: node}, ol.format.WPS.EX_INPUTSERIALIZER_PUSH,
		 ol.xml.NodeCreator, [value], objectStack,[["ows:Identifier", "http://www.opengis.net/ows/1.1"]]);   // iterate over dataOutputs (Identifier), in child appender function for Input - its now value arg	 
	}
	ol.format.WPS.EX_RESPONSEDOCUMENTPUSHER = function(node, value, objectStack){
		
		ol.xml.pushSerializeAndPop({node: node, 'dataObject': objectStack[objectStack.length - 1]['dataObject']}, ol.format.WPS.EX_INPUTSERIALIZER_PUSH,
			ol.xml.makeSimpleNodeFactory("wps:Output"), value, objectStack);   // give Output element to ResponseDocument
	}
	ol.format.WPS.EX_INPUTPUSHER = function(node, value, objectStack){
	    var inputDataObject = objectStack[objectStack.length - 1]["dataObject"][value]; // value and format of input 
	    var keyOfDataType = objectStack[objectStack.length - 1]['dataObjectKey']; // Complex data ....
		//var inputobject_keys = ol.format.WPS.OPT_OBJECT_write.object_keys_support ? Object.keys(inputDataObject) : goog.object.getKeys(inputDataObject),key; // loop cez input identifiers"
		var inputValue = inputDataObject[ol.format.WPS.OPT_OBJECT_write["BBOXCOMPLEX_value"]]; // value 
		// ak je value object, tak musi dat Reference, inak Data
		/*if(goog.isObject(inputDataObject[ol.format.WPS.OPT_OBJECT_write["BBOXCOMPLEX_value"]]) && !goog.object.isEmpty(inputDataObject[ol.format.WPS.OPT_OBJECT_write["BBOXCOMPLEX_value"]])){
		    var wps = new ol.format.WPS(); // tu pojde rekuzia na wps:body   ----- DOROBIT!!!
			ol.xml.pushSerializeAndPop({node: node, 'inputObject': inputDataObject, key:keyOfDataType}, ol.format.WPS.EX_INPUTSERIALIZER_PUSH,
				ol.format.WPS.WPS_EX_IDNODE_FACTORY_, [value,value], objectStack, [["ows:Identifier", "http://www.opengis.net/ows/1.1"],["wps:Reference", 'http://www.opengis.net/wps/1.0.0']]); 
		}else{*/
		   ol.xml.pushSerializeAndPop({node: node, 'inputObject': inputDataObject, key:keyOfDataType}, ol.format.WPS.EX_INPUTSERIALIZER_PUSH,
				ol.xml.NodeCreator, [value,value], objectStack, [["ows:Identifier", "http://www.opengis.net/ows/1.1"],["wps:Data", 'http://www.opengis.net/wps/1.0.0']]);
		/*}
		ol.xml.pushSerializeAndPop({node: node, 'dataObject': executeInputObj[key], 'dataObjectKey': key}, ol.format.WPS.EX_INPUTSERIALIZER,
				ol.format.WPS.WPS_EX_IDNODE_FACTORY_, [value], objectStack, [["ows:Identifier", "http://www.opengis.net/ows/1.1"]]);
	*/
	}
	// //gml bounded by creating
		  // /**
	       // <gml:boundedBy><gml:Box>unknown</gml:null></gml:boundedBy>
		   // */
		  // ol.format.WPS.GML_boundedBy = ol.xml.makeStructureNS(
        // ol.format.WPS.GMLNS, {
          // 'boundedBy': ol.xml.makeChildAppender(ol.format.WPS.EX_BBOXPUSHER),
	      // 'Box': ol.xml.makeChildAppender(ol.format.WPS.EX_BBOXPUSHER)
		  // 'coordinates'
        // });
	// execute
	
    ol.format.WPS.EX_INPUTSERIALIZER_DATATYPE_PUSH = ol.xml.makeStructureNS(
        ol.format.WPS.NAMESPACE_URIS_, {
      'LiteralData': ol.xml.makeChildAppender(ol.format.WPS.EX_XSDFORMAT_PUSHER), // rozhodne ktory datovy typ piise 
	  'BoundingBoxData': ol.xml.makeChildAppender(ol.format.WPS.EX_BBOXDATA_PUSHER),
	  'ComplexData': ol.xml.makeChildAppender(ol.format.WPS.EX_COMPLEXDATA_PUSHER)
    });
	ol.format.WPS.EX_INPUTSERIALIZER_PUSH = ol.xml.makeStructureNS(
       ol.format.WPS.NAMESPACE_URIS_ALL_, {
      'Identifier': ol.xml.makeChildAppender(ol.format.XSD.writeStringTextNode),
	  'Data': ol.xml.makeChildAppender(ol.format.WPS.EX_dataPUSHER),
	  'Reference': ol.xml.makeChildAppender(ol.format.XSD.writeStringTextNode),
	  'Output': ol.xml.makeChildAppender(ol.format.WPS.EX_RAWDATAPUSHER)
    });
    ol.format.WPS.EX_INPUTS_OUTPUTSERIALIZER = ol.xml.makeStructureNS(
       ol.format.WPS.NAMESPACE_URIS_, {
      'Input': ol.xml.makeChildAppender(ol.format.WPS.EX_INPUTPUSHER),
	  'RawDataOutput': ol.xml.makeChildAppender(ol.format.WPS.EX_RAWDATAPUSHER),
	  'ResponseDocument' : ol.xml.makeChildAppender(ol.format.WPS.EX_RESPONSEDOCUMENTPUSHER)
    });
	ol.format.WPS.WPS_IDSERIALIZER_ = ol.xml.makeStructureNS(
       ol.format.WPS.NAMESPACE_URIS_ALL_, {
      'Identifier': ol.xml.makeChildAppender(ol.format.XSD.writeStringTextNode),
	  'DataInputs': ol.xml.makeChildAppender(ol.format.WPS.EX_writeInputOutput, {"input":true, "types": ol.format.WPS.OPT_OBJECT_write["InputsTypes"]()}), // --def -- /n
	  'ResponseForm': ol.xml.makeChildAppender(ol.format.WPS.EX_writeInputOutput, {"input": false, "types": ol.format.WPS.OPT_OBJECT_write["OutputsTypes"]()}) // --def --{} for returning input or output and OutputTypes (BBOX, LITERAL...)
    });
		 /**
		Function for writing XML execute request.
		@param options {Object}:
			objectExecute (Object) configuration object for writing execute XML
				{Identifier, LiteralData {value}, ComplexData {value}, BoundingBoxData {value}},
			ResponseDocument (Boolean=) If write ResponseDocument instead RawDataOutput and  false
			ResponseDocumentAttributes (Object=) undefined
		*/
          ol.format.WPS.prototype.writeExecute = function(options){
			   goog.asserts.assertObject(options);
			   goog.asserts.assertObject(options["objectExecute"]);
			   var RespDocumentAttrs = options["ResponseDocumentAttributes"] || {};
				if (options["ResponseDocument"] != null){
				   var ResponseDocument = options["ResponseDocument"];
				   if (ResponseDocument){
						ol.format.WPS.ResponseDocumentNode = ol.xml.NodeCreator(null, null, ["wps:ResponseDocument", 'http://www.opengis.net/wps/1.0.0']);
						goog.dom.xml.setAttributes(ol.format.WPS.ResponseDocumentNode, RespDocumentAttrs);
					}
			   }else{
				   var ResponseDocument = false;
			   }
			   this.ExecuteNode = ol.format.WPS.writeExecuteNode();
			   var elements_arr = [["ows:Identifier", "http://www.opengis.net/ows/1.1"], ["wps:DataInputs", 'http://www.opengis.net/wps/1.0.0'], ["wps:ResponseForm", 'http://www.opengis.net/wps/1.0.0']];
			   ol.xml.pushSerializeAndPop( 
                {node: this.ExecuteNode, "executeObject": options["objectExecute"], "responseDocument": ResponseDocument}, ol.format.WPS.WPS_IDSERIALIZER_,
                           ol.xml.NodeCreator, [options["objectExecute"]["Identifier"], null, null], [], elements_arr);
				ol.format.WPS.ResponseDocumentNode = null;
				return this.ExecuteNode;
			}	
goog.exportProperty(ol.format.WPS.prototype,'readGetCapabilities', ol.format.WPS.prototype.readGetCapabilities);
goog.exportProperty(ol.format.WPS.prototype,'readDescribeProcess', ol.format.WPS.prototype.readDescribeProcess);
goog.exportProperty(ol.format.WPS.prototype,'writeExecute', ol.format.WPS.prototype.writeExecute);
goog.exportProperty(ol.format.WPS.prototype,'readExecuteResponse', ol.format.WPS.prototype.readExecuteResponse);
goog.exportSymbol('ol.format.WPS.EX_objectKeysReturner', ol.format.WPS.EX_objectKeysReturner);


