	  ol.format.XSD.readValue = function(node){
	       return node.firstChild.nodeValue;
	  }
	  ol.format.XSD.trueReturner = function(){
	     return true;
	  }
	/**
	*Return node with integer number in text node.
	*@param {Node} node
	*@param {number} Integer Integer number to write into node
	*/
	ol.format.XSD.writeIntegerTextNode = function(node, Integer){
        var string = parseInt(Integer).toString();
        node.appendChild(ol.xml.DOCUMENT.createTextNode(string));
	}