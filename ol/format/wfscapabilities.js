goog.provide('ol.format.WFSCapabilities');

goog.require('goog.asserts');
goog.require('goog.dom.NodeType');
goog.require('ol');
goog.require('ol.format.XLink');
goog.require('ol.format.XML');
goog.require('ol.format.XSD');
goog.require('ol.xml');
goog.require('goog.array');

/**
 * @classdesc
 * Format for reading WFS capabilities data
 *
 * @constructor
 * @extends {ol.format.XML}
 * @api
 */
ol.format.WFSCapabilities = function(options) {

  goog.base(this);

  /**
   * @type {string|undefined}
   */
  this.version = undefined;
  
   ol.format.WFSCapabilities.NAMESPACE_URIS_EXTENDER_(options.featureNamespaces);
};
goog.inherits(ol.format.WFSCapabilities, ol.format.XML);

ol.format.WFSCapabilities.NAMESPACE_URIS_ = [
	null,
	'http://www.opengis.net/ows/1.1', 
	"http://www.opengis.net/ows", 
	'http://www.opengeospatial.net/ows',
	'http://www.opengis.net/wfs'
];

ol.format.WFSCapabilities.NAMESPACE_URIS_EXTENDER_ = function(arr){
	goog.array.extend(ol.format.WFSCapabilities.NAMESPACE_URIS_, arr);
}

/**
 * Read a WFS capabilities document.
 *
 * @function
 * @param {Document|Node|string} source The XML source.
 * @return {Object} An object representing the WMS capabilities.
 * @api
 */
ol.format.WFSCapabilities.prototype.read;

/**
 * @param {Document} doc Document.
 * @return {Object} WFS Capability object.
 */
ol.format.WFSCapabilities.prototype.readFromDocument = function(doc) {
  goog.asserts.assert(doc.nodeType == goog.dom.NodeType.DOCUMENT,
      'doc.nodeType should be DOCUMENT');
  for (var n = doc.firstChild; n; n = n.nextSibling) {
    if (n.nodeType == goog.dom.NodeType.ELEMENT) {
      return this.readFromNode(n);
    }
  }
  return null;
};

/**
 * @param {Node} node Node.
 * @return {Object} WMS Capability object.
 */
ol.format.WFSCapabilities.prototype.readFromNode = function(node) {
  goog.asserts.assert(node.nodeType == goog.dom.NodeType.ELEMENT,
      'node.nodeType should be ELEMENT');
  goog.asserts.assert(node.localName == 'WFS_Capabilities',
      'localName should be WFS_Capabilities');
  this.version = node.getAttribute('version').trim();
  goog.asserts.assertString(this.version, 'this.version should be a string');
  var wfsCapabilityObject = ol.xml.pushParseAndPop({
    'version': this.version
  }, ol.format.WFSCapabilities.PARSERS_, node, []);
  return wfsCapabilityObject ? wfsCapabilityObject : null;
};

ol.format.WFSCapabilities.readServiceIdentification_ = function(node, objectStack) {
  goog.asserts.assert(node.nodeType == goog.dom.NodeType.ELEMENT,
      'node.nodeType should be ELEMENT');
  goog.asserts.assert(node.localName == 'ServiceIdentification',
      'localName should be Service');
  return ol.xml.pushParseAndPop(
      {}, ol.format.WFSCapabilities.SERVICE_PARSERS_, node, objectStack);
};

/**
 * @private
 * @param {Node} node Node.
 * @param {Array.<*>} objectStack Object stack.
 * @return {Array.<string>|undefined} Keyword list.
 */
ol.format.WFSCapabilities.readKeywordList_ = function(node, objectStack) {
  goog.asserts.assert(node.nodeType == goog.dom.NodeType.ELEMENT,
      'node.nodeType should be ELEMENT');
  goog.asserts.assert(node.localName == 'Keywords',
      'localName should be Keywords');
  return ol.xml.pushParseAndPop(
      [], ol.format.WFSCapabilities.KEYWORDLIST_PARSERS_, node, objectStack);
};

/**
 * @private
 * @param {Node} node Node.
 * @param {Array.<*>} objectStack Object stack.
 * @return {Object} Bounding box object.
 */
ol.format.WFSCapabilities.readBoundingBox_ = function(node, objectStack) {
  goog.asserts.assert(node.nodeType == goog.dom.NodeType.ELEMENT,
      'node.nodeType should be ELEMENT');
  goog.asserts.assert(node.localName == 'BoundingBox',
      'localName should be BoundingBox');

  var extent = [
    ol.format.XSD.readDecimalString(node.getAttribute('minx')),
    ol.format.XSD.readDecimalString(node.getAttribute('miny')),
    ol.format.XSD.readDecimalString(node.getAttribute('maxx')),
    ol.format.XSD.readDecimalString(node.getAttribute('maxy'))
  ];

  var resolutions = [
    ol.format.XSD.readDecimalString(node.getAttribute('resx')),
    ol.format.XSD.readDecimalString(node.getAttribute('resy'))
  ];

  return {
    'crs': node.getAttribute('CRS'),
    'extent': extent,
    'res': resolutions
  };
};

/**
 * @private
 * @param {Node} node Node.
 * @param {Array.<*>} objectStack Object stack.
 * @return {Object|undefined} Online resource object.
 */
ol.format.WFSCapabilities.readFormatOnlineresource_ = function(node, objectStack) {
  goog.asserts.assert(node.nodeType == goog.dom.NodeType.ELEMENT,
      'node.nodeType should be ELEMENT');
  return ol.xml.pushParseAndPop(
      {}, ol.format.WFSCapabilities.FORMAT_ONLINERESOURCE_PARSERS_,
      node, objectStack);
};

ol.format.WFSCapabilities.readMetadataURL_ = function(node, objectStack) {
  goog.asserts.assert(node.nodeType == goog.dom.NodeType.ELEMENT,
      'node.nodeType should be ELEMENT');
  goog.asserts.assert(node.localName == 'MetadataURL',
      'localName should be MetadataURL');
  var metadataObject =
      ol.format.WFSCapabilities.readFormatOnlineresource_(node, objectStack);
  if (metadataObject) {
    metadataObject['type'] = node.getAttribute('type');
    return metadataObject;
  }
  return undefined;
};

/**
 * @private
 * @param {Node} node Node.
 * @param {Array.<*>} objectStack Object stack.
 * @return {Array|undefined} Feature types list.
 */
ol.format.WFSCapabilities.readFeatureTypes = function(node, objectStack){
	return ol.xml.pushParseAndPop(
      [], ol.format.WFSCapabilities.FEATURE_TYPE_PARSERS_, node, objectStack);
}

/**
 * @private
 * @param {Node} node Node.
 * @param {Array.<*>} objectStack Object stack.
 * @return {Array|undefined} Feature types list.
 */
ol.format.WFSCapabilities.readFeatureType = function(node, objectStack){
	return ol.xml.pushParseAndPop(
      {}, ol.format.WFSCapabilities.FEATURE_TYPES_PARSERS_, node, objectStack);
}

 
ol.format.WFSCapabilities.PARSERS_ = ol.xml.makeStructureNS(
    ol.format.WFSCapabilities.NAMESPACE_URIS_, {
      'ServiceIdentification': ol.xml.makeObjectPropertySetter(
          ol.format.WFSCapabilities.readServiceIdentification_),
      'FeatureTypeList': ol.xml.makeObjectPropertySetter(
          ol.format.WFSCapabilities.readFeatureTypes)
    });
	
ol.format.WFSCapabilities.SERVICE_PARSERS_ = ol.xml.makeStructureNS(
    ol.format.WFSCapabilities.NAMESPACE_URIS_, {
      'Name': ol.xml.makeObjectPropertySetter(ol.format.XSD.readString),
      'Title': ol.xml.makeObjectPropertySetter(ol.format.XSD.readString),
      'Abstract': ol.xml.makeObjectPropertySetter(ol.format.XSD.readString),
      'Keywords': ol.xml.makeObjectPropertySetter(
          ol.format.WFSCapabilities.readKeywordList_),
      'OnlineResource': ol.xml.makeObjectPropertySetter(
          ol.format.XLink.readHref),
      'Fees': ol.xml.makeObjectPropertySetter(ol.format.XSD.readString),
      'AccessConstraints': ol.xml.makeObjectPropertySetter(
          ol.format.XSD.readString),
      'LayerLimit': ol.xml.makeObjectPropertySetter(
          ol.format.XSD.readNonNegativeInteger),
      'MaxWidth': ol.xml.makeObjectPropertySetter(
          ol.format.XSD.readNonNegativeInteger),
      'MaxHeight': ol.xml.makeObjectPropertySetter(
          ol.format.XSD.readNonNegativeInteger)
    });

/**
 * @const
 * @type {Object.<string, Object.<string, ol.xml.Parser>>}
 * @private
 */
ol.format.WFSCapabilities.FEATURE_TYPE_PARSERS_ = ol.xml.makeStructureNS(
    ol.format.WFSCapabilities.NAMESPACE_URIS_, {
      'FeatureType': ol.xml.makeArrayPusher(ol.format.WFSCapabilities.readFeatureType)
    });

/**
 * @const
 * @type {Object.<string, Object.<string, ol.xml.Parser>>}
 * @private
 */
ol.format.WFSCapabilities.FEATURE_TYPES_PARSERS_ = ol.xml.makeStructureNS(
    ol.format.WFSCapabilities.NAMESPACE_URIS_, {
      'Name': ol.xml.makeObjectPropertySetter(ol.format.XSD.readString),
      'Title': ol.xml.makeObjectPropertySetter(ol.format.XSD.readString),
      'Abstract': ol.xml.makeObjectPropertySetter(ol.format.XSD.readString),
      'Keywords': ol.xml.makeObjectPropertySetter(
          ol.format.WFSCapabilities.readKeywordList_),
      'CRS': ol.xml.makeObjectPropertyPusher(ol.format.XSD.readString),
	  'DefaultSRS': ol.xml.makeObjectPropertySetter(ol.format.XSD.readString),
	  'BoundingBox': ol.xml.makeObjectPropertyPusher(
          ol.format.WFSCapabilities.readBoundingBox_),
      'Identifier': ol.xml.makeObjectPropertyPusher(ol.format.XSD.readString),
      'MetadataURL': ol.xml.makeObjectPropertyPusher(
          ol.format.WFSCapabilities.readMetadataURL_),
      'DataURL': ol.xml.makeObjectPropertyPusher(
          ol.format.WFSCapabilities.readFormatOnlineresource_),
      'FeatureListURL': ol.xml.makeObjectPropertyPusher(
          ol.format.WFSCapabilities.readFormatOnlineresource_),
      'MaxScaleDenominator': ol.xml.makeObjectPropertySetter(
          ol.format.XSD.readDecimal)
    });
	
/**
 * @const
 * @type {Object.<string, Object.<string, ol.xml.Parser>>}
 * @private
 */
ol.format.WFSCapabilities.FORMAT_ONLINERESOURCE_PARSERS_ =
    ol.xml.makeStructureNS(ol.format.WFSCapabilities.NAMESPACE_URIS_, {
      'Format': ol.xml.makeObjectPropertySetter(ol.format.XSD.readString),
      'OnlineResource': ol.xml.makeObjectPropertySetter(
          ol.format.XLink.readHref)
    });
	
/**
 * @const
 * @type {Object.<string, Object.<string, ol.xml.Parser>>}
 * @private
 */
ol.format.WFSCapabilities.KEYWORDLIST_PARSERS_ = ol.xml.makeStructureNS(
    ol.format.WFSCapabilities.NAMESPACE_URIS_, {
      'Keyword': ol.xml.makeArrayPusher(ol.format.XSD.readString)
    });