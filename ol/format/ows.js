goog.provide('ol.format.Ows');
goog.require('ol.format.XML');
goog.require('ol.format.XSD');
goog.require('ol.xml');
goog.require('ol.extent');
goog.require('goog.dom.xml');
	/**
	For ows format with projection definition.
	@constructor 
	@param proj {ol.proj.ProjectionLike}
	@extends {ol.format.XML}
	*/
	ol.format.Ows = function(proj){
		this.projection = goog.isDef(proj) ? proj : "EPSG:4326";
		this.dimensions = "2";
		/**
		@type
		@api
		*/
		this.attr = {
			"crs": this.projection,
			"dimensions": this.dimensions
		}
		goog.base(this);
	}
	goog.inherits(ol.format.Ows, ol.format.XML);
	ol.format.Ows.NAMESPACE_URIS_OWS_ = [null, 'http://www.opengis.net/ows/1.1', 'http://www.opengeospatial.net/ows'];
	ol.format.Ows.NAMESPACE_URIS_ALL_ = [null, 'http://www.opengis.net/ows/1.1', 'http://www.opengeospatial.net/ows', 'http://www.opengis.net/wps/1.0.0', 'http://www.opengeospatial.net/wps'];
	ol.format.Ows.EX_BBOXPUSHER = function(node, value, objectStack){
	    var corner = value.join(" ");
		ol.format.XSD.writeStringTextNode(node, corner); // into lower and upper we push ol.Coordinates joined with space.
	}
	ol.format.Ows.BBOX_SERIALIZER = ol.xml.makeStructureNS(
        ol.format.Ows.NAMESPACE_URIS_OWS_, {
      'LowerCorner': ol.xml.makeChildAppender(ol.format.Ows.EX_BBOXPUSHER),
	  'UpperCorner': ol.xml.makeChildAppender(ol.format.Ows.EX_BBOXPUSHER)
    });
	/**
	Write lower corner and upper corner nodes.
	@param extent {<ol.Extent>}
	@param opt_bboxnode {Node|undefined} if undefined, create ows:BoundingBox element, else append lower corner and upper corner to this node.
	@param opt_objectStack {Array=} if is used in other serializers or empty array.
	@return {node|null|undefined} if opt_bboxnode returns null, extent not array returns undefined, else return ows:BoundingBox node
	@api
	*/
	ol.format.Ows.prototype.writeBoundingBox = function(extent, opt_bboxnode, opt_objectStack){
		if(goog.isArray(extent)){
			var bottomLeft = ol.extent.getBottomLeft(extent);
			var topRight = ol.extent.getTopRight(extent);
		}else{
			return;
		}
		if(goog.isDef(opt_bboxnode)){
			ol.xml.pushSerializeAndPop({node: opt_bboxnode}, ol.format.Ows.BBOX_SERIALIZER,
			ol.xml.NodeCreator, [bottomLeft, topRight], opt_objectStack || [],[["LowerCorner", "http://www.opengis.net/ows/1.1"], ["UpperCorner", "http://www.opengis.net/ows/1.1"]]);
			return null;
		}else{
			var bboxNode = ol.xml.createElementNS('http://www.opengis.net/ows/1.1', 'ows:BoundingBox');
			ol.xml.pushSerializeAndPop({node: bboxNode}, ol.format.Ows.BBOX_SERIALIZER,
			ol.xml.NodeCreator, [bottomLeft, topRight], opt_objectStack || [],[["LowerCorner", "http://www.opengis.net/ows/1.1"], ["UpperCorner", "http://www.opengis.net/ows/1.1"]]);
			goog.dom.xml.setAttributes(bboxNode, this.attr); // attributes to new node
			return bboxNode;
		}
	}
	ol.format.Ows.BBOX_PARSECORNERS = function(node, objectStack){ // read coordinates string and return array of coordinates
		var corner = ol.format.XSD.readString(node);
		var coordinates = corner.split(" ");
		return coordinates;
	}
	ol.format.Ows.BBOX_PARSER = ol.xml.makeParsersNS( // bbox format
		ol.format.Ows.NAMESPACE_URIS_OWS_, {
		'LowerCorner': ol.xml.makeObjectPropertySetter(ol.format.Ows.BBOX_PARSECORNERS),
		'UpperCorner': ol.xml.makeObjectPropertySetter(ol.format.Ows.BBOX_PARSECORNERS)
	});
	ol.format.Ows.BBOX_reader = function(node, objectStack){
		return ol.xml.pushParseAndPop({},
			ol.format.Ows.BBOX_PARSER, node, objectStack);
	}
	
	ol.format.Ows.BBOX_MAINPARSER = ol.xml.makeParsersNS( // bbox format
		ol.format.Ows.NAMESPACE_URIS_ALL_, {
		'BoundingBox': ol.xml.makeObjectPropertySetter(ol.format.Ows.BBOX_reader),
		'BoundingBoxData': ol.xml.makeObjectPropertySetter(ol.format.Ows.BBOX_reader)
	});
	/**
	@param node {Node} BoundingBox node with LowerCorner and Upper corner or node with Bounding Box element as child element.
	@return {object} 
	@api
	*/
	ol.format.Ows.prototype.readBoundingBox = function(node, opt_objectStack){	
		var localName = node.localName;
		if(localName === 'BoundingBox' || localName === "BoundingBoxData"){
			var bboxarr = ol.xml.pushParseAndPop({"crs": node.getAttribute("crs") || null},ol.format.Ows.BBOX_PARSER, node, []);
			return bboxarr;
		}else{
			return ol.xml.pushParseAndPop({},ol.format.Ows.BBOX_MAINPARSER, node, []);
		}
	};

	ol.format.Ows.ExceptionText = ol.xml.makeParsersNS(
		ol.format.Ows.NAMESPACE_URIS_OWS_, {
		'ExceptionText': ol.xml.makeObjectPropertySetter(ol.format.XSD.readString)
	});

	ol.format.Ows.readException = function(node, objectStack){
		return ol.xml.pushParseAndPop({"exception": true}, ol.format.Ows.ExceptionText, node, objectStack);
	}

	ol.format.Ows.ExceptionReport = ol.xml.makeParsersNS(
		ol.format.Ows.NAMESPACE_URIS_OWS_, {
			'Exception': ol.xml.makeObjectPropertySetter(ol.format.Ows.readException)
		}
	);

	ol.format.Ows.prototype.readException = function(options){
		if(goog.isDef(options.node)){
			if(options.node.localName == "ExceptionReport"){
				return ol.xml.pushParseAndPop({}, ol.format.Ows.ExceptionReport, options.node, []);
			}else{
				return null;
			}
		}else{
			return null;
		}
	}


	goog.exportProperty(ol.format.Ows.prototype,'writeBoundingBox', ol.format.Ows.prototype.writeBoundingBox);
	goog.exportProperty(ol.format.Ows.prototype,'readBoundingBox', ol.format.Ows.prototype.readBoundingBox);
	goog.exportProperty(ol.format.Ows.prototype,'readException', ol.format.Ows.prototype.readException);
	goog.exportSymbol('ol.format.Ows.attr', ol.format.Ows.attr);