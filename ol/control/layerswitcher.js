goog.provide('ol.control.LayerSwitcher');
goog.require('ol.control.Control');
goog.require('ol.source.Vector');
goog.require('ol.layer.Group');
goog.require('goog.events');
goog.require('goog.dom');
goog.require('goog.dom.classlist');
goog.require('goog.events');
goog.require('goog.events.EventType');
goog.require('goog.array');
goog.require('goog.object');
	/**
	@consructor
	@extends {ol.control.Control}
	@param {Object} opt_options:
	target (Element|undefined), 
	selectInteraction (ol.interaction.Select), 
	tipTitle (string=) or text Layers, 
	buttonClass (string=) or ol-layerSwitcher-main-button
	selectValue (string=) or text nothing
	removeValue (string=) or text nothing
	selectTitle (string=) or text nothing
	removeTitle (string=) or text nothing
	buttonCreator (function=) empty function with ol.control.LayerSwitcher in this,
		parameters: actual layer, element with buttons to layer, layers of layer group (if undefined null), actual layer group.
	buttonCreator_this
	*/
	ol.control.LayerSwitcher = function(opt_options){
		var options = opt_options || {};
		var tipLabel = options.tipTitle || 'Layers';
		var button = goog.dom.createDom('button', { // --var button for layers
			'type': 'button',
			'title': tipLabel,
			'class': options.buttonClass || 'ol-layerSwitcher-main-button'
		});
		var headerLi = goog.dom.createDom('li', {'id':'header-layer-li', 'class': 'grey-font'}, goog.dom.createDom('span', {id:'header-layer'}, goog.dom.createTextNode(tipLabel)));
		this.layersUl = goog.dom.createDom('ul', {'id': 'ol-scroll-layers'});
		var mainUl = goog.dom.createDom('ul', null, headerLi, this.layersUl); // -def ul with layers
		goog.events.listen(button, goog.events.EventType.CLICK, this.makeClick_, false, this);
		goog.events.listen(button, [goog.events.EventType.FOCUSOUT, goog.events.EventType.MOUSEOUT], function() {
		/**
		For uncolored button after mouseout
		*/
			this.blur();
		}, false);
		var container = goog.dom.createDom('div', {'class': "ol-layerSwitcher ol-unselectable ol-control ol-collapsed"}, button, mainUl); // --append container for li layers and button
		goog.events.listen(button, goog.events.EventType.CLICK, this.makeClick_, false, this);
		goog.base(this, {
			element: container,
			render: ol.control.LayerSwitcher.render,
			target: options.target
		});
		this.buttonCreator = options.buttonCreator || function(){};
		this.buttonCreator_this = options.buttonCreator_this || this;
		this.selectValue = options.selectValue || '';
		this.removeValue = options.removeValue || '';
		this.selectTitle = options.selectTitle || 'Select';
		this.removeTitle = options.removeTitle || 'Remove';
		this.selectInteraction = options.selectInteraction;
	    this.result = [];
	    this.UIDs = [];
		this.layersUIDs = {};
		this.cursorLayerGroup = null;
	}
	goog.inherits(ol.control.LayerSwitcher, ol.control.Control);
	/**
	* @param {ol.MapEvent} mapEvent Map event.
	* @this {ol.control.LayerSwitcher}
	* @api
	*/
	ol.control.LayerSwitcher.render = function(MapEvent){
		this.updateLayers(MapEvent.map);
		this.result.length = 0;
	    this.UIDs.length = 0;
		this.cursorLayerGroup = null;
	}
	/**
	Function to display layers (if ol-collapsed).
	*/
	ol.control.LayerSwitcher.prototype.makeClick_ = function(e){
		goog.dom.classlist.toggle(this.element,'ol-collapsed');
		goog.dom.classlist.toggle(this.element,'ol-border-grey');
	}
	/**
	Select features to ol.interaction.Select defined in options. 
	@param layer {array.ol.layer.Layer}
	*/
	ol.control.LayerSwitcher.prototype.selectFeaturesFromLayer = function(layer){
		var selectedFeatures = this.selectInteraction.getFeatures(); // --var select interaction ol.Collection 
		for(var i = 0, ii=layer.length; i < ii; i++){
			var featuresInLayer = layer[i].getSource().getFeatures(); // --var features in layer source
			var arrFeatures = selectedFeatures.getArray();
			featuresInLayer.forEach(function(e, i){
				var element = e;
				if(goog.array.every(arrFeatures, function(e,i){ // --if --not find feature with UID in selected features --then push feature from layer to selected
					return goog.getUid(element) !== goog.getUid(e);
				})){
					selectedFeatures.push(element); 
				};
			});
		}
	}
	/**
	Create select button and bind in to layer or layergroup. Validating for exitistence of ol.source.Vector in layer array.
	@param layer {Array.ol.layer.Layer}
	@param {element} to append the button
	@param {Array.<ol.layer.Layer>}layersOfGroup to check, if its some vectors to select button
	@param {ol.layer.Group||null} on button remove
	*/
	ol.control.LayerSwitcher.prototype.createButtons = function(layer, parentElement, layersOfGroup, layerGroup){
		var closeButton = goog.dom.createDom('button', {'type': 'button', 'class': 'ol-layerSwitcher-button ol-layerSwitcher-remove', 'title': this.removeTitle, 'value': this.removeValue});
		goog.events.listen(closeButton, goog.events.EventType.CLICK, function(e){
			/**
			If layerGroup, remove from layer group, else from map.
			*/
			if(layerGroup){
				layerGroup.getLayers().remove(layer); // remove layer from layer group --if its child of layergroup
			}else{
				this.getMap().removeLayer(layer);
			}
		}, false, this);
		var validVectorLayers = goog.array.filter(layersOfGroup || [layer], function(e,i,o){
			if(e instanceof ol.layer.Group){
				return true;
			}else{
				return e.getSource() instanceof ol.source.Vector;
			}
		});
		if(validVectorLayers.length > 0){ // - if vector, push buttons
			var sButton = goog.dom.createDom('button', {'class': 'ol-selecter ol-layerSwitcher-button', 'type':'button', 'title': this.selectTitle, 'value': this.selectValue});
			goog.events.listen(sButton, goog.events.EventType.CLICK, function(e){
				e.preventDefault();
				this.selectFeaturesFromLayer(validVectorLayers);
			}, false, this);
			goog.dom.appendChild(parentElement, sButton);
		}
		goog.dom.appendChild(parentElement, closeButton); // append closing button to li
	}
	/**
	Add li element with layer tools to the ul. Extend object with active layers -> test if active layers contains oldLayer UID -> 
	if not, remove li element with layer.
	@param map {ol.Map}
	*/
	ol.control.LayerSwitcher.prototype.updateLayers = function(map){
		var layers = map.getLayers();
	    this.findNewLayers(layers);
	    var actualLayers = this.layersUIDs;
	        goog.array.forEach(goog.object.getKeys(actualLayers), function(e,i,o){ // --if layer in layerSwither --not --in actual layers UIDs --remove
				var key = e;
				if(!goog.array.some(this.UIDs, function(element,i,o){
					return element === key;
				})){
					goog.dom.removeNode(actualLayers[e]);
					goog.object.remove(actualLayers, e);
	            }
		}, this);
		this.addLayers();
	}
        /**
	 find new layers in map and return position of this layer in standard object. All founded UIDs of layers push to this.UIDs array.
	 After its possible to compare with this.layerUIDs object if layer exists or no.
	 -- filter for new layers
	 */
        ol.control.LayerSwitcher.prototype.findNewLayers = function(layers, opt_array){
	    var news = opt_array || this.result;
	    var layerss = layers.getArray();
	    var len = layers.getLength(); 
	    for(var i = len - 1; i >= 0; --i){
			var layer = layerss[i];
			var layerUID = goog.getUid(layer).toString(); // --var get uid of layer/layer collection to test, if we dont have this layer in map
			var obj = {"l": layer, "child":[], "newvalue": true, "layerGroup": false};
			this.UIDs.push(layerUID);
			if(layer instanceof ol.layer.Group){
				goog.object.set(obj, "layerGroup", true);
				if(goog.object.containsKey(this.layersUIDs, layerUID)){
					goog.object.set(obj, "newvalue", false);
				}
				news.push(obj); // set if layer group, but attribute newvalue if false, so not to render
				this.findNewLayers(layer.getLayers(), obj["child"]);
			}else{
				if(!goog.object.containsKey(this.layersUIDs, layerUID)){
					news.push(obj);
				}
			}
	    }
	}
	/**
	Replace ol.dom.Input
	@param input {DOM} checkbox
	@param layer {ol.layer.Layer}
	*/
	ol.control.LayerSwitcher.prototype.checking = function(input, layer){
		goog.dom.setProperties(input, {"checked": layer.getVisible()});
		goog.events.listen(input, goog.events.EventType.CHANGE, function(e){
			var checked = e.target.checked;
			if (checked !== layer.getVisible()) {
				layer.setVisible(checked);
			}
		});
		layer.on('change:visible', function(){
			var visible = this.getVisible();
			if (visible !== input.checked) {
				input.checked = visible;
			}
		});
	}
	/**
	Add li element and tools for layers (buttons ...), recursively self call if found something @param newLayersArray {Array}
	@param newLayersArray {Array}
	*/
	ol.control.LayerSwitcher.prototype.addLayers = function(opt_newLayersArray, opt_ul){
		goog.array.forEach(opt_newLayersArray || this.result, function(e, i, a){
		/**
		@this {ol.control.LayerSwitcher}
		*/
		var layer = e["l"];
		if(e["newvalue"]){
			var inputVisible = goog.dom.createDom('input', {'type': 'checkbox'});
			var layerName = goog.dom.createDom('span', {'class':'layer-title'}); // layer title text
			var buttoncontainer = goog.dom.createDom('div', {'class': 'ol-layerSwitcher-buttons'});
		    var li = goog.dom.createDom('li', null, inputVisible, layerName, buttoncontainer); // --var main li element to which append
			this.checking(inputVisible, layer);
		    goog.object.set(this.layersUIDs, goog.getUid(layer), li); // set uid to uids object defined in constructor
		    goog.dom.insertChildAt(opt_ul || this.layersUl, li, i); // --append to opt_ul element on the strict place depended on i --not this ul element of ol.control.LayerSwitcher
		    if(e["layerGroup"]){ // --if layer group --true self call --not add layer to ul
				if(!opt_newLayersArray){ // -- if recusrsion
					this.cursorLayerGroup = null;
				}
				var childUl = goog.dom.createDom('ul');
				this.createButtons(layer, buttoncontainer, layer.getLayers().getArray(), this.cursorLayerGroup); // --function create button for selecting features from group
				this.buttonCreator.call(this.buttonCreator_this, layer, buttoncontainer, layer.getLayers().getArray(), this.cursorLayerGroup);
				goog.dom.setTextContent(layerName, layer.get("title") || "Layer Group " + (i + 1));
				goog.dom.appendChild(li, childUl);
				this.cursorLayerGroup = layer;
				this.addLayers(e["child"], childUl) // --call recursivly function for Layer group with ul, also with layer group (layer)
		    }else{ // --not layergroup
				goog.dom.setTextContent(layerName, layer.get("title") || "Layer " + (i + 1));
				this.createButtons(layer, buttoncontainer, null, this.cursorLayerGroup);
				this.buttonCreator.call(this.buttonCreator_this, layer, buttoncontainer, null, this.cursorLayerGroup);
		    }
		}else{ // --not new
		    var li = goog.object.get(this.layersUIDs, goog.getUid(e["l"]));
		    if(layer instanceof ol.layer.Group){ // --if layer group --true self call --not add layer to ul
				this.addLayers(e["child"], li.querySelector("ul")) // --call recursivly function for Layer group with ul
		    }
		}
	}, this);
}
