goog.provide('ol.control.EditTool');
goog.require('ol.interaction.Draw');
goog.require('ol.control.Control');
goog.require('ol.source.Vector');
goog.require('ol.geom.GeometryType');
goog.require('ol.source.Vector');
goog.require('goog.object');
goog.require('goog.events');
goog.require('goog.array');
goog.require('goog.dom');
goog.require('goog.dom.classlist');
	/**
	Create edit tools with buttons for writing point, line and polygon. Buttons for removing and adding (empty) vector layers.
	@param {Object} options:
		selectInteraction {ol.interaction.Select|undefined}
		controlClass {string=} defeault options: ol-editTool ol-unselectable ol-control 
		pointTitle {string=} default is Point
		lineTitle {string=} defalt is Line
		polygonTitle {string=} default is Polygon
		pointLabel {string=} default are div
		lineLabel {string=} default are div
		polygonLabel {string=} default are div
		target {Element|string|undefined} Specify a target if you want the control to be rendered outside of the map's viewport.
	@extends {ol.control.Control}
	@constructor
	*/
	ol.control.EditTool = function(options){
		var options = options || {};
		// for editing title in layer switcher "U+270E"
		// --create buttons for editing enabling
		var point = goog.isString(options.pointLabel) ? goog.dom.createTextNode(options.pointLabel) : goog.dom.createDom('div', {'class': 'ol-point'});
		var line = goog.isString(options.lineLabel) ? goog.dom.createTextNode(options.lineLabel) : goog.dom.createDom('div', {'class': 'ol-line'});
		var polygon = goog.isString(options.polygonLabel) ? goog.dom.createTextNode(options.lineLabel) : goog.dom.createDom('div', {'class': 'ol-polygon'});
		
		var pointBtn = goog.dom.createDom('button', {'title': options.pointTitle || 'Point', 'name': 'Point'}, point);
		var lineBtn = goog.dom.createDom('button', {'title': options.lineTitle || 'Line', 'name': 'LineString'}, line);
		var polygonBtn = goog.dom.createDom('button', {'title': options.polygonTitle || 'Polygon', 'name': 'Polygon'}, polygon);
		var container = goog.dom.createDom('div', {'class': options.controlClass || "ol-editTool ol-unselectable ol-control"}, pointBtn, lineBtn, polygonBtn); // --append container for buttons and checkboxes
		goog.base(this, {
			element: container,
			target: options.target
		});
		var btnArr = [pointBtn, lineBtn, polygonBtn];
		this.buttons = btnArr;

		this.drawid = 0;

		var geometriesArr = [ol.geom.GeometryType.POINT,ol.geom.GeometryType.LINE_STRING, ol.geom.GeometryType.POLYGON];
		// --def new vector layer as container
		/**
		type {ol.source.Vector}
		*/
		var sourcesArr = [new ol.source.Vector(), new ol.source.Vector(), new ol.source.Vector()];
		/**
		Select interaction to toggle acitve
		*/
		this.selectInteraction = options.selectInteraction || null;
		/**
		@type
		Main object for delete layers and start and stop interactions:
			ol.geom.GeometryType : {Array.<ol.interaction.Draw, ol.layer.Vector>}
		*/
		this.configObject = {};
		for(var i = 0; i < 3; i++){ // --for 
			var geometryType = geometriesArr[i];
			var button = btnArr[i];
			var draw = new ol.interaction.Draw({
				source: sourcesArr[i],
				type: /** @type {ol.geom.GeometryType} */ geometryType
			});
			draw.setActive(false);
			// draw.once('change:active', this.toggleSelect, this); // --event toggle select interction in map defined as options.selectInteraction
			var layer = new ol.layer.Vector({
				source: sourcesArr[i],
				title: "Draw " + geometryType
			});
			goog.object.set(this.configObject, geometryType, [draw, layer]);
			//draw.setActive(false);
			// this.layersArr.push(layer);
			// this.interactionArr.push(draw);
			goog.events.listen(button, goog.events.EventType.CLICK, 
				function(e){
					var clickedbutton = e.currentTarget;
					for(var j = 0, jj = btnArr.length; j < jj; j++){
						var btn = btnArr[j];
						if(btn.name !== clickedbutton.name){
							if(goog.dom.classlist.contains(btn, 'ol-editTool-editing')){
								goog.dom.classlist.remove(btn, 'ol-editTool-editing');
							}
						}else{
							goog.dom.classlist.toggle(clickedbutton, "ol-editTool-editing"); // add class with editing
						}
						
					}
					this.startDrawing.call(this, e);
				}, false, this);
			goog.events.listen(button, [goog.events.EventType.FOCUSOUT, goog.events.EventType.MOUSEOUT], function() {
				this.blur();
			}, false);
		}
	}
	goog.inherits(ol.control.EditTool, ol.control.Control);
	/**
	Toggle select interaction.
	@param active {boolean} set this as active
	@this {ol.control.EditTool}
	*/
	ol.control.EditTool.prototype.toggleSelect = function(active){	
		var select = this.selectInteraction;
		if(select){
			select.setActive(active);
		}
	}
	/**
	If interactions to drawing are not in map, add it. Initial state.
	*/
	ol.control.EditTool.prototype.initial = function(){
		var map = this.getMap();
		goog.object.forEach(this.configObject, function(e){
			map.addInteraction(e[0]);
		});
	}
	ol.control.EditTool.prototype.clearInteractions = function(){
		var map = this.getMap();
		map.set("editing", false);
		this.toggleSelect(true);
		goog.object.forEach(this.configObject, function(e){
			e[0].setActive(false);
			map.removeInteraction(e[0]);
		});
		for(var i = 0, ii = this.buttons.length; i<ii; i++){
			var button = this.buttons[i];
			if(goog.dom.classlist.contains(button, 'ol-editTool-editing')){
				goog.dom.classlist.remove(button, 'ol-editTool-editing');
			}
		}
	}
	/**
	Disable interactions and remove empty layers added to map with edit buttons. If interaction is added to map, active event dont fire, so
	must disable select interaction.
	@param {ol.interaction.Draw}
	*/
	ol.control.EditTool.prototype.dissableEmpty = function(draw){
		var map = this.getMap();
		var active = draw.getActive(); // --inow is active
		if(!draw.getMap()){ // --if interaction not in map --true --add --all draw interactions
			this.initial();
		}
		goog.object.forEach(this.configObject, function(e){
			var layer = e[1];
			var drawToUnactive = e[0];
			if(goog.getUid(draw) !== goog.getUid(drawToUnactive)){ // --if --not draw to add, set active to false
				drawToUnactive.setActive(false);
				if(goog.array.isEmpty(layer.getSource().getFeatures())){ // --if empty layer without features
					map.removeLayer(layer);
				}
			}else{ // draw to add or remove
				if(drawToUnactive.getActive()){ // is active, i now that deactivate
					if(goog.array.isEmpty(layer.getSource().getFeatures())){ // --if empty layer without features
						map.removeLayer(layer);
					}
				}else{//not active
					var inMap = goog.array.findRight(map.getLayers().getArray(), function(l){ // --if layer not in map --true add it
						return goog.getUid(l) === goog.getUid(layer);
					});
					if(!inMap){
						map.addLayer(layer);
					}
				}
			}
		});
		draw.setActive(!active);
		map.set("editing", !active); // dissable editing attribute in ol.Map
		this.toggleSelect(active); 
	}
	ol.control.EditTool.prototype.startDrawing = function(e){
		var button = e.currentTarget;
		var buttonName = button.name;
		var type = this.configObject[buttonName];
		var draw = type[0];
		var layerToAdd = type[1];
		if(!draw.getActive()){
			layerToAdd.setStyle(undefined);
		}
		draw.on("drawend", function(e){
			//var features = [];
			//features = layerToAdd.getSource().getFeatures();
			e.feature.set("drawid", this.drawid);
			this.drawid = this.drawid + 1;
		}, this);
		this.dissableEmpty(draw);
	}

	ol.control.EditTool.prototype.clearLayers = function(){
		var map = this.getMap();
		goog.object.forEach(this.configObject, function(e){
			var layer = e[1];
			layer.getSource().clear();
			map.removeLayer(layer);
		});
	}