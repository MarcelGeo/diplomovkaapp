ol.Feature.prototype.transform = function(from, to){
	/**
	Transform feature to projection. This method changes feature´s geometry.
	@param Projection from transform
	@param Projection to which transform
	*/
		this.setGeometry(this.getGeometry().transform(from, to));
	}
	
	goog.exportProperty(
		ol.Feature.prototype,
		'transform',
		ol.Feature.prototype.transform);