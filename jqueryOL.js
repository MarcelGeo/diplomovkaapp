	 ol.request = function(){};
	/*  ol.request.GET = function(url, callback){
        if(typeof callback === "function" && url !== ""){		
	        goog.net.XhrIo.send(url, callback);
	    }
	  }*/
	  ol.request.GETJQXHR = function(opt_object,asyn){
	      opt_object['async'] = asyn ? true : asyn;		  
	      this.jqxhr = jQuery.ajax(opt_object);
	      return this.jqxhr;
	  }
  ol.format.WPS.prototype.getCapabilities = function(options){ 		  
		      if(goog.isDef(options) && goog.isDef(options.success) && goog.isFunction(options.success)){
			    var reqq = ol.request.GETJQXHR({url:this.url, dataType: "xml", data:{service:"WPS", version:"1.1.0", request:"GetCapabilities"}});
			    reqq.done(success);
				reqq.fail(function(jqXHR, textStatus){console.log(textStatus)});
			  }else{ // using ol.xml parser and give getCapabilitites to json
			    var reqq = ol.request.GETJQXHR({url:this.url, dataType: "xml", data:{service:"WPS", version:"1.1.0", request:"GetCapabilities"}}, false);
				reqq.fail(function(jqXHR, textStatus){console.log("status of requested url:  "+ jqXHR.statusText)});
				 if(goog.isDef(reqq.responseXML)){
				this.process = ol.format.WPS.readFromNode.getCapabilities(reqq.responseXML.firstChild, []);
				}else{return false;}
				return this.process;
				}	
			};
/**
		PArams: options<Object> -> proces<string>: identifier of proces, success<function>: function after request with data | undefined
		Return: Object with describe proces
		*/
		ol.format.WPS.prototype.describeProcess = function(options){
		      var proces = goog.asserts.assertString(options.proces); 
			  if(goog.isDef(options.success) && goog.isFunction(options.success)){
			     var reqq = ol.request.GETJQXHR(
				    {
					  url: this.url,
					  dataType: "xml",
                      data: {service: "WPS",
					         version:"1.0.0",
					         request: "DescribeProcess",
							 identifier:proces	
							}				  
					});
				reqq.done(options.success);
				reqq.fail(function(jqXHR, textStatus){console.log(textStatus)});
				}
				else{
				   var reqq = ol.request.GETJQXHR(
				     {
					  url: this.url,
					  dataType: "xml",
                      data: {service: "WPS",
					         version:"1.0.0",
					         request: "DescribeProcess",
							 identifier: proces	
							}				  
					}, false);
				   reqq.fail(function(jqXHR, textStatus){console.log("status of requested url:  "+ jqXHR.statusText);});
				   if(goog.isDef(reqq.responseXML)){
				      this.desc_process = ol.format.WPS.readFromNode.describeProcess(reqq.responseXML.firstChild, []);	
                   }else{return false;}
                   if(this.desc_process.ProcessDescriptions && this.desc_process.ProcessDescriptions.length < 2){// ak je key s procesDescr, tak vrati ho, inak nie.
				         return this.desc_process.ProcessDescriptions[0]; 
					}else{
					     return this.desc_process;
					}
				}
			};