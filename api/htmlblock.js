/**
create new block with data to this.
*/
goog.provide('api.htmlblock');
goog.require('api.dynamic');
goog.require('api.dynamicTypes');
goog.require('goog.dom');
goog.require('goog.events');
/**
@constructor
@param data {} data to block, used as this.data
@param blocks {Array.<api.htmlblock>} blocks to append
@param display= {Function} called by display method
@param undisplay= {Function} called by display method
@param creator= {Function} called by display method
*/
api.htmlblock = function(element, data, blocks, display, hide, creator){
	this.element = element;
	this.data = data;
	this.display = display || function(){};
	this.hide = hide || function(){};
	this.creator = creator || function(){};
	this.blocks = new api.dynamic();
	goog.events.listen(this.blocks, api.dynamicTypes.REMOVE, function(e){
		goog.dom.removeNode(e.element.element);
	}, false, this);
	goog.events.listen(this.blocks, api.dynamicTypes.PUSH, function(e){
		goog.dom.appendChild(this.element , e.element.element);
	}, false, this);
	if(blocks){
		for (var i = 0, ii = blocks.length; i < ii; i++){
			this.blocks.push(blocks[i]);
		}
	}
	
	this.creator.call(this);
}

api.htmlblock.prototype.appendBlock = function(toappend){
		this.blocks.push(toappend);
}

api.htmlblock.prototype.clear = function(blocktoremove){
	this.blocks.clear();
}

api.htmlblock.prototype.display = function(){
	this.display.call(this);
}

api.htmlblock.prototype.hide = function(){
	this.hide.call(this);
}

api.htmlblock.prototype.setData = function(data){
	this.data = data;
}



