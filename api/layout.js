	goog.provide('api.layout');
	goog.require('goog.events');
	goog.require('goog.events.EventType');
	goog.require('goog.dom.classlist');
	goog.require('api.htmlblock');

	api.layout.mainHTMLOverlay = new api.htmlblock(document.querySelector("div#footeroverlay"), null, null, function(){
		if(goog.dom.classlist.contains(this.element, "nodisplay")){
			goog.dom.classlist.remove(this.element, "nodisplay");		
		}
	}, function(){
		if(!goog.dom.classlist.contains(this.element, "nodisplay")){
			goog.dom.classlist.add(this.element, "nodisplay");
		}
	}, function(){ // creator
		goog.events.listen(this.element.querySelector("#closeButton"), goog.events.EventType.CLICK, function(e){
			this.hide();
		}, false, this);
	});

	api.layout.selecter = function(input){
		input.select();
	};
	api.layout.hover = function(button, opt_class){
		goog.dom.classlist.toggle(button, opt_class);
	};
	api.layout.navigation = document.querySelector('nav');
	/**
	api.layout.navbutton = api.layout.navigation.querySelector('button#nav');
	goog.events.listen(api.layout.navbutton, goog.events.EventType.CLICK, function(e){
		
		Toggle class for displaying menu in responsive.
		
		this.hover(this.navigation, 'menu-show');
	}, false, api.layout);
*/
	api.layout.showFooter = function(){
		api.layout.mainHTMLOverlay.display();
	}

	goog.exportSymbol('api.layout.hover', api.layout.hover);
	goog.exportSymbol('api.layout.selecter', api.layout.selecter);
	goog.exportSymbol('api.layout.showFooter', api.layout.showFooter);