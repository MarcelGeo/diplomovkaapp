goog.provide('api.processForm');
goog.require('ol.format.WPS');
goog.require('goog.events');
goog.require('goog.events.EventType');
goog.require('goog.array');
goog.require('goog.object');
goog.require('goog.dom');
goog.require('goog.dom.classlist');
goog.require('api.dynamic');
goog.require('api.dynamicTypes');
goog.require('api.customs');
goog.require('api.htmlblock');
goog.require('goog.dom.classlist');
	/**
	@constructor
	Define form, process object created from form and errors.
	@param formContainer {Element} element to append metadata and form. 
	@param options {Object}:
		identifier (string),
		title (string),
		desc (string),
		executeUri (appUrl)
	@param submitF {Function} call to submit the form 
	*/
	api.processForm = function(formContainer, options, submitF){
		/**
		Uri for execute
		*/
		this.executeUri = options.executeUri.proxySet(null, null, 'route').proxyUri;
		this.baseUri = options.executeUri;
		this.formContainer = formContainer;
		/**
		Errors dynamic array.
		*/
		this.errors = new api.dynamic();
		// goog.events.listen(this.errors, api.dynamicTypes.REMOVE, this.submitRenderer, false, this);
		goog.events.listen(this.errors, api.dynamicTypes.PUSH, this.submitRenderer, false, this);
		/**
		Input object to ol.format.WPS with first key value.
		Object with data from this.formElement
		*/
		this.processInput = {"Identifier": options.identifier};
		this.formData = {};
		/**
		Epsg for transformation of extent.
		Default is WGS84.
		*/
		this.epsg = "EPSG:4326";
		/**
		Metadata element.
		*/
		var header = goog.dom.createDom('h2', {'id':'header', 'class':'clearer'}, goog.dom.createTextNode(options.title));
		var description = goog.dom.createDom('div', {'id': 'proc_abstract', 'class':'abstract grey-border-radius'}, goog.dom.createTextNode(options.desc));
		this.metadata = goog.dom.createDom('div', {'id':'metadata', 'class': 'metadat grey-border-radius grey-font'}, header, description);
		/**
		Form element.
		*/
		var loaderImg = goog.dom.createDom('img', {'id': 'loaderimg', 'src': '../icons/smallLoader.GIF'});
		var loader = goog.dom.createDom('div', {'class':'loaderBack nodisplay', 'data-loader': 'false'}, goog.dom.createDom('span'), loaderImg);
		this.formElement = goog.dom.createDom('form', {
			'class': 'procesInput grey-border-radius',
			'action': ''
		}, loader);
		goog.events.listen(this.formElement, goog.events.EventType.SUBMIT, submitF, false, this);
		/**
		button to append on the end depended on errors.
		*/
		this.submitButton = goog.dom.createDom('input', {'type':'submit', 'value':'Execute', 'disabled': true}); // --def execute submit --default green button
		
		/**
		Selectors api.htmlblock for dynamic WFS
		*/
		this.blocks = new api.dynamic();
		this.data = options.additional;
		
	}
	
	/**
	If submit button name is not the same as grey, then replace it with grey...
	*/
	api.processForm.prototype.GreyOrGreen = function(disabled){
		var classDisabled = 'submit-disabled';
		/*
		if(this.submitButton.tagName !== buttonToReplace.tagName){ // --if --not the same button (green button) --action define new this.submitButton and test
			if(goog.dom.contains(this.formElement, this.submitButton)){ // --if form contains green button --action replace it with grey
				goog.dom.replaceNode(buttonToReplace,this.submitButton);
			}
		this.submitButton = buttonToReplace;
		}*/
		if(goog.dom.contains(this.formElement, this.submitButton)){ // --if form contains green button --action disable it
			this.submitButton.disabled = disabled;
			if(disabled){
				goog.dom.classlist.add(this.submitButton, classDisabled);
			}else{
				goog.dom.classlist.remove(this.submitButton, classDisabled);
			}
		}
		
	}
	/**
	Add grey or green button to append to the end of form and replace green/grey button with grey/green button.
	Depended on added value to array (if: false -> test for existing (this.submitButton) greybutton, if form contains green button, replace with grey.) 
	@param e {goog.events.Event}
	@this {api.processForm}
	*/
	api.processForm.prototype.submitRenderer = function(e){
		var added = e.element;
		// --create grey button and change this button with button to append on the end of form 
		//var greyButton = goog.dom.createDom('div', {'class':"off_div"}, goog.dom.createTextNode("Execute")); 
		//var greenButton = goog.dom.createDom('input', {type:"submit", value:"Execute"}); // --var --create green submit button -> execute submit
		if(!added){ // --if false added to array --action test for existing grey button ... --else loop trought errors and find false
			// alert("false added");
			// this.GreyOrGreen(greyButton);
			this.GreyOrGreen(true);
		}else{ // --find false in errors, --else define green button, --if contains form grey button, replace with green button.
			// alert("true added");
			if(goog.array.every(this.errors.arr, function(e){
					return e;
				})){
				// this.GreyOrGreen(greenButton);
				this.GreyOrGreen(false);
			}
		}
	}
	/** --depracted
	If length is 0, then replace grey submit button with green button.
	@param e {goog.events.Event}
	@this {api.processForm}
	*/
	// api.processForm.prototype.submitRenderer = function(e){
		// var errarr = e.target;
		// if(errarr.length < 1 && !e.element){ // --if errors array is !empty --action render grey div (button)
			// goog.dom.appendChild(this.formElement, goog.dom.createDom('div', {class:"off_div"}, goog.dom.createTextNode("Execute")));
		// }
	// }
	/**
	Push into processInputs the typical data type if dont exists.
	@param type {String} type of input (LiteralData ...)
	@param identifier {string} for example ("geom") identifier of input
	@param objecttopush {Object}:
		value {Object|ol.Feature|Node|Document|String|null} value as input data (features, value from input ...)
	@api
	*/
	api.processForm.prototype.processInputPusher = function(type, identifier, objecttopush){
		goog.object.setIfUndefined(this.processInput, type, {});
		goog.object.set(this.processInput[type],identifier,objecttopush);
	}
	/**
	Combining value object (this.formData) with values in processInput object as (input for wps.execute).
	@api
	*/
	api.processForm.prototype.writeExecuteObject = function(){
		// goog.asserts.assertObject(value_obj);
		//var process = goog.object.clone(this.proces_input_cache);
		var keys = ol.format.WPS.EX_objectKeysReturner(this.processInput),key,value_o;
		for(var j = 0, jj = keys.length; j < jj; j++){ // loop trought data types : complex ....
			key = keys[j];
			value_o = this.processInput[key];
			if(!goog.object.isEmpty(value_o)){// if input_obj prop is empty, go to other data inputs (prop)
				api.customs.LiteralDataBBOXComplexValueAdd(value_o, this.formData);
			}
		}
		return this.processInput;
	}
	/**
	For each input (text, dropdown) fill data to this.formData object {identifier:value}
	validateF {function} function to call for each input and validate value (return see api.valTextInput)
	opt_this_validate {Object=} id undefined this (api.processForm)
	@api
	*/
	api.processForm.prototype.addDataToFormData = function(validateF, opt_this_validate){
		//var inputObj = goog.object.clone(this.formData);
		for(var i = 0, ii = this.formElement.length; i<ii; i++){ // --for input in form
			var formelement = this.formElement.elements[i]; 
			if(formelement.type === "button" || formelement.type === "submit" || goog.dom.classlist.contains(formelement, "select-selectFeatures")){continue;} // dont need work with buttons
			this.formData[formelement.name] = validateF.call(opt_this_validate || this , formelement)["value"];
			//inputObj["required"] = formelement.getAttribute("data-required"); potrebne len pri validacii  
		}
		return this.formData;
	}
	/**
		@return select element
	*/
	api.processForm.prototype.appendWFSConnection = function(name, opt_element){
		var element = opt_element || this.formContainer;
		var selectelement = goog.dom.createDom("select", {'name': name, 'class': 'select-selectFeatures'}, goog.dom.createDom('option', {'value': 'none'}, goog.dom.createTextNode("Select additional layer")));
		var select = new api.htmlblock(selectelement, this.data, null, 
			function(){
				var data = this.data.arr;
				for(var i = 0, ii=data.length; i<ii; i++){
					var item = data[i];
					var option = goog.dom.createDom("option", {'value': i}, goog.dom.createTextNode(item["title"]));
					goog.dom.appendChild(this.element, option);
				}
			},
			null,
			function(){
				var element = this.element;
				goog.events.listen(this.data, api.dynamicTypes.PUSH, function(e){
					var item = e.element;
					var option = goog.dom.createDom("option", {'value': this.arr.length - 1}, goog.dom.createTextNode(item["title"]));
					goog.dom.appendChild(element, option);
				});
				
			}
		);
		this.blocks.push(select);
		select.display();
		goog.dom.appendChild(element, select.element)
		return select.element;
	}
	
	
	goog.exportProperty(api.processForm.prototype,'processInputPusher', api.processForm.prototype.processInputPusher);
	goog.exportProperty(api.processForm.prototype,'writeExecuteObject', api.processForm.prototype.writeExecuteObject);
	goog.exportProperty(api.processForm.prototype,'addDataToFormData', api.processForm.prototype.addDataToFormData);
	goog.exportSymbol('api.processForm.metadata', api.processForm.metadata);
	goog.exportSymbol('api.processForm.errors', api.processForm.errors);
	goog.exportSymbol('api.processForm.epsg', api.processForm.epsg);
	goog.exportSymbol('api.processForm.formElement', api.processForm.formElement);
	goog.exportSymbol('api.processForm.formContainer', api.processForm.formContainer);
	goog.exportSymbol('api.processForm.processInput', api.processForm.processInput);
	goog.exportSymbol('api.processForm.formData', api.processForm.formData);
	
	
