goog.provide("api.Styler");
goog.require("goog.dom");
goog.require("goog.events");
goog.require("api.htmlblock");
goog.require('api.styles');

api.Styler = function() {
  goog.base(this, document.querySelector("div#overlay"), null, null, null, null, function() {
    this.overlayContainer = this.element.querySelector(
      ".app-main-overlay-container"
    );
    goog.events.listen(
      this.element.querySelector("#closeButton"),
      goog.events.EventType.CLICK,
      function(e) {
        this.hide();
      },
      false,
      this
    );
  });

  this.sorting = [];

  this.hide = function() {
    this.data = null;
    goog.dom.removeNode(this.formElement);
		this.formElement = null;
    goog.dom.classlist.add(this.element, "nodisplay");
  };
};
goog.inherits(api.Styler, api.htmlblock);

api.Styler.prototype.setSelect = function(feature) {
  var properties = feature.getProperties();
  var props = Object.keys(properties);
  var select = goog.dom.createDom("select", {
    name: "attribute",
    class: "styler-selectAttribute"
  });
  for (var i = 0, ii = props.length; i < ii; i++) {
    var prop = props[i];
    if (prop === "geometry" || prop === "Style" || prop === "ramp" || prop === "ramptype" || prop === "rampint" || prop === "boundedBy") {
      continue;
    } else {
      goog.dom.appendChild(
        select,
        goog.dom.createDom(
          "option",
          { "value": prop },
          goog.dom.createTextNode(prop)
        )
      );
    }
  }
  return select;
};

api.Styler.prototype.setLegends = function(formElement){
    goog.dom.appendChild(formElement, goog.dom.createDom("p", {"class": "styler-colorsLabel"}, goog.dom.createTextNode("Select color ramp ")))
    var images = ["r1", "r2", "r3", "r4"];
    for (i = 0, ii = images.length; i<ii; i++){
      var image = images[i];
      var radio = goog.dom.createDom("input", {
        "type": "radio",
        "class": "styler-selectRamp",
        "name": "ramp",
        "value": image,
        "checked": image === "r1" && "checked"
      });
      var legend = goog.dom.createDom("img", {
        "alt": image,
        "src": "../images/" + image + ".png"
      });
      var label = goog.dom.createDom("label", {}, radio, legend);
      var div = goog.dom.createDom(
        "div",
        {
          "class": "styler-containerRamp"
        },
        label
      );
      goog.dom.appendChild(formElement, div);
    }
  }

api.Styler.prototype.setIntervals = function(){
  var select = goog.dom.createDom("input", {
    "type": "number",
    "name": "intervals",
    "class": "styler-selectInterval",
    "min": "4",
    "max": "9",
    "value": "4"
  });
  goog.events.listen(select, ["blur"], function(e){
    var val = parseInt(e.target.value);
    if(val){
      e.target.value = (val < 4 || val > 9) ? 4 : val;
    }else{
      e.target.value = 4;
    }
  });
  var labelselect = goog.dom.createDom("label", {"class": "styler-intervalsLabel"}, goog.dom.createTextNode("Number of intervals "), select);
  return labelselect;
}

api.Styler.prototype.applyStyleToFeature = function(
  features,
  intervalues,
  selectedAttribute
) {
  for (var i = 0, ii = features.length; i < ii; i++) {
    var feature = features[i];
    var prop = feature.getProperties()[selectedAttribute];
    if (!isNaN(parseFloat(prop)) && isFinite(prop)) {
      prop = parseFloat(prop);
    }
    // iterate over interval values and define style
    for (var j = 0, jj = intervalues.length; j < jj; j++) {
      var interval = intervalues[j];
      if (prop <= interval) {
            feature.set("ramp", j);
            break;
        }
      }
    }
  };


api.Styler.prototype.getType = function(){
  var number = false;
  for(var i = 0, ii=this.sorting.length; i < ii; i++){
    var item = this.sorting[i];
    if(!isNaN(parseFloat(item)) && isFinite(item)){
      number = true;
      break;
    }
  }
  return number;
}

api.Styler.prototype.getOtherIntervals = function(selected){
  var len = this.sorting.length;
  var size = len / selected;
  var intervalues = [];
  for(var i = 1, ii = selected; i < ii; i++){
    var topush = this.sorting[i * Math.round(size)];
    if(topush){
      intervalues.push(topush);
    }else{
      intervalues.push(this.sorting[len - 1]);
    }
  }
  intervalues.push(this.sorting[len - 1]);
  return intervalues;
}

api.Styler.prototype.getNumberIntervals = function(selected){
  var intervalues = [];
  var len = this.sorting.length;
  var min = parseFloat(this.sorting[0]);
  var max = parseFloat(this.sorting[len - 1]);
  var intervallen = max - min;
  var size = intervallen / selected;
  for(var i = 1, ii = selected; i < ii; i++){
    var topush = (i * size) + min;
    if(topush < max){
      intervalues.push(topush);
    }else{
      intervalues.push(this.sorting[len - 1]);
    }
  }
  intervalues.push(this.sorting[len - 1]);
  return intervalues;
}

api.Styler.prototype.applyStyle = function(e){
  this.sorting.length = 0;
  var form = e.target;
  var intervals = form.elements["intervals"];
  var attribute = form.elements["attribute"];
  var ramp = form.elements["ramp"].value || "r1";
  var selectedAttribute = attribute[attribute.selectedIndex].value;
  var selectedIntervals = intervals.value;
  var selectedIntervalsIn = (parseInt(selectedIntervals));
  var selectedIntervalsInt = selectedIntervalsIn || 4;
  var features = this.data["layer"].getSource().getFeatures();
  for(var i = 0, ii = features.length; i < ii; i++){
    var feature = features[i];
    var value = feature.getProperties()[selectedAttribute];
    this.sorting.push(value);
  }
  // sort it baby
  var number = this.getType();
  this.sorting = this.sorting["sort"](function(a, b){
    if(number){
      a = parseFloat(a);
      b = parseFloat(b);
    }
    if ( a < b )
       return -1;
     if ( a > b )
       return 1;
     return 0;
  })
  if(number){
    var intervalues = this.getNumberIntervals(selectedIntervalsInt);
  }else{
    var intervalues = this.getOtherIntervals(selectedIntervalsInt);
  }
  this.applyStyleToFeature(features, intervalues, selectedAttribute);
  api.styles["createStyle"](ramp, selectedIntervalsInt);
  this.data["layer"].setStyle(function(feature, resolution){
    return [api.styles["actualStyle"][feature.get("ramp")]];
  })
  e.preventDefault();
  return false;
}

api.Styler.prototype.show = function() {
  if (goog.dom.classlist.contains(this.element, "nodisplay")) {
    goog.dom.classlist.remove(this.element, "nodisplay");
    app.showLoader(
      this.element,
      null,
      null,
      null,
      "loaderBack loaderBackShow app-main-overlay-loader"
    );
    this.data["edit"].clearInteractions();
    var layer = this.data["layer"];
    var first = layer.getSource().getFeatures()[0];
    var select = this.setSelect(first);
    var labelselect = goog.dom.createDom("label", {"class": "styler-selectLabel"}, goog.dom.createTextNode("Field "), select);
    this.formElement = goog.dom.createDom(
      "form",
      {
        "id": "stylerform",
        "class": "grey-border-radius",
        "name": "getstyle",
        "action": ""
      },
      labelselect,
      this.setIntervals(),
      goog.dom.createDom('input', {'type':'submit', 'value':'Apply'})
    );
    this.setLegends(this.formElement);
    goog.dom.appendChild(this.overlayContainer, this.formElement);
    goog.events.listen(this.formElement, goog.events.EventType.SUBMIT, this.applyStyle, false, this);
  }
};
