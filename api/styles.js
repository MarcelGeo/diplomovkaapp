goog.provide("api.styles");
goog.require("ol.style.Style");
goog.require("ol.style.Stroke");

var stroke = new ol.style.Stroke({
  color: "#3399CC",
  width: 1.25
});

goog.provide('api.stylesproto');

api.stylesproto = {
  "style": function(rgba) {
    return new ol.style.Style({
      image: new ol.style.Circle({
        fill: new ol.style.Fill({
          color: rgba
        }),
        stroke: stroke,
        radius: 5
      }),
      fill: new ol.style.Fill({
       color: rgba
      }),
      stroke: stroke
    });
  }
}

api.styles = {
  "actualStyle": [],
  "r1def":[
    [254, 254, 230],
    [236, 180, 72],
    [200, 136, 56]
  ],
  "r2def":[
    [232, 152, 44],
    [248, 216, 160],
    [249, 201, 93]
  ], 
  "r3def": [
    [239, 247, 103],
    [139, 247, 167],
    [95, 247, 211]
  ], 
  "r4def": [
    [175, 247, 127],
    [139, 247, 167],
    [95, 247, 211]
  ],
  "createStyle": function(schema, intervals){
    this["actualStyle"].length = 0;
    var percentage = [];
    for(var i = 1, ii=intervals; i <=ii; i++){
      var perc = (i-1)/(intervals-1);
      var key = schema + "def";
      var defSchema = this[key];
      var rsch = defSchema[0];
      var gsch = defSchema[1];
      var bsch = defSchema[2];
      var r,g,b;
      if(perc <= 0.5){
        r = parseInt(rsch[0] + perc * 2 * (rsch[1] - rsch[0]));
        g = parseInt(gsch[0] + perc * 2 * (gsch[1] - gsch[0]));
        b = parseInt(bsch[0] + perc * 2 * (bsch[1] - bsch[0]));
      }else{
        r = parseInt(rsch[1] + (perc * 2 - 1)*(rsch[2] - rsch[1]));
        g = parseInt(gsch[1] + (perc * 2 - 1)*(gsch[2] - gsch[1]));
        b = parseInt(bsch[1] + (perc * 2 - 1)*(bsch[2] - bsch[1]));
      }
      var rgba = "rgba(" + r + "," + g + "," + b + ", 0.8)";
      this["actualStyle"].push(api.stylesproto.style(rgba));
    }
  },
  "r1": {
    "4": [
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(254,236,200,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(254,236,200,0.8)"
        }),
        stroke: stroke
      }),
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(254,198,157,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(254,198,157,0.8)"
        }),
        stroke: stroke
      }),
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(246,144,109,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(246,144,109,0.8)"
        }),
        stroke: stroke
      }),
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(230,72,56,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(230,72,56,0.8)"
        }),
        stroke: stroke
      })
    ],
    "5": [
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(254,236,200,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(254,236,200,0.8)"
        }),
        stroke: stroke
      }),
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(254,208,168,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(254,208,168,0.8)"
        }),
        stroke: stroke
      }),
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(254,180,136,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(254,180,136,0.8)"
        }),
        stroke: stroke
      }),
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(242,126,96,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(242,126,96,0.8)"
        }),
        stroke: stroke
      }),
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(230,72,56,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(130,72,56,0.8)"
        }),
        stroke: stroke
      })
    ],
    "7": [
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(254,236,200,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(254,236,200,0.8)"
        }),
        stroke: stroke
      }),
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(254,217,178,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(254,217,178,0.8)"
        }),
        stroke: stroke
      }),
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(254,198,157,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(254,198,157,0.8)"
        }),
        stroke: stroke
      }),
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(254,180,136,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(254,180,136,0.8)"
        }),
        stroke: stroke
      }),
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(246,144,109,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(246,144,109,0.8)"
        }),
        stroke: stroke
      }),
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(238,108,82,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(238,108,82,0.8)"
        }),
        stroke: stroke
      }),
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(230,72,56,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(230,72,56,0.8)"
        }),
        stroke: stroke
      })
    ]
  },
  "r2": {
    "4": [
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(232,248,249,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(232,248,249,0.8)"
        }),
        stroke: stroke
      }),
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(178,226,217,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(178,226,217,0.8)"
        }),
        stroke: stroke
      }),
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(116,197,165,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(116,197,165,0.8)"
        }),
        stroke: stroke
      }),
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(44,160,93,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(44,160,93,0.8)"
        }),
        stroke: stroke
      })
    ],
    "5": [
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(232,248,249,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(232,248,249,0.8)"
        }),
        stroke: stroke
      }),
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(192,232,225,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(192,232,225,0.8)"
        }),
        stroke: stroke
      }),
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(152,216,201,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(152,216,201,0.8)"
        }),
        stroke: stroke
      }),
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(98,188,147,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(98,188,147,0.8)"
        }),
        stroke: stroke
      }),
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(44,160,93,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(44,160,93,0.8)"
        }),
        stroke: stroke
      })
    ],
    "7": [
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(232,248,249,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(232,248,249,0.8)"
        }),
        stroke: stroke
      }),
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(205,237,233,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(205,237,233,0.8)"
        }),
        stroke: stroke
      }),
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(178,226,217,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(178,226,217,0.8)"
        }),
        stroke: stroke
      }),
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(152,216,201,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(152,216,201,0.8)"
        }),
        stroke: stroke
      }),
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(116,197,165,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(116,197,165,0.8)"
        }),
        stroke: stroke
      }),
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(80,178,129,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(80,178,129,0.8)"
        }),
        stroke: stroke
      }),
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(44,160,93,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(44,160,93,0.8)"
        }),
        stroke: stroke
      })
    ]
  },
  "r3": {
    "4": [
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(239,139,95,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(239,139,95,0.8)"
        }),
        stroke: stroke
      }),
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(244,211,196,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(244,211,196,0.8)"
        }),
        stroke: stroke
      }),
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(199,220,235,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(199,220,235,0.8)"
        }),
        stroke: stroke
      }),
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(103,167,211,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(103,167,211,0.8)"
        }),
        stroke: stroke
      })
    ],
    "5": [
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(239,139,95,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(239,139,95,0.8)"
        }),
        stroke: stroke
      }),
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(243,193,171,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(243,193,171,0.8)"
        }),
        stroke: stroke
      }),
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(247,247,247,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(247,247,247,0.8)"
        }),
        stroke: stroke
      }),
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(175,207,229,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(175,207,229,0.8)"
        }),
        stroke: stroke
      }),
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(103,167,211,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(103,167,211,0.8)"
        }),
        stroke: stroke
      })
    ],
    "7": [
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(239,139,95,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(239,139,95,0.8)"
        }),
        stroke: stroke
      }),
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(241,175,145,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(241,175,145,0.8)"
        }),
        stroke: stroke
      }),
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(244,211,196,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(244,211,196,0.8)"
        }),
        stroke: stroke
      }),
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(247,247,247,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(247,247,247,0.8)"
        }),
        stroke: stroke
      }),
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(199,220,235,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(199,220,235,0.8)"
        }),
        stroke: stroke
      }),
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(151,193,223,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(151,193,223,0.8)"
        }),
        stroke: stroke
      }),
      new ol.style.Style({
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: "rgba(103,167,211,0.8)"
          }),
          stroke: stroke,
          radius: 5
        }),
        fill: new ol.style.Fill({
          color: "rgba(103,167,211,0.8)"
        }),
        stroke: stroke
      })
    ]
  },
  "r4": {
    "4": [
      api.stylesproto.style("rgba(175,139,191, 0.8)"),
      api.stylesproto.style("rgba(223,211,228, 0.8)"),
      api.stylesproto.style("rgba(207,228,204, 0.8)"),
      api.stylesproto.style("rgba(127,191,119, 0.8)")
    ],
    "5": [
      api.stylesproto.style("rgba(175,139,191, 0.8)"),
      api.stylesproto.style("rgba(211,293,219, 0.8)"),
      api.stylesproto.style("rgba(247,247,247, 0.8)"),
      api.stylesproto.style("rgba(187,219,183, 0.8)"),
      api.stylesproto.style("rgba(127,191,119, 0.8)")
    ],
    "7": [
      api.stylesproto.style("rgba(175,139,191, 0.8)"),
      api.stylesproto.style("rgba(199,175,209, 0.8)"),
      api.stylesproto.style("rgba(223,211,228, 0.8)"),
      api.stylesproto.style("rgba(247,247,247, 0.8)"),
      api.stylesproto.style("rgba(207,228,204, 0.8)"),
      api.stylesproto.style("rgba(167,209,161, 0.8)"),
      api.stylesproto.style("rgba(127,191,119, 0.8)")
    ]
  }
};
