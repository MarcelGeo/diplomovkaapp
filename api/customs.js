	goog.provide('api.customs');
	goog.require('ol.format.WPS');
	goog.require('ol.xml');
	
	api.customs.SimpleExtendObjectKeys = function(input_obj, value_objoropt_keys, opt_value){
		  /**
		     Function to extend input_obj with value_obj, using Object.keys(obj) or optionally added array of keys.
			 Example: ({a:1}, {a:3,b:3}) => {a:3,b:3}
		  */
		if(!goog.isArray(value_objoropt_keys)){
			var val_keys = ol.format.WPS.EX_objectKeysReturner(value_objoropt_keys);
		}else{
			var val_keys = value_objoropt_keys;
		}
	    var val_key;
		for(var l = 0, ll = val_keys.length; l < ll; l++){ // ak je vo value object object s hodnotami {Mimetype: ..., value:...}, tak musi preloopovat cez neho
			val_key = val_keys[l];
			if(!goog.isDef(opt_value)){
				goog.object.set(input_obj, val_key, value_objoropt_keys[val_key]);
			}else{
				goog.object.set(input_obj, val_key, opt_value); 
			}	
		}
		return input_obj;
	}
	api.customs.LiteralDataBBOXComplexValueAdd = function(obj, val_obj){
	/**
	   Fill values into execute object values depend on val_obj ({identifier of input:value} values
	*/
		    if(goog.isObject(obj)){
				var cont_keys = ol.format.WPS.EX_objectKeysReturner(obj),cont_key,value_in_valobj;
				for(var k = 0, kk = cont_keys.length; k< kk; k++){
					cont_key = cont_keys[k];
					if(goog.object.containsKey(val_obj,cont_key)){ // ak sa nachadza vo value key ako v inputobj a je to string, musi do value appendnut tuto hodnotu 
						value_in_valobj = val_obj[cont_key];
						if(value_in_valobj || value_in_valobj === 0){
							if(goog.isDef(value_in_valobj[0]) || !goog.isObject(value_in_valobj) || ol.xml.isDocument(value_in_valobj) || ol.xml.isNode(value_in_valobj)){ // ak je to string alebo number atd.
								obj[cont_key]["value"] = value_in_valobj;
								continue;
							}else{ // ak je to object, tak prejde cez tento object a nabin duje dane hodnoty do
								this.SimpleExtendObjectKeys(obj[cont_key], value_in_valobj);
							}
						}
					}
				} 
			}
		}
		api.customs.xmlToString = function(xmlData) { // function for writing xml string
            var xmlString;
            //IE
            if (window.ActiveXObject){
				xmlString = xmlData.xml;
            }
            // code for Mozilla, Firefox, Opera, etc.
            else{
                xmlString = (new XMLSerializer()).serializeToString(xmlData);
            }
            return xmlString;
		}