goog.provide('api.goog_'); // --def new methods for goog
goog.require('goog.asserts');
goog.require('goog.json');
	api.goog_.trygetResponseJson = function(r){
	/**
	Try parse response string, if success return object, else returns null
	@param r {string} response text
	@return {Object|null} Json or null if invalid string or number passed to goog.json.parse
	*/
		goog.asserts.assertString(r);
		var json;
		try{
			json = goog.json.parse(r);
		}catch(e){
			json = null;
		}
		if(!goog.isObject(json)){ // --if number --or string --return null
			return null;
		}else{
			return json;
		}
	}