	goog.provide('api.processConnector');
	goog.require('goog.events');
	goog.require('goog.events.EventType');
	goog.require('goog.dom');
	goog.require('goog.asserts');
	goog.require('api.dynamic');
	goog.require('api.dynamicTypes');
	//goog.require('appUrl');
	/**
	@constructor
	Create dropdown and form with defined events to process, url for dropdown + form... and data defined in form.
	@param container {Element} element to write
	@param uri {appUrl}
	@param describeF {Function} calling if dropdown is changed.
	*/
	api.processConnector = function(container, uri, describeF, base, name){
		/**
		define uri with special uris to describe ...
		*/
		this.baseUri = uri;
		this.UriCapabilities = this.baseUri.proxySet(["service", "version", "request"], ["WPS", "1.0.0", "GetCapabilities"]).proxyUri;
		//this.UriDescribe = uri.proxySet(["service", "version", "request", "identifier"], ["WPS", "1.0.0", "DescribeProcess", select.value]).proxyUri;
		/**
		define select with callback onchange.
		*/
		this.select = goog.dom.createDom('select', {'class':'selecter'}); // identifier of uri for wich call ajax requests
		goog.dom.appendChild(this.select, goog.dom.createDom('option', {value: "NONE"}, goog.dom.createTextNode("-Select available process-")));// append option into select
		goog.events.listen(this.select, goog.events.EventType.CHANGE, describeF /* call descibe proces*/, false, this);
		this.ul = container;
		this.selectDiv = goog.dom.createDom('div', {'class': 'selectDiv maxheightTrans maxheightPlus'}, this.select); // div with select
		/**
		Forms
		*/
		this.forms = new api.dynamic();
		goog.events.listen(this.forms, api.dynamicTypes.REMOVE, this.removeForm, false, this);
		goog.events.listen(this.forms, api.dynamicTypes.PUSH, this.addForm, false, this);
		/**
		If is it base processConnector 
		*/
		this.base = base || false;
		this.name = name || "";
	}
	/**
	Remove form from given element in api.processForm, it is callback on removing elements from this
	@param e {goog.events.Event}
	*/
	api.processConnector.prototype.removeForm = function(e){
		var form = e.element;
		goog.dom.removeNode(form.metadata);
		goog.dom.removeNode(form.formElement);
	}
	/**
	Add form to given element in form.
	@param e {goog.events.Event}
	*/
	api.processConnector.prototype.addForm = function(e){
		var form = e.element;
		goog.dom.append(form.formContainer, form.metadata, form.formElement);
	}
	/**
	* Clear forms from all api.processConnector.forms in connectorArray. Add form {api.processForm} to api.processConnector.forms in this.
	* When removing from forms {api.dynamic}, then removing also from DOM.
	* @param form {api.processForm}
	* @param connectorArray {Array.<api.processConnector>}
	* @api
	*/
	api.processConnector.prototype.renderForm = function(form, connectorArray){
		goog.asserts.assertArray(connectorArray);
		// --loop trought connectoArray with api.processConnector --then remove forms from this.forms
		for(var i = 0, ii = connectorArray.length; i < ii; i++){
			connectorArray[i].forms.clear();
		}
		this.forms.push(form);
	}

	api.processConnector.prototype.setInitialState = function(){
		this.forms.clear();
		this.select.selectedIndex = 0;
	}
	goog.exportProperty(api.processConnector.prototype,'renderForm', api.processConnector.prototype.renderForm);
