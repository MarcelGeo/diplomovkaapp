/**
 - Creating Blob object if possible
 - Creating anchor tag and click on it to download data from Blob
 - Possible to use it with data to make Blob url
*/
goog.provide('api.Downloading');
goog.require("goog.dom");
//goog.require()
/**
@param options {Object}:
	a {Element} link element
	filename {String=}
*/
api.Downloading = function(options){
	var options = options || {};
	this.a = options.element;
	if (options.filename != null){
		this.filename = options.filename;
	}
	else{
		this.filename = "download.txt";
	}
	this.blob;
}
/**
Create blob and url from blob to save.
*/
api.Downloading.prototype.createBlobInterface = function(data, opt_filetype){
	var filetype = opt_filetype || "text/plain;charset=UTF-8", blob;
	this.dataurl = "data:" + filetype + "," + encodeURIComponent(data);
	if(Blob !== undefined) {
		var blob = new Blob([data], {type: filetype});
		this.url = URL.createObjectURL(blob);
	}
	return blob;
}

api.Downloading.prototype.save = function(data, filetype, opt_filename_extension){
	var blob = this.createBlobInterface(data, filetype);
	if(window.navigator["msSaveOrOpenBlob"]){
		window.navigator["msSaveOrOpenBlob"](blob, this.filename + "." + opt_filename_extension); // return false by not starting download
		blob.msClose();
	}else{
		var a = this.a;
		if("download" in a){
			if(blob){ // failed to blob
				a.setAttribute("href", this.url);
				a.setAttribute("download", this.filename + "." + opt_filename_extension);
			}else{
				a.setAttribute("href", this.dataurl);
			}
		}else{ 
			if(FileReader !== undefined && blob){ // Safari
				var reader = new FileReader();
				reader.onloadend = function() {
					var base64Data = reader.result;
					// "data:attachment/text" + base64Data.slice(base64Data.search(/[,;]/));
					window.location.href = base64Data;
					};
				reader.readAsDataURL(blob);
				window.URL.revokeObjectURL(this.url);
			}else{ // old old old alternative
				window.location.href = this.dataurl;
			}
			return;
		}
		a.setAttribute("target", "_blank");
		var click = function(node) { 
			var event = new MouseEvent("click");
			node.dispatchEvent(event);
		}
		setTimeout(function() {
			click(a);
			goog.dom.setProperties(a, {"href": null, "download": null});
			blob = null;
			window.URL.revokeObjectURL(this.url);
		});
	}
}
