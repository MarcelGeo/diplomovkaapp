	goog.provide('api.dynamic');
	goog.provide('api.dynamicEvent');
	goog.provide('api.dynamicTypes');
	goog.require('goog.events.Event');
	goog.require('goog.events.EventTarget');
	goog.require('goog.array');
	api.dynamicTypes = {
		/**
		* Triggered when an item is added to the dynamic.
		* @event api.dynamicEvent#push
		*/
		PUSH: 'push',
		/**
		* Triggered when an item is removed from the dynamic.
		* @event api.dynamicEvent#remove
		*/
		REMOVE: 'remove'
	}
	/**
	@derived ol.Collection
	@constructor
	Event for goog.events.EventTarget.dispatchEvent
	@extends {goog.events.Event}
	*/
	api.dynamicEvent = function(type, opt_target, opt_element){
		goog.base(this, type, opt_target);
		this.element = opt_element; 
	}
	goog.inherits(api.dynamicEvent, goog.events.Event);
	/**
	@constructor 
	dynmaic array with events.
	@param opt_array {Array=}
	@extends {goog.events.EventTarget}
	*/
	api.dynamic = function(opt_array){
		goog.base(this);
		this.arr = opt_array || [];
	} 
	goog.inherits(api.dynamic, goog.events.EventTarget);
	
	/**
	Return element in specified index
	*/
	api.dynamic.prototype.get = function(index){
		var arr = this.arr;
		var length = arr.length - 1;
		if(index >= 0 || index < length){
			return arr[index]; 
		}else{
			return null;
		}
	}
	
	
	/**
	@return index of pushed element.
	*/
	api.dynamic.prototype.push = function(element){
		var length = this.arr.push(element);   
		this.dispatchEvent(new api.dynamicEvent(api.dynamicTypes.PUSH, this, element));
		return length - 1;
	}
	// --depracted --notused
	// api.dynamic.prototype.removeLast = function(){
		// var last = goog.array.peak(this.arr)//  --var --cache last element for event
		// goog.array.splice(this.arr, this.arr.length - 1);
		// this.dispatchEvent(new api.dynamicEvent(api.dynamicTypes.REMOVE, this, last));
	// }
	api.dynamic.prototype.removeAt = function(index){
		var last = this.arr[index]//  --var --cache index element for event
		goog.array.removeAt(this.arr, index);
		this.dispatchEvent(new api.dynamicEvent(api.dynamicTypes.REMOVE, this, last));
	}
	api.dynamic.prototype.insertAt = function(element, index){
		goog.array.insertAt(this.arr, element, index);
		this.dispatchEvent(new api.dynamicEvent(api.dynamicTypes.PUSH, this, element));
	}
	/**
	* Replace element at index.
	* @param newEl 
	* @param index {number}
	*/
	api.dynamic.prototype.replaceAt = function(newEl, index){
		var removed = goog.array.splice(this.arr, index, 1, newEl);
		this.dispatchEvent(new api.dynamicEvent(api.dynamicTypes.PUSH, this, newEl));
		this.dispatchEvent(new api.dynamicEvent(api.dynamicTypes.REMOVE, this, removed));
	}
	/**
	Clear all elements from array with removeAt, difference from ol.Collection.clear
	*/
	api.dynamic.prototype.clear = function(){
		for(var i = 0, ii = this.arr.length; i < ii; i++){
			this.removeAt(i);
		}
	} 
	goog.exportProperty(api.dynamic.prototype,'push', api.dynamic.prototype.push);
	goog.exportProperty(api.dynamic.prototype,'removeAt', api.dynamic.prototype.removeAt);
	goog.exportProperty(api.dynamic.prototype,'insertAt', api.dynamic.prototype.insertAt);
	goog.exportProperty(api.dynamic.prototype,'replaceAt', api.dynamic.prototype.replaceAt);
	goog.exportProperty(api.dynamic.prototype,'clear', api.dynamic.prototype.clear);
	