/**
Javascript for wps map client application.
Use case:
1. Create Map and controls
2. Create app object with methods for working with proces_input_cache: input into WriteExecute method of ol.format.WPS, which returns NODE for AJAX EXECUTE.
3. Connect WPS service url defined by user:
	a: connect url of wps service -> send request on PHP proxy -> if succes -> 4a  
4. Send AJAX requests on WPS service using PHP proxy (CORS) with user activities on form and map:
	a: GetCapabilities -> Create dropdown with available process -> User select process ->
						events: on change -> b
	b: Describe Process (request) -> bi:Create form with inputs into wps service -> User fill inputs ->
						events: validating inputs - text: 
														empty if required
														number or string
														script
								validating selectedFeatures from map:
														nothing selected if required 
																					-> bii
						  bii: Create proces_input_cache object from inputs -> c
	c: User push excute button -> app fills proces_input_cache with user defined data (app.formData object) after validation -> Create NODE from proces_input_cache created from form -> d
	d: Execute (request) with node from c -> e
	e: Test if Execute was secces on server side (valid geometry return or error writes) -> Write geometry to map into new ol.layer -> add to Layer Switcher
	*/	
	goog.provide('app');
	goog.require('api');
	goog.require('ol.interaction.Select');
	goog.require('ol.interaction.Modify');
	goog.require('ol.interaction.DragAndDrop');
	goog.require('ol.Map');
	goog.require('ol.View');
  goog.require('ol.Overlay');
	goog.require('ol.layer.Vector');
	goog.require('ol.source.Vector');
	goog.require('ol.source.MapQuest');	
	goog.require('ol.format.GPX');
	goog.require('ol.format.GeoJSON');
	goog.require('ol.format.IGC');
	goog.require('ol.format.KML');
	goog.require('ol.format.WPS');
	goog.require('ol.format.Ows');
	goog.require('ol.format.TopoJSON');
	goog.require('ol.geom.MultiPoint');
	goog.require('ol.geom.MultiLineString');
	goog.require('ol.geom.MultiPolygon');
	goog.require('ol.control.Attribution');
	goog.require('ol.control');
	goog.require('ol.interaction');
	goog.require('ol.control.EditTool');
	goog.require('ol.control.LayerSwitcher');
	goog.require('ol.control.Control');
	goog.require('ol.proj');
	goog.require('ol.format.WFS');
	goog.require('ol.format.WFSCapabilities');
	goog.require('goog.dom');
	goog.require('goog.dom.classlist');
	goog.require('goog.events');
	goog.require('goog.events.EventType');
	goog.require('goog.net.XhrIo');
	goog.require('goog.string');
	goog.require('goog.object');
	goog.require('goog.array');
	goog.require('goog.asserts');
	goog.require('api.processConnector');
	goog.require('api.processForm');
	goog.require('api.customs');
	goog.require('api.layout');
	goog.require('api.Downloading');
	goog.require('api.htmlblock');
	goog.require('api.goog_');
	goog.require('appUrl');
	goog.require('api.Styler');
	var app = {
		mimeTypeSupported:["gml/3.1.1","wfs-collection/1.1"],
		// errors:[], // object for return errors and disable execute submit button.
		dynamicBBoxArr:[],
		successUris:[], // array of success uris {api.processConnector}.
		additionalConnections: new api.dynamic(), // connections to WFS with important data to other structures...
		additionalWFS: [{url: 'http://gis.fns.uniba.sk/geoserver/sykora10/wfs', featureNS: "http://gis.fns.uniba.sk/geoserver/10sykora",featurePrefix: "sykora10"}],
		additionalWFSNS: ['http://gis.fns.uniba.sk/geoserver/10sykora'],
		olWFS: new ol.format.WFS(),
		"resultIds": []
	};
	app.wfs = new ol.format.WFSCapabilities({
		featureNamespaces: app.additionalWFSNS
	});
	
	
	//{ --section app 
	/**
	Validating input data values in text input and write 
	@return {Object} prepared input value with parameters of value:
		value {string|null},
		empty {true|false},
		requiredInput {string|null} if not defined required, is not required so null
	Using for error messages only.
	*/
	app.valTextInput = function(element){
		/**
		Data types to control.
		*/
		var patt = /int$|integer$|double$|float$|decimal$/ // ked da niekto xs:float alebo float, spravne sa prebehne dataTypeConvertor funkcia
		var datatype = element.getAttribute("data-datatype") || null; 
		var required = element.getAttribute("data-required") || null;
		var returner = {
			value:null,
			empty:true,
			requiredInput: required
		}
		var val = element.value;
		/*if(!app.emptyRequiredInput(val, required)){
			app.error(element, this.errorMessagesTexts["non_empty"], "empty");
		}else{
			app.unerror(element, "empty*/
		var reg = patt.exec(datatype);
		var FormValue = reg ? this.dataTypeConvertor[reg](val.replace(",", ".")) : val;
		if(required === "true"){
			if(goog.string.isEmptySafe(val)){
				this.error(element, this.errorMessagesTexts["non_empty"], "empty", "input_title nameid colored_border");
			}else{
				this.unerror(element, ["empty"]);
			}
			if(FormValue == null){
				this.error(element, this.errorMessagesTexts["bad_format"] + reg || "string", "format", "input_title nameid colored_border");
			}else{
				this.unerror(element, ["format"]);		
			}
		}else{
			if(goog.string.isEmptySafe(val) || FormValue != null){	
				returner.empty = true; // maybe is empty, not possible
				this.unerror(element, ["format"]);
			}else{
				this.error(element, this.errorMessagesTexts["bad_format"] + reg || "string", "format", "input_title nameid colored_border");
				returner.empty = false;
			}
		}
		returner.value = FormValue;
		return returner;
	};
	 //{ --section --def new projection for proj4
	proj4.defs("EPSG:102067","+proj=krovak +lat_0=49.5 +lon_0=24.83333333333333 +alpha=30.28813972222222 +k=0.9999 +x_0=0 +y_0=0 +ellps=bessel +towgs84=485,169.5,483.8,7.786,4.398,4.103,0 +units=m +no_defs");
  app.epsg102067 = ol.proj.get('EPSG:102067');
	app.epsg102067.setExtent([-598360.49, -1324572.24,-164132.22,-1144343.40]);
	app.epsg102067.setWorldExtent([16.84,47.73, 22.56, 49.61]);
	//} --endsection
	/*
	{ --section events functions on inputs defined in HTML
	*/
	/*
	} --endsection
	*/
	app.errorMessagesTexts = {
	/**
	Object, which contains error messages and information messages for app.
	*/
		   "projection": "Output will be in projection: ",
		   "mime": "Used output mimetype: ",
		   "emptyLiteralData": "Result without content",
		   "notresult": "WPS is without result.",
		   "unsupported_format": "This application doesn´t support formats in added WPS service",
		   "unsupported_this_format": "This application doesn´t support one of format in this input required. We don´t support raster formats",
		   "unsupported_this_crs": "This application doesn´t support CRS in this BBOX (Choose other CRS in map)",
		   "unsupported_projection": "This application doesnt´t support projection in added WPS service",
		   "bbox_dynamicaly": "The BBOX is calculating dynamically from map window view",
		   "non_empty": "This value is required for executing process",
		   "bad_format": "This value is in ",
		    "nochoosed_proces": "Please choose one of the supported process",
		   "not_valid_url": "Url is not valid: try http://example.com/ or our geoserver process",
		   "not_responding": "WPS did not start response",
		   "error_request": "WPS returns ",
		   "error_format": "WPS did not return getCapabilities in XML",
		   "error_formatDescribe": "WPS did not return describeProcess in XML",
		   "error_formatExecute": "WPS did not return valid response",
		   "error_formatExecute_bad": "WPS did not return response in valid format for this app (GML or WFS Collection)",
		   "error_timeout": "WPS service timed out / returns error",
		   "error_exception": "WPS exception",
		   "error_features": "Select features from map",
		   "selected_features": "Selected features: ",
		   "error_wpsexecute_failed": "Execution of WPS process failed."
		}
	app.dataTypeConvertor = {
		   "int": function(number){var num = parseInt(number); if(num || num === 0){return num;}else{return;};},
		   "integer": function(number){var num = parseInt(number); if(num || num === 0){return num;}else{return;};},
		   "double": function(number){var num = parseFloat(number); if(num || num === 0){return num;}else{return;};},
		   "decimal": function(number){var num = parseFloat(number); if(num || num === 0){return num;}else{return;};},
		   "float": function(number){var num = parseFloat(number); if(num || num === 0){return num;}else{return;};}
		};
	/**
	Find supported format or epsg with app and return it, else return undefined.
	@param Supported_Arr {Array} array from descibe process
	@param type {string} "CRS" or "MimeType"
	@return {Array.<Supported, textOfSupportedMimetype, EPSG>}
	*/
	app.supported_returner = function(Supported_Arr, type){
			// FIXME TO OBJECT
		    goog.asserts.assertArray(Supported_Arr);
			if(type === "MimeType"){
				var TypeSupported = this.mimeTypeSupported;
				var elementsToFind = [type, "Schema"];
				var mimetype = true;
			}else{
				var TypeSupported = this.epsgSupported;
				var elementsToFind = [type];
			}
		    for(var i = 0, ii = TypeSupported.length; i < ii; i++){ // --for every suported text in array, if is it supported, return options
				var possibleSupported = TypeSupported[i]; 
			    var patt = new RegExp(possibleSupported); 
			    for(var j = 0, jj = Supported_Arr.length; j < jj; j++){
					for(var k = 0, kk = elementsToFind.length; k < kk; k++){
						var typetofind = elementsToFind[k];
						if(mimetype && Supported_Arr[j][typetofind]){
							var mimetext = Supported_Arr[j][typetofind];
						}else if(!mimetype){
							var mimetext = Supported_Arr[j];
						}else{
							continue;
						}
						if(patt.test(mimetext)){
							return [Supported_Arr[j],mimetext, possibleSupported];				
						}
					}
				}
			}
			return;
		}
	//--depracted by new implementation
	// app.clearProcesInputCache = function(){
		// if(!goog.object.isEmpty(this.proces_input_cache)){
			// goog.object.clear(this.proces_input_cache);
		// } 
		// goog.array.clear(this.errors);
		// goog.object.clear(this.formData);
	// }
	app.createMultiFeature = function(features){
	/**
	Create one feature form array of features with multi geometry. 2 features with geomtry Point -> 1 feature with multi point.
	@param features {array.<ol.Feature>} array of features with same geometry type <ol.geom.GeometryType> and multi eqvivalent (point, multipoint)
	@return <ol.Feature> first feature with multigeometry
	*/
		var len = features.length;
		var firstFeature = features[0].clone();
		firstFeature.setId(features[0].get("id"));
		if(len < 2){
			return firstFeature;
		}
		var multiObject = {
			"Point": function(layout){return new ol.geom.MultiPoint([], layout)},
			"Polygon": function(layout){return new ol.geom.MultiPolygon([], layout)},
			"LineString": function(layout){return new ol.geom.MultiLineString([], layout)},
			"MultiPolygon": function(layout, firstGeometry){return firstGeometry.clone()},
			"MultiLineString": function(layout, firstGeometry){return firstGeometry.clone()},
			"MultiPoint": function(layout, firstGeometry){return firstGeometry.clone()}
		}
		var multiActions = {
			"Point": function(multigeom, point){multigeom.appendPoint(point);},
			"Polygon": function(multigeom, polygon){multigeom.appendPolygon(polygon);},
			"LineString": function(multigeom, line){multigeom.appendLineString(line);},
			"MultiPolygon": function(multigeom, polygon){
				var polygons = polygon.getPolygons();
				for(var i = 0, ii = polygons.length; i < ii; i++){
					multigeom.appendPolygon(polygons[i]);
				}
			},
			"MultiLineString": function(multigeom, line){
				var lines = line.getLineStrings();
				for(var i = 0, ii = lines.length; i < ii; i++){
					multigeom.appendLineString(lines[i]);
				}
			},
			"MultiPoint": function(multigeom, point){
				var points = point.getPoints();
				for(var i = 0, ii = points.length; i < ii; i++){
					multigeom.appendPoint(points[i]);
				}
			}
		}
		var patt = /MultiPolygon|MultiLineString|MultiPoint|Point|Polygon|LineString/; // return type of first feature and with this feature compare other features in array
		var firstGeometry = firstFeature.get("geometry");
		var firstType = patt.exec(firstGeometry.getType())[0];
		var firstLayout = firstGeometry.getLayout();
		var multigeom = multiObject[firstType](firstLayout, firstGeometry); // point -> multipoint
		multiActions[firstType](multigeom, firstGeometry); // geometry of first feature
		for(var i = 1; i < len; i++){
			var geometry = features[i].get("geometry");
			var type = patt.exec(geometry.getType())[0];
			if(type === firstType && geometry.getLayout() === firstLayout){ // if type and layout like first, append to multipoint
				multiActions[type](multigeom, geometry);
			}
		}
		firstFeature.setGeometry(multigeom);
		return firstFeature;
	}
	/**
	Calculate extent in EPSG:4326 or in defined epsg.
	@param size {ol.Size}
	@param opt_epsg {ol.proj.ProjectionLike=} default EPSG:4326
	*/
	app.calculateTransformedExtent = function(size, opt_epsg){
	   var view = this.map.getView();
	   var extent = view.calculateExtent(size);
	   return ol.proj.transformExtent(extent, view.getProjection().getCode(), opt_epsg || "EPSG:4326");
	}
	app.extentWriter = function(extent, opt_parent, opt_ids){
	/**
	Write extent into elements with ids min-x, min-y, max-x and max-y.
	@param extent {Array.<ol.Extent>}
	@param opt_parent {Element|undefined} container for extent
	@param opt_ids {Array|undefined} 
	*/
		var ids = opt_ids || ["min-x", "min-y", "max-x", "max-y"];
		for(var i = 0, ii = extent.length; i < ii; i++){
			var e = opt_parent ? goog.dom.findNode(opt_parent, function(n){return n.id === ids[i]}) : goog.dom.getElement(ids[i]); // --if parent -- findNode --else getElement
		    if(e){
				goog.dom.setTextContent(e, parseFloat(extent[i]).toFixed(3));
			}
			
		}
	}
	/**
		1. Append actual extent to bounding box control in map.
	    2. Append to formData of every form in app.successUris array with actual extent. 
	*/
	app.extentPusher = function(size){
		var transf_extent = this.calculateTransformedExtent(size); // need for bbox control in wgs.
		this.extentWriter(transf_extent);
		for(var i = 0, ii = this.successUris.length; i < ii; i++){ // --for every processConnector --action check --if every form.processInput in array contains BoundingBoxData key --true append to formData --else set key in formData to null
			//if(goog.object.containsKey(this.proces_input_cache, "BoundingBoxData")){
			var forms = this.successUris[i].forms.arr;
			for(var j = 0, jj = forms.length; j < jj; j++){
				var form = forms[j];
				transformedExtentFormEPSG = this.calculateTransformedExtent(size, form.epsg);
				if(goog.object.containsKey(form.processInput, "BoundingBoxData")){
					api.customs.SimpleExtendObjectKeys(form.formData, this.dynamicBBoxArr, transformedExtentFormEPSG)
		   // this.customs.LiteralDataBBOXComplexValueAdd(this.proces_input_cache["BoundingBoxData"], this.formData, transf_extent);
				}
			}
		}
	}
	app.error = function(element, errorText, errorid, opt_errorclass, opt_errorelementclass){
		var errorclass = opt_errorclass || "colored_border";
		var errorelementclass = opt_errorelementclass || "small_error_message";
		var parentElement = goog.dom.getParentElement(element);
		if(errorid === "xml"){
			var message = parentElement.querySelector("#result_message") || null;
			if(message){
				goog.dom.appendChild(message, goog.dom.createTextNode(errorText));
				var errorElement = goog.dom.createDom('p', {'id':errorid, 'class':errorelementclass}, goog.dom.createTextNode("Service Exception"));
			}else{
				var errorElement = goog.dom.createDom('p', {'id':errorid, 'class':errorelementclass}, goog.dom.createDom("span", {}, goog.dom.createTextNode(errorText)), goog.dom.createDom("p", {}, goog.dom.createTextNode("Service Exception")));
			}
		}else{
			var errorElement = goog.dom.createDom('p', {'id':errorid, 'class':errorelementclass}, goog.dom.createTextNode(errorText));
		}
		//var existingErrorElement = goog.dom.getElement(errorid);
		// if(!goog.dom.findNode(parentElement, function(n){
		     // return n.id === errorid;
		// }))
		if(!parentElement.querySelector('#' + errorid)){
			goog.dom.appendChild(parentElement, errorElement);
		}
		goog.dom.classlist.set(parentElement, errorclass);
	}
	app.unerror = function(element, errorid, class_to_add){
	/**
	Multiple events call error and this is clearer of error elements with id.
	@param element {element} element for which function find parent element in html tree
	@param errorid {array} ids of elements to remove
	*/
		var parentElement = goog.dom.getParentElement(element);
		for(var i = 0, ii = errorid.length; i < ii; i++){ // loop trought ids of error elements
			// var errorNode = goog.dom.findNode(parentElement, function(n){
				// return n.id === errorid[i];
			// });
			var errorNode = parentElement.querySelector('#' + errorid[i]);
			if(errorNode){ //--if --display errorNode
				goog.dom.removeNode(errorNode);
				goog.dom.classlist.remove(parentElement, "colored_border");
				if(errorid[i] === "xml"){
					var message = parentElement.querySelector("#result_message");
					goog.dom.removeChildren(message);
				}
				if(class_to_add){
					goog.dom.classlist.set(parentElement, class_to_add);
				}
			}
		}
	}
	app.showLoader = function(element, opt_button, opt_loaderbutton_class, opt_button_text, class_to_show){
	/**
	Display loader for getCapa, Describe and Execute process for specifed element. If button arguments are not defined, only loading gif.
	@param element {element} where to find loader element and change opacity of conntent
	@param opt_button {element|undefined} button which must be disabled and changed class 
	@param opt_loaderbutton_class {string|undefined} class to add to submit button
	@param opt_button_text {string|undefined}
	@return element with we replace button
	*/
		var imgloader = element.querySelector('.loaderBack');
		var showclass = class_to_show || 'loaderBack loaderBackShow';
		if(imgloader.getAttribute("data-loader") !== "true"){
			imgloader.setAttribute("data-loader", "true");
			goog.dom.classlist.set(imgloader, showclass); // --display loader image
			goog.dom.classlist.add(element, 'opaciter'); // --style change opacity of loaders content 
			//var elementToAdd = goog.dom.createDom('div', {'class': opt_loaderbutton_class}, goog.dom.createTextNode(opt_button_text || 'Connect'));
			if(opt_button){
				opt_button.value = opt_button_text || opt_button.value;
				opt_button.disabled = true;
				//goog.dom.replaceNode(elementToAdd, opt_button); // --hide button and --add div button
				if(opt_loaderbutton_class){
					goog.dom.classlist.add(opt_button, opt_loaderbutton_class);
				}
				return opt_button;
			}
		}
		return;
	}
	app.hideLoader = function(element, opt_button, opt_button_class, opt_button_text, class_to_hide){
	/**
	Hide loader. If button arguments are not defined, only loading gif.
	@param element {element} where to find loader element and change opacity of conntent
	@param opt_button {element} button which add 
	@param opt_button_class {String|undefined} class to remove to show submit button
	@param opt_button_text {string|undefined}
	@param class_to_hide= {element} class to loader div, which is used to hide ("nodisplay")
	*/
		var imgloader = element.querySelector('.loaderBack');
		if(imgloader.getAttribute('data-loader') === 'true'){
			if(opt_button){
				opt_button.value = opt_button_text || opt_button.value;
				opt_button.disabled = false;
				if(opt_button_class){
					goog.dom.classlist.remove(opt_button, opt_loaderbutton_class);
				}
			}
			var classtohide = class_to_hide || "loaderBack nodisplay";
			goog.dom.classlist.set(imgloader, classtohide);
			goog.dom.classlist.remove(element, 'opaciter');
			imgloader.setAttribute("data-loader", "false");	
		}
	}
    // --depracted
	// app.uriReturner = function(index){
	// /**
	// Return uri in succes uris array, which is created after validation of get Capabilities and creating dropdown with process. Index in array is name of dropdown and name of form with input  values.
	// @param index {number} index in successUris array.
	// */
		// return this.successUris[parseInt(index)]; // select uri for this WPS, depended on select name and index in array
	// }	
	/*
	var gmlFormat = new ol.format.WFS(
		 /*featureNS: "http://www.openplans.org/topp",
         featureType: 'states'
	);*/
	 //Transformovanie gml states do 3857 a zobrazenie do mapy cez ServerVector
	 /*
	 var gmlservector = new ol.source.ServerVector({
	     format: gmlFormat, //new ol.format.GeoJSON(),
		 loader: function(extent, resolution, projection){
		       var reqq = new ol.request.GETJQXHR({url:'topp-states-wfs.xml'});//{url:'/OL3/geojsTest.geojson.js', dataType:'json'}
			   reqq.done(function(data){
			       var features = gmlservector.readFeatures(data);
				   for(var i = 0; i < features.length; i++){
				        features[i].getGeometry().transform('EPSG:4326', 'EPSG:3857');
				        gmlservector.addFeature(features[i]);
				   }
                });
			   reqq.fail(function(jqXhr, textStatus){
			      console.log("FAIL Re"+textStatus);
			   })
			 //if(xhr.isSuccess){
			   // var resp = xhr.getResponseText();
			   // var xml = goog.dom.xml.loadXml(resp);
			   // console.log(xml);
			   // gmlserver.writeFeatures(gmlserver.readFeatures(xml));
			   //} 
		 },
		 preojection: "EPSG:3857"
	  });
      var gmlServerLayer = new ol.layer.Vector({
		 source: gmlservector
	    });
		*/
//var gmlReqq = ol.request.GETJQXHR({url:'topp-states-wfs.xml'});
	app.extentTextElementsPusher = function(opt_ids){
	/**
	@param opt_ids {Array} Ids of elements with coordinates
	@return paragraph with span element with specific ids for extent pushnig.
	*/
		var parr;
		if(!opt_ids){
			parr = [goog.dom.createDom('p', null, goog.dom.createTextNode("MINX:"), goog.dom.createDom('span', {id:"min-x"})), goog.dom.createDom('p', null, goog.dom.createTextNode("MINY:"), goog.dom.createDom('span', {id:"min-y"})), goog.dom.createDom('p', null,goog.dom.createTextNode("MAXX:"), goog.dom.createDom('span',{id:"max-x"})),goog.dom.createDom('p', null,goog.dom.createTextNode("MAXY:"), goog.dom.createDom('span', {id:"max-y"}))];
		}else{
			parr = [goog.dom.createDom('p', null, goog.dom.createTextNode("MINX:"), goog.dom.createDom('span', {id:opt_ids[0]})), goog.dom.createDom('p', null, goog.dom.createTextNode("MINY:"), goog.dom.createDom('span', {id:opt_ids[1]})), goog.dom.createDom('p', null,goog.dom.createTextNode("MAXX:"), goog.dom.createDom('span',{id:opt_ids[2]})),goog.dom.createDom('p', null,goog.dom.createTextNode("MAXY:"), goog.dom.createDom('span', {id:opt_ids[3]}))];
		}
		return parr;
	} 
	/*
	app.createBBOXCONTROL = function(){
	   /**
	   Create control for bounding box values in the map.
	   */
	   /*
	      var bboxbutton = goog.dom.createDom('button', {'class':"bbox-button", title:"Bounding Box"}, goog.dom.createDom('div', {'class': 'bbox-icon'}));
	      var bboxcont = goog.dom.createDom('div', {'class':"bboxcont grey-font"}, this.extentTextElementsPusher());	  
          var control_div = goog.dom.createDom('div', {'class':'bbox-control ol-unselectable ol-control'},bboxbutton, bboxcont); 
	      goog.events.listen(bboxbutton,goog.events.EventType.CLICK, function(e){ // listen for bbox button click and show/hide bbox content
	        // if(goog.dom.classlist.contains(bboxcont, "heighter")){
				// goog.dom.classlist.remove(bboxcont, "heighter");
			// }else{
				// goog.dom.classlist.set(bboxcont, "bboxcont heighter");
			// }
			goog.dom.classlist.toggle(bboxcont, "heighter");
			goog.dom.classlist.toggle(bboxcont, "grey-border-radius");
			});
		  goog.events.listen(bboxbutton, [goog.events.EventType.FOCUSOUT, goog.events.EventType.MOUSEOUT], function() {
				this.blur();
		  }, false);
		  return new ol.control.Control({element:control_div});
	   }
	*/
	app.SJTSKview = new ol.View({
		  projection: "EPSG:102067",
          center: ol.proj.transform([17.44656794714337,48.457516689235554], 'EPSG:4326', 'EPSG:102067'),
          zoom: 6,
		  maxZoom: 19
    });
	app.WGSviewOpts = {
		projection: "EPSG:3857",
    center: ol.proj.transform([17.44656794714337,48.457516689235554], 'EPSG:4326', 'EPSG:3857'),
    zoom: 10,
		maxZoom:19
	}
	app.WGSview = new ol.View(app.WGSviewOpts);

	app.geojson = new ol.source.Vector({
	  url:"../enviroZataze.geojson",
		format: new ol.format.GeoJSON(),
		attributions:[
			new ol.Attribution({
				html: "&copy;  MŽP SR a SAŽP"
			})
		]
		// projection: "EPSG:4326"
	});

	app.points_style = new ol.style.Style({
  image: new ol.style.Circle({
    radius: 5,
    fill: null,
    stroke: new ol.style.Stroke({ color: "red", width: 1 })
  })
});


	app.points = new ol.layer.Vector({
	  renderOrder:null,
	  source: app.geojson, // geo JSOn
		style: app.points_style,
		title: 'Contaminated Sites'
	});

	app.baseGroup = new ol.layer.Group({layers:[
	    new ol.layer.Tile({
			source: new ol.source.XYZ({
				//url: 'http://api.tiles.mapbox.com/v4/marcel-kocisek.gfi1fp3n/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFyY2VsLWtvY2lzZWsiLCJhIjoic1JmdGdxSSJ9.DPnFujDpzTJQiphq0djaxQ'
				url: 'https://api.mapbox.com/styles/v1/mapbox/light-v9/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFyY2VsLWtvY2lzZWsiLCJhIjoic1JmdGdxSSJ9.DPnFujDpzTJQiphq0djaxQ'
				}),
			title: "Light",
			visible: true
			}),
		new ol.layer.Tile({
			source: new ol.source.XYZ({
				attributions:[
					new ol.Attribution({
						html: "<a href=\"https://www.mapbox.com/about/maps/\" target=\"_blank\">&copy; Mapbox &copy; OpenStreetMap</a> <a class=\"mapbox-improve-map\" href=\"https://www.mapbox.com/map-feedback/\" target=\"_blank\">Improve this map</a>"
					})
				],
				// url: 'http://api.tiles.mapbox.com/v4/mapbox.satellite.json/?access_token=pk.eyJ1IjoibWFyY2VsLWtvY2lzZWsiLCJhIjoic1JmdGdxSSJ9.DPnFujDpzTJQiphq0djaxQ',
				urls:["http://a.tiles.mapbox.com/v4/mapbox.satellite/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFyY2VsLWtvY2lzZWsiLCJhIjoic1JmdGdxSSJ9.DPnFujDpzTJQiphq0djaxQ","http://b.tiles.mapbox.com/v4/mapbox.satellite/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFyY2VsLWtvY2lzZWsiLCJhIjoic1JmdGdxSSJ9.DPnFujDpzTJQiphq0djaxQ"]
				}),
			title: "Satellite MAPBOX",
			visible: false
			})
        ],
		title:"Base tiles"});
		
	app.defaultVectorGroup = new ol.layer.Group({
		layers:[]
	}); 
	/**
	Transform deault vector layer group to defined projection.
	@param to {ol.proj.ProjectionLike}
	@param opt_layergroup {ol.layer.Group} layer group to transform
	@return {ol.layer.Group}
	*/
	app.transformLayers = function(to){
		var togroup = this.defaultVectorGroup;
		var newlayers = [];
		togroup.getLayers().forEach(function(layer){
			var source = layer.getSource();
			var features = source.getFeatures();
			for(var i = 0, ii = features.length; i < ii; i++){
				features[i].transform(source.getProjection() || "EPSG:4326", to);	
			};
			source.setProjection(to);
			var newlayer = new ol.layer.Vector({source: source, style: layer.getStyle(), title: layer.get("title")}); // new Uid
			newlayers.push(newlayer);
		});
		var transformed = new ol.layer.Group({layers: newlayers, title: "Base Group"});
		return transformed;
	}
	app.dragAndDropInteraction = new ol.interaction.DragAndDrop({
		formatConstructors: [
			ol.format.GPX,
			ol.format.GeoJSON,
			ol.format.IGC,
			ol.format.KML,
			ol.format.TopoJSON
		]
	});
	app.dragAndDropInteraction.on('addfeatures', function(e){
		/**
		Add draged features to map and fit extent to this features.
		*/
		var vsource = this.addAndStyleLayer(e.features, e.file.name); // add features to map
		var map = this.map;
		map.getView().fitExtent(vsource.getExtent(), /** @type {ol.Size} */ (map.getSize()));
	}, app);
	app.selectInteraction = new ol.interaction.Select();
	app.modifyInteraction = new ol.interaction.Modify({
		features: app.selectInteraction.getFeatures(),
		deleteCondition: function(event) {
			return ol.events.condition.shiftKeyOnly(event) &&
			ol.events.condition.singleClick(event);
		}
	});
	app.formatSaver = function(format, config, features, downloading){
		var formater = config[format];
		var featurestosave = formater[1].writeFeatures(features, formater[2]);
		downloading.save(featurestosave, formater[3], formater[4]);
	}
	app.mainHTMLOverlay = new api.htmlblock(document.querySelector("div#overlay"), null, null, function(){
		if(goog.dom.classlist.contains(this.element, "nodisplay")){
			goog.dom.classlist.remove(this.element, "nodisplay");
			app.showLoader(this.element, null, null, null, 'loaderBack loaderBackShow app-main-overlay-loader');
			var source = this.data["lay"].getSource();
			var features = source.getFeatures();
			var proj = source.getProjection() || this.data["defaultProj"];
			var geojson = new ol.format.GeoJSON();
			var texts = {
				"geojson": [".Geo JSON", geojson, {}, "text/json;" + "charset=" + document.characterSet, "geojson"],
				"geojson4326": [".Geo JSON (EPSG:4326)", geojson, {dataProjection: "EPSG:4326", featureProjection: proj}, "text/json;" + "charset=" + document.characterSet, "geojson"],
				"kml": [".KML", new ol.format.KML(), {dataProjection: "EPSG:4326", featureProjection: proj}, "text/xml;" + "charset=" + document.characterSet, "kml"]
			}
			var configids = ["geojson", "kml"];
			var coordsids = ["4326", "3857"];
			this.custom_anchor = goog.dom.createDom("a", {"class": "nodisplay"});
			goog.dom.appendChild(document.body, this.custom_anchor);
			var downloading  = new api.Downloading({
				element: this.custom_anchor,
				filename: "export"
			});
			var animationsa = [];
			var select = goog.dom.createDom("select", {
    		"name": "format",
    		"class": "export-selectFormat"
  		});
			var coordselect = goog.dom.createDom("select", {
    		"name": "coord",
    		"class": "export-selectEpsg"
  		});
			var selectlabel = goog.dom.createDom('label',{"class": "export-selectFormatLabel"}, goog.dom.createTextNode("Select format "), select);
			var coordselectlabel = goog.dom.createDom('label',{"class": "export-selectEpsgLabel"}, goog.dom.createTextNode("Select coordinate system "), coordselect);
			for(var i = 0, ii = coordsids.length; i < ii; i++){
				var config = coordsids[i];
				var option = goog.dom.createDom("option", {
      		"value": config
    			},
    			goog.dom.createTextNode("EPSG:" + config)
    		);
    		goog.dom.appendChild(coordselect, option);
			}
			goog.events.listen(select, goog.events.EventType.CHANGE, function(e){
				var format = e.target.value;
				if(format === "kml"){
					coordselect.setAttribute("disabled", "disabled");
				}else{
					coordselect.removeAttribute("disabled");
				}
			} , false, app);
			for(var i = 0, ii = configids.length; i < ii; i++){
				var config = configids[i];
				var option = goog.dom.createDom("option", {
      		"value": config
    			},
    			goog.dom.createTextNode(texts[config][0])
    		);
    		goog.dom.appendChild(select, option);
			}
			this.formElement = goog.dom.createDom(
      	"form",
      	{
        	"id": "formatform",
        	"class": "opaciterMinus translateBack grey-font grey-border-radius",
        	"name": "getformat",
        	"action": ""
      	},
				selectlabel,
				coordselectlabel,
      	goog.dom.createDom('input', {'type':'submit', 'value':'Apply'})
    	);
			goog.dom.appendChild(this.overlayContainer, this.formElement);
			goog.events.listen(this.formElement, goog.events.EventType.SUBMIT, function(e){
				var coord, formater;
				var form = e.target;
  			var format = form.elements["format"].value;
				var coords = form.elements["coord"].value;
				if(coords === "3857"){
					coord = ""
				}else{
					coord = coords;
				}
				if(format === "kml"){
					this.formatSaver(format, texts, features, downloading);
				}else{
					this.formatSaver(format + coord, texts, features, downloading);
				}
				e.preventDefault();
				return false;
			} , false, app);
			/*
			for(var i = 0, ii = configids.length; i < ii; i++){ //
				var config = configids[i];
				var clickeranchor = goog.dom.createDom("a", {"href": "javascript:void(0);", "data-format": config, "class": "opaciterMinus translateBack grey-font grey-border-radius"}, goog.dom.createTextNode(texts[config][0]));
				goog.events.listen(clickeranchor, goog.events.EventType.CLICK, function(e){
					var format = e.target.getAttribute("data-format");
					this.formatSaver(format, texts, features, downloading);
				}, false, app);
				goog.dom.appendChild(this.ul, goog.dom.createDom("li", null, clickeranchor));
				animationsa.push(clickeranchor);
			}
			*/
			var formElement = this.formElement;
			window.setTimeout(function(){
					goog.dom.classlist.remove(formElement, "opaciterMinus");
					goog.dom.classlist.remove(formElement, "translateBack");
			}, 400);
			
		}
	}, function(){
		if(!goog.dom.classlist.contains(this.element, "nodisplay")){
			this.data = null;
			goog.dom.removeNode(this.formElement);
			this.formElement = null;
			goog.dom.removeNode(this.custom_anchor);
			this.custom_anchor = null;
			goog.dom.classlist.add(this.element, "nodisplay");
		}
	}, function(){ // creator
		this.overlayContainer = this.element.querySelector('.app-main-overlay-container');
		goog.events.listen(this.element.querySelector("#closeButton"), goog.events.EventType.CLICK, function(e){
			this.hide();
		}, false, this);
	});

	app.Styler = new api.Styler();

	app.EditTool = new ol.control.EditTool({
  	selectInteraction: app.selectInteraction,
  	pointLabel: "",
  	lineLabel: "",
  	polygonLabel: ""
	});


	app.map = new ol.Map({
        target: 'map',
		    controls: ol.control.defaults({attribution:false, zoomOptions: {zoomInLabel: "", zoomOutLabel: ""}}).extend(
				[ 
				new ol.control.Attribution({
					collapseLabel: '\u00AB'
				}),
				new ol.control.LayerSwitcher({
					selectInteraction:app.selectInteraction,
					selectTitle: "Select all features",
					buttonClass: 'ol-layerSwitcher-button-main-button-empty',
					buttonCreator: function(layer, container, layers, layersgroup){
						if (layers == null && layer instanceof ol.layer.Vector){
							var saver = goog.dom.createDom("button", {"type": "button", "class": "ol-layerSwitcher-button ol-layerSwitcher-save", "title": "Save as"});
							var styler = goog.dom.createDom("button", {"type": "button", "class": "ol-layerSwitcher-button ol-layerSwitcher-styler", "title": "Change style"});
							goog.events.listen(saver, goog.events.EventType.CLICK, function(e){
								app.mainHTMLOverlay.setData({"lay": layer, "defaultProj": this.getMap().getView().getProjection()});
								app.mainHTMLOverlay.display();
								app.hideLoader(app.mainHTMLOverlay.element);
							}, false, this);
							goog.events.listen(styler, goog.events.EventType.CLICK, function(e){
								app.Styler.setData({"layer": layer, "edit": app.EditTool});
								app.Styler.show();
								app.hideLoader(app.Styler.element);
							}, false, this);
						var remove = container.querySelector('.ol-layerSwitcher-remove');
						goog.dom.insertSiblingBefore(saver, remove);
						goog.dom.insertSiblingBefore(styler, remove);
						}
					}
				}),
				app.EditTool
			]),
		interactions: ol.interaction.defaults().extend([app.dragAndDropInteraction, app.selectInteraction, app.modifyInteraction]),
        layers: [app.baseGroup, app.points],
	    view: app.WGSview
    });
	app.epsgSupported = [/\d*$/.exec(app.WGSview.getProjection().getCode())[0], '4326'], // prvy je najpreferovanejsi, cize projekcia mapy
	app.extentPusher(app.map.get("size"));
	//{--section manipulating with map view
	/**
	Change view of the map based on projection changed with button.
	@param button {element}
	Outside
	*/
	app.toggleView = function(button){
		var name = button.name;
		var toDissable = document.querySelector("#options button:not(" + "#" + button.id + ")");
		if(goog.dom.classlist.contains(button, "dark-button")){ // --if clicked the same button --true nothing to do
			return false;
		}
		var layers = {
			'sjtsk': {
				view: app.SJTSKview
			},
			'wgs84': {
				view: app.WGSview,
				baseRasters: app.baseGroup
			}
		}; 
		var map = app.map;
		var view = layers[name].view;
		var rasters = layers[name].baseRasters;
		map.getLayers().clear();
		map.setView(view); // --change view of the map
		rasters && map.addLayer(rasters);
		map.addLayer(app.transformLayers(view.getProjection().getCode()));
		// toggling controls drawing control.
		var controls = map.getControls();
		for(i = 0, ii = controls.getLength(); i < ii; i++){
			var control = controls.getArray()[i];
			if(control instanceof ol.control.EditTool){
				control.clearInteractions();
				map.removeControl(control);
				map.addControl(new ol.control.EditTool({
					selectInteraction: app.selectInteraction
				}));
				break;
			}
		} 
		goog.dom.classlist.toggle(button, "dark-button");
		goog.dom.classlist.toggle(toDissable, "dark-button");
	}
	goog.exportSymbol('app.toggleView', app.toggleView);
	//} --endsection
	//{ --section functions to handle with requests
	/**
		Succes handler with callbacks and error messages calling.
		@param dataArr {Array.<responseXML, responseJSON, responseText>} If not null call success function
		@param opt_object {object} options to error functions and success function
		@param successf {function} function with arguments: data, <opt_object>.callbackopt
	*/
	app.successResponse = function(dataArr, opt_object, successf){
			goog.asserts.assertObject(opt_object);
			var exception = opt_object.exception || null;
			var nuller, data = dataArr[0]; //  --variables: nuller - handles if result in array is not null, text or extent, data - handles xml
			if(data != null){ // if xml first value in array is defined
				if(data.documentElement.nodeName === 'parsererror'){
					this.error(opt_object.errelement, opt_object.exceptiontext, "xml", opt_object.errclass);
					return nuller;
				}
				else if(data.firstChild.localName === exception){
					var ows = new ol.format.Ows();
					var exceptionreport = ows.readException({
						node: data.firstChild
					});
					if(exceptionreport){
						if(exceptionreport["Exception"]){
							var text = exceptionreport["Exception"]["ExceptionText"];
							if(goog.isDef(text)){
								this.error(opt_object.errelement, text, "xml", opt_object.errclass);
								return nuller;
							}else{
								this.error(opt_object.errelement, opt_object.exceptiontext, "xml", opt_object.errclass);
								return nuller;
							}
						}else{
							this.error(opt_object.errelement, opt_object.exceptiontext, "xml", opt_object.errclass);
							return nuller;
						}
					}else{
						this.error(opt_object.errelement, opt_object.exceptiontext, "xml", opt_object.errclass);
						return nuller;
					}
				}else{
					successf.call(opt_object.callbackopt, data.firstChild);
					nuller = true; // it was something returned
					return nuller;
				}
			}else{
				for(var i = 1, ii = dataArr.length; i < ii; i++){ // iterate over text and json responses
					data = dataArr[i];
					if(data != null){ // --if returned data are text or json (somethin is returned in response)
						successf.call(opt_object.callbackopt ,data);
						nuller = true;	
						break;
					}
				}
			}
			if(!nuller){
				this.error(opt_object.errelement, opt_object.notreturnedtext, "xml", opt_object.errclass);
			}
		}
	app.unsuccessResponse = function(data, opt_object){
	/**
	Handle with unsuccess request.
	@param data {goog.net.XhrIo} 
	@param opt_object {Object}
	*/
		
		if(data.getStatus() === -1){ // -1 if do not start responding
			this.error(opt_object.errelement, opt_object.notresponding, "responding", opt_object.errclass || false);
		}else{// other bad requests 
			this.error(opt_object.errelement, opt_object.badrequest, "responding", opt_object.errclass || false);
		} 
	}
	//} --endsection
	 ////////////////////////////// --new WPS constructor
	app.wps = new ol.format.WPS(/*{
	   url:app.url
	 }*/);
	//{ --section get capabilities handling 
	/**
	Create li element with select, close button, vertical slider.
	@param xml {doc} xml to parse and push process to select element.
	@this {api.processConnector}
	*/
	app.processSelectRenderer = function(xml){
		app.successUris.push(this); // --push to array
		var procesOfferings = app.wps.readGetCapabilities({ // parse GetCapabilities
			node: xml
		});
		if(goog.object.isEmpty(procesOfferings)){
			return;
		}
		//Creating elements
		var li = goog.dom.createDom('li');
		//var last = opt_object.i - 1;
		var a = goog.dom.createDom('a', {href:'javascript:void(0)', rel:'Hide / Show select'}, goog.dom.createTextNode(this.baseUri.uri.getDomain() + this.name));
		goog.events.listen(a, goog.events.EventType.CLICK, function(e){ // show/hide select dropdown
		/**
		@this {api.processConnector}
		*/
			e.preventDefault();
			goog.dom.classlist.toggle(this.selectDiv, 'maxheightPlus');
			return false;
		}, false, this);
		if(!this.base){ // --if base connector
			var closeButton = goog.dom.createDom('button', {id:'closeButton', type:'button', title:'Close connection'});		
			goog.events.listen(closeButton, goog.events.EventType.CLICK, function(e){
				/**
				Remove connection element, remove forms with name defined in connection select element. Clear object in successUris array with url.
				*/
				// var _this = e.target;
				// var parent = goog.dom.getParentElement(_this);
				goog.dom.removeNode(li); // remove element
				this.forms.clear() // --remove all forms in given connection, and then all forms unrender
				goog.array.remove(app.successUris, this); //remove this from array contained connections
				//var formsWithName = document.querySelectorAll("form#procesInputs[name='" + last.toFixed() + "']") // --using querySelectorAll -- in future may be more than one form
				//goog.object.clear(app.successUris[last]); // --remove uri from succes uris 
				// for(var i = 0, ii = formsWithName.length; i < ii; i++){ // --for forms with defined name --action --remove form with defined name
				// goog.dom.removeChildren(formsWithName[i]); 
				// }
			}, false, this);
			
		}
		
		// loop trought process in getCapabilities
		for(var i = 0, ii = procesOfferings["ProcessOfferings"].length; i < ii;i++){
			// append options into select
			var id = procesOfferings["ProcessOfferings"][i]['Identifier'];
			var title = procesOfferings["ProcessOfferings"][i]['Title'];
			goog.dom.appendChild(this.select, goog.dom.createDom('option', {value:id}, goog.dom.createTextNode(title)));// append option into select
		}
		goog.dom.append(li, a, closeButton, this.selectDiv); // apped all to li
		goog.dom.appendChild(this.ul, li); // append li with content to ul
	}
	// functions in events - onchange, onsubmit
	/**
	Validate URL given by user. And display errormessage. 
	@private
	@param form {Element}
	@param formClassName {string} Class name of error element
	@param uriValue {string} to validate.
	*/
	app.validateUrl = function(form, formClassName, uriValue){
		// strip html from url
		var StrippedString = uriValue.replace(/(<([^>]+)>)/ig,"");
		var uriobj = new goog.Uri(StrippedString); // for checking valid url
		var schemePath = /http|https/;
		// check for valid url with http and domain (not empty ...) and create construnctor
		if(uriobj.hasScheme() && uriobj.hasDomain() && schemePath.test(uriobj.getScheme())){
			return new appUrl(StrippedString);// call appi url constructor	
		}else{ // if not valid url - error
			this.error(form, this.errorMessagesTexts["not_valid_url"], "urlvalid", formClassName + " colored_border");			
			return false;
		}
	}
	app.divloader = document.querySelector('#loader');
	app.divloaderbutton = app.divloader.querySelector('form.urlLoader').elements[1];
	
	app.getFeaturesFromWFSCapabilities = function(capabilityObject, url, getFeatureOptions){
		var ftypelist = capabilityObject['FeatureTypeList'];
		for(var j = 0, jj = ftypelist.length; j < jj; j++){
			var capability = ftypelist[j];
			var ftype = capability["Name"];
			var ftitle = capability["Title"] || ftype;
			var splitted = ftype.split(':');
			var name = splitted.length > 1 ? splitted[1] : splitted[0];
			getFeatureOptions["featureTypes"] = [name];
			
			var getfeature = this.olWFS.writeGetFeature(getFeatureOptions); // write get feature xml node request
			
			if(getfeature){
				app.showLoader(app.divloader, app.divloaderbutton);
				var execute = new goog.net.XhrIo();
				goog.events.listen(execute, goog.net.EventType.COMPLETE, function(e){
				
					var reqq = e.target;
					if(reqq.isSuccess()){
						var data = reqq.getResponseXml();
						if(data){
							this["wfsdata"] = data;
							app.additionalConnections.push(this);
						}
					}
					app.hideLoader(app.divloader, app.divloaderbutton);
				}, false, {'title': ftitle});
				execute.send(url.proxySet().proxyUri, "POST", api.customs.xmlToString(getfeature), {"Content-type": "text/xml; charset=UTF-8;"});
		}
		// console.log(getfeature);
	}
}
	
	app.handleWFSCapabilities = function(connection){
		var urlproxy = new appUrl(connection["url"]);
		app.showLoader(app.divloader, app.divloaderbutton);
		var execute = new goog.net.XhrIo();
		goog.events.listen(execute, goog.net.EventType.COMPLETE, function(e){
			var reqq = e.target;
			if(reqq.isSuccess()){
				var data = reqq.getResponseXml();
				var source = this.this_.wfs.read(data);
				if(source){
					var conn = this.connection;
					var getfeature = {
						featureNS: conn["featureNS"],
						featurePrefix: conn["featurePrefix"],
						srsName: "EPSG:4326"
					}
					this.this_.getFeaturesFromWFSCapabilities(source, this['url'], getfeature);
				}
			}
			
		}, false, {connection: connection,url: urlproxy, this_: app});
		execute.send(urlproxy.proxySet(["service", "version", "request"], ["WFS", "1.1.0", "GetCapabilities"]).proxyUri);
		
	}
	/**
	Connect to array of app.additionalWFS connections array (api.dynamic)
	*/
	app.getWFS = function(opt_url){
		if(opt_url){ // preparing for api.dynamic in app.additionalWFS
			app.handleWFSCapabilities(opt_url);
			return;
		}
		var conns = this.additionalWFS;
		for(var i = 0, ii = conns.length; i < ii; i++){
			app.handleWFSCapabilities(conns[i]);
		}
		
	}
	/**
	Set url defined with user (text input) and send getCapabilities request, if success create select, else error messages. Call constructor for app.
	Test for maximum count fo connections.
	@param form {Element}  
	@param opt_uri {String|undefined}
	@this app
	*/
	app.getCapabilities = function(form, opt_uri, opt_auth_user, opt_name){
		// uri 
		// console.time("Parser Get capa");
		var ul = document.querySelector('#selectContainer'); // get ul
		if(app.successUris.length > 3){ // --if length of connections is more than 3 --action do nothing
			return false;
		}
		var formClassName = form.className;
		var button = form.elements[1];
		app.unerror(button, ["xml","responding","urlvalid"]); // remove error messages from other actions before. // remove text from empty request.
		var divloader = app.divloader;
		var baseconnector = false;
		var uriValue;
		var nameOfProcess = "";
		if(opt_uri){
			var uriValue = opt_uri;
			var baseconnector = true;
		}else{
			var uriValue = form.elements[0].value; // --var value of input with base url
		}
		app.showLoader(divloader, button);
		
		// { --section url validation and proxy setting
		var appiUrl = app.validateUrl(button, formClassName, uriValue);
		if(!appiUrl){
			app.hideLoader(divloader, button);
			return false;
		}
		if(opt_auth_user){
			appiUrl.setUserAuth(opt_auth_user);
		}
		if(opt_name){
			nameOfProcess = opt_name;
		}
		var request = new goog.net.XhrIo();// --new instance of request object
		// --var --new process LI and function on dropdown
		var processManipulation = new api.processConnector(ul, appiUrl, app.describeProcess, baseconnector, nameOfProcess);
		//appiUrl.proxySet(["service", "version", "request"], ["WPS", "1.0.0", "GetCapabilities"]);
		// } --endsection
		goog.events.listen(request, goog.net.EventType.COMPLETE, function(e){ // callback to request
			/**
			@this {api.processConnector}
			*/
			var data = e.target;
			if(data.isSuccess() && data.getStatus() === 200){
				app.successResponse([data.getResponseXml()], {
					errelement: button,
					errclass: formClassName + " colored_border",
					exception:'ExceptionReport',
					exceptiontext:app.errorMessagesTexts["error_exception"],
					notreturnedtext:app.errorMessagesTexts["error_format"],
					callbackopt:this
				},app.processSelectRenderer); // render connector li
				//alert("success");
				/*
				if(xml == null){ // if not xml - it could be 302 status, but in javascript 200
					app.error(form, appi.errorMessagesTexts["error_format"], "xml");
				}else{// push option to select or write exception error of WPS
					if(xml.firstChild.localName === 'ExceptionReport'){
						app.error(form, appi.errorMessagesTexts["error_exception"], "xml");
					}else{
						var index = appi.successUris.push(appiUrl); // push to array of success uris and call ajaxs with this uri
						app.procesSelectRenderer(xml, {urltext:appiUrl.uri.getDomain() + appiUrl.uri.getPath(), i:index}); // render connector li 
					}; 
				}	
				*/
			}else{ // not success
				app.unsuccessResponse(data, {
					errelement: button,
					errclass: formClassName + " colored_border",
					notresponding:app.errorMessagesTexts["not_responding"],
					badrequest:app.errorMessagesTexts["error_timeout"] + " " + data.getStatus()
				});
			    /*
				if(data.getStatus() === -1){ // -1 if do not start responding
					app.error(form, appi.errorMessagesTexts["not_responding"], "responding");
				}else{// other bad requests 
					//app.error(form, appi.errorMessagesTexts["error_request"] + data.getStatus(), "responding");
					app.error(form, appi.errorMessagesTexts["error_timeout"] + " " + data.getStatus(), "responding");
				} */
			}
			// --hide loader gif
			app.hideLoader(divloader, button);
			// console.timeEnd("Parser Get capa");	
		}, false, processManipulation);
		//send request
		var authUri = processManipulation.baseUri.auth || false;
		if(authUri){
			request.send(processManipulation.UriCapabilities, null, null, authUri);
		}else{
			request.send(processManipulation.UriCapabilities/*'http://localhost:80/geoserver/wps?service=WPS&version=1.0.0&request=GetCapabilities'*/);
		}
		return false;
	}
	//} --endsection
      /*
	  b.bindTo("value", myView).transform( 
	  function(){},
	  function(){return myView.calculateExtent(map.getSize())[0];});
	  */
	  
//Transofrmacia statov do 3857 a pridanie do mapy
/** --depracted states are removed form map
gmlReqq.done(function(data){
    var staticSource = new ol.source.StaticVector({
        doc: data,
		format: gmlFormat,
		projection: "EPSG:3857"
    });
	var gmlFeatures = staticSource.getFeatures();
    for(var i = 0; i < gmlFeatures.length; i++){
		gmlFeatures[i].setGeometry(gmlFeatures[i].getGeometry().transform('EPSG:4326', 'EPSG:3857'));
	}
	var gmlLayer = new ol.layer.Vector({   
		source: staticSource
	});
	app.map.addLayer(gmlLayer);
});*/
	//{ --section --name events on map
app.popupcontainer = document.querySelector('#popup');
app.popupcontent = app.popupcontainer.querySelector('#popup-content');        
app.popup = new ol.Overlay(/** @type {olx.OverlayOptions} */ ({
  element: app.popupcontainer,
  autoPan: true
}));

/**
@return ol.Coordinate as position for popup window
*/
app.calculatePopupPosition = function(geometry){
	var tocalculate, tocoordinate;
	if(typeof geometry.getGeometries === 'function'){ // test for GeometryCollection
		tocalculate = geometry.getGeometries()[0];
	}else{
		tocalculate = geometry;
	}
	// check for multi geometries
	if(typeof tocalculate.getPoint === 'function'){ 
		tocoordinate = tocalculate.getPoint(0);
	}else if(typeof tocalculate.getLineString === 'function'){
		tocoordinate = tocalculate.getLineString(0);
	}else if(typeof tocalculate.getPolygon === 'function'){
		tocoordinate = tocalculate.getPolygon(0);
	}else{
		tocoordinate = tocalculate;
	}
	//check for simple
	if(typeof tocoordinate.getCoordinateAtM === 'function'){
		var coords = tocoordinate.getCoordinates();
		var coordslen = Math.round(coords.length / 2);
		return coords[coordslen - 1];
	}if(typeof tocoordinate.getInteriorPoint === 'function'){
		return tocoordinate.getInteriorPoint().getCoordinates();
	}if(typeof tocoordinate.getCenter === 'function'){
		return tocoordinate.getCenter();
	}
	return tocoordinate.getCoordinates() || null; // Point
}

    app.map.on('singleclick', function(evt) {
	    var emap = evt.map;
		if(!emap.get("editing")){
			var features = []
			var efeature = emap.forEachFeatureAtPixel(evt.pixel,
				function(feature, layer) {
					features.push(feature);
				}
			);
			var len = features.length;
			if (len > 0) {
				app.popup.setMap(emap); // hack for ol.Overlay.prototype.UpdatePixel
				goog.dom.removeChildren(app.popupcontent);
				for(var i=0, ii = len;i<ii;i++){
					var feature = features[i];
					var properties = feature.getProperties();
					var geometry = feature.getGeometry();
					//var lay = geometry.getLayout();
					var poc = 0;
					// var coord = goog.isNumber(point[0]) ? point : evt.coordinate; // check if point, --if is number
					goog.object.forEach(properties, function(v, k, all){
						if(k !== "geometry" && k !== "Style" && k !== "ramp" && k !== "ramptype" && k !== "rampint" && k !== "boundedBy"){
							goog.dom.appendChild(app.popupcontent, goog.dom.createDom("p", goog.dom.createTextNode(k + " : " + v)));
						}else{
							poc++; // dont need style ... in popup
						}
					});
					/*
					if(goog.object.getCount(properties) > poc){
						goog.dom.appendChild(app.popupcontent, goog.dom.createDom("p",{'style': "font-size: .7em"}, goog.dom.createTextNode("Layout: " + lay)));
					}
					*/
				}
				// calculate for first feature position
				var first = features[0];
				var coordinate = app.calculatePopupPosition(first.getGeometry());
				if(!coordinate){
					emap.removeOverlay(app.popup);
					return;
				}
				app.popup.setPosition(coordinate);
				emap.addOverlay(app.popup);
			}else{
				emap.removeOverlay(app.popup);
			}
		}else{
			emap.removeOverlay(app.popup);
		}
	});
	app.map.on('moveend', function(evt){
	    var emap = evt.map;
		app.extentPusher(emap.get("size"));  
	});
	app.map.on('change:view', function(evt){
		/**
		Change supported epsg on changin view of the map.
		*/
		var emapview = evt.target.getView();
		var newepsg = /\d*$/.exec(emapview.getProjection().getCode())[0];
		this.epsgSupported.splice(0, 1, newepsg);
	}, app);  
	app.map.once("postrender", function(evt){
		if (goog.dom.classlist.contains(app.popupcontainer, "nodisplay")){
			goog.dom.classlist.remove(app.popupcontainer, "nodisplay");
		}
	})
    //} --endsection events
		/**
		@this {api.processForm}
		*/
		app.bboxcomplexOutputWriter = function(supported_obj, key, type, error_msg, prefix_text, div_to_append){
			var crs_mime = app.supported_returner(supported_obj[key]["Supported"], type);
			if(crs_mime){
				// naplnanie input objectu, ktory pojde dalej
				if(crs_mime[1] === crs_mime[0]){// ak sa crs alebo mime text zhoduje s polozkou v podporovanom array, tak je to len obycajny string, inak sa musi rovnat vsetkemu, co potrebujeme pre nas format 	
					var a = {}
					a[type] = crs_mime[1];
					this.processInputPusher(key,supported_obj["Identifier"],a); // je to vlastne text formatu, ktory podporujeme a pushneme ho do podporovaneho objectu
				}else{
				    this.processInputPusher(key,supported_obj["Identifier"],crs_mime[0]);
				}
				goog.dom.append(div_to_append, goog.dom.createDom('span', {'class':" resultId floater"}, goog.dom.createTextNode(supported_obj["Title"])), goog.dom.createDom('p', {'class': "output_message"}, goog.dom.createTextNode(prefix_text + crs_mime[1])));
			}else{
			    this.errors.push(false);
				goog.dom.appendChild(div_to_append, goog.dom.createDom('p', {'class': "error_message"}, goog.dom.createTextNode(error_msg)));
			}
		}
		/* --depracted
		function ComplexOutputObjectPusher(input_obj,dataInputsComplexData, value){
		    /**
			   Create object for ComplexData,LiteralData ... , f.e. {MimeType: ... , value: ...}
			
			var obj_return = appi.customs.SimpleExtendObjectKeys(input_obj, dataInputsComplexData);
		    obj_return["value"] = value ? value : null;
			return obj_return;
		}
		app.createExecuteObject = function(){
		    this.exObject = {};
		}
	*/
		/**
		@this {Object}:
			_this (api.processForm),
			index (number) index to which add the error false
		@param e {goog.events.Event}
		*/
		app.pushFeatures = function(e){
			var selected = app.selectInteraction.getFeatures();
			var button = e.target;
			app.unerror(button, ["features"]);
			if(selected.getLength() === 0){
				app.error(button, app.errorMessagesTexts["error_features"], "features", "input_title nameid colored_border");
				goog.object.set(this._this.formData, button.name.toString(), null); // --add nothing to object
				this._this.errors.replaceAt(false, this.index);
			}else{
				goog.dom.setProperties(button, {disabled:"disabled"}); // --start loader
				app.error(button, app.errorMessagesTexts["selected_features"] + selected.getLength().toString(), "features", "input_title nameid", "small_error_message blue_error_message"); //--not error message, only message ok.
				this._this.errors.replaceAt(true, this.index);
				var featuresToTransform = goog.array.clone(selected.getArray());
				var multi = this["mimetype"] !== "wfs-collection/1.1" ? [app.createMultiFeature(featuresToTransform)] : featuresToTransform;
				goog.object.set(this._this.formData, button.name.toString(), multi);// multi feature array
				button.removeAttribute("disabled"); // --end loader
				// alert(proces_input_cache["ComplexData"][this.name].value);
			}
		}
		/**
		@this {Object}:
			_this (api.processForm),
			index (number) index to which add the error false
		@param e {goog.events.Event}
		*/
		app.pushAdditionalNode = function(e){
			var select = e.target;
			var that = this._this;
			var data = that.data;
			var value = select.value;
			var name = select.name;
			var selectedIndex = value === 'none' ? -1 : parseInt(value);
			var nodeobj = data.get(selectedIndex);
			app.unerror(select, ["features"]);
			if(nodeobj){
				goog.object.set(that.formData, name, nodeobj["wfsdata"].firstChild);
				app.error(select, app.errorMessagesTexts["selected_features"] + nodeobj["title"], "features", "input_title nameid", "small_error_message blue_error_message"); //--not error message, only message ok.
				that.errors.replaceAt(true, this.index);
			}else{
				app.error(select, app.errorMessagesTexts["error_features"], "features", "input_title nameid colored_border");
				goog.object.set(that.formData, name, null); // --add nothing to object
				that.errors.replaceAt(false, this.index);
			}
		}
		
		/**
		Push form depended on process description.
		@param data {Node} 
		--FIXME for caching variables from description
		*/
		app.renderForm = function(data){
			var description = app.wps.readDescribeProcess({
				node: data
			});
			if(description == null){
				return;
			}
			//var formContainer = document.querySelector('#formContainer'); // --fix if more then one form created copy or createDom this element
			// var metadata = this.formContainer.querySelector('#metadata');
			// var inputdiv = this.formContainer.querySelector('#procesInputs');
			// inputdiv.name = this.selectname; // share url from select to form - valid url for request
			// --add text to head text
			var form = new api.processForm(document.querySelector('#formContainer'), {
				identifier: description["Identifier"],
				title: description["Title"] || "Process",
				desc:description["Abstract"] || "Selected process.",
				executeUri: this.baseUri,
				additional: app.additionalConnections
			}, app.executeProcess);
			var formE = form.formElement;
			// --add JSON to WPS execute
			//goog.object.set(form.processInput, "Identifier", description["Identifier"]); 
			// goog.dom.setTextContent(this.formContainer.querySelector('#header'), description["Title"] + "(" + description["Identifier"] + ")");
			// goog.dom.setTextContent(this.formContainer.querySelector('#proc_abstract'), );
			// // --
			// --remove mazanie divu s formularom, aby sa mohol vytvorit novy
			// goog.dom.classlist.contains(inputdiv, 'procesInput') && goog.dom.removeChildren(inputdiv);
			for(var i = 0, ii = description["DataInputs"].length; i < ii; i++){
				var dataInput = description["DataInputs"][i];
				var required = (dataInput['min'] !== "0");	// --var define if is input required		  
				var input_label = goog.dom.createDom('label',{"for": dataInput['Title']}, goog.dom.createTextNode(dataInput["Title"]), required && goog.dom.createDom('span',{id:"requiredStar"}, goog.dom.createTextNode("*"))); 
				var input_title = goog.dom.createDom('div', {'class':"input_title nameid"}, input_label);
				// --add elements to form
				goog.dom.append(formE, input_title, goog.dom.createDom('div', {'class': "input_abstract nameid grey-border-radius grey-font"}, goog.dom.createTextNode(dataInput.Abstract)));
				if(dataInput["LiteralData"]){
					// --add proces cache object - empty values for literal data
					form.processInputPusher("LiteralData", dataInput['Identifier'], {"DataType": dataInput["LiteralData"]["DataType"] || null,value:null});
					dataInput["LiteralData"]["AnyValue"] && (function(){
					var input = goog.dom.createDom('input', {type: "text", 'class': 'grey-font',name: dataInput["Identifier"], id: dataInput["Title"], value: dataInput["LiteralData"]["DefaultValue"] || "", 'data-datatype': dataInput["LiteralData"]["DataType"] || null, 'data-required':required.toString()});
					// --var index of error false, --if input is not required --then push true to errors. 
					var index = required ? form.errors.push(false) : form.errors.push(true);
					goog.events.listen(input, ["keyup", "change", "blur"], function(e){
						/**
						Event callback on text inputs.
						FIXME: for unhappy solution for validating
						FIXME: for unhappy solution for validating
						*/
						var _this = e.target;
						var val = app.valTextInput(_this);
						if(val.value != null){ // --if --def val --action testing for blur --and replace with true in errors array at index --else insert false at element.
							this.errors.replaceAt(true ,index);
							if(e.type === "blur"){
								_this.value = val.value || val.value === 0 ? val.value : "";
							}
						}else{
							if(val.requiredInput === "true"){
								// alert("required");
								this.errors.replaceAt(false, index);
							}else{
								if(val.empty){
									this.errors.replaceAt(true, index);
								}else{
									this.errors.replaceAt(false, index);
								}
							}
						}
					}, false, form);
					goog.dom.appendChild(input_title, input)
					})() || // --else test for allowed values
					dataInput["LiteralData"]["AllowedValues"] && 
					(function(){
						form.errors.push(true); // always true in errors for dropdown
						var select = goog.dom.createDom('select', {name: dataInput["Identifier"], id: dataInput["Title"], 'class': "procesSelect", 'data-required':required.toString(), 'data-datatype': dataInput["LiteralData"]["DataType"] || "AnyText"});
						if(!required){ // --if --not required --action append to select also choose value <option> --else need some value without choose value from
							goog.dom.appendChild(select, goog.dom.createDom('option', {value: 'none'}, goog.dom.createTextNode('Choose from '+ dataInput['Title'])));
						}
						var allowed = dataInput["LiteralData"]["AllowedValues"];
						for(var j = 0, jj = allowed.length; j < jj; j++){ // --for allowed array --action creating select elements
						    if(dataInput["LiteralData"]["DefaultValue"]){ // --if default value --action return select with selected
							    var selected = allowed[j] === dataInput["LiteralData"]["DefaultValue"] ? {selected: "selected"} : {};
							}else{
							    var selected = {};
							}
							goog.dom.appendChild(select, goog.dom.createDom('option', api.customs.SimpleExtendObjectKeys({value:allowed[j]}, selected), goog.dom.createTextNode(allowed[j])));
						}
						goog.dom.appendChild(input_title, select);
					})();
					continue;
				}else if(dataInput["ComplexData"]){
					// --return --var supported object with mimetype... , supported by app
					var supportedByApp = app.supported_returner(dataInput["ComplexData"]["Supported"], "MimeType");  // --var object with mimetype ... of supported mime type by this app
					// --depracted we have label instead jQuery("<span/>", {html:dataInput.Title+(required && " <span id='requiredStar'>*</span>" || ""), class:"floater"}).appendTo(input_title);
					if(supportedByApp == null){ 
						goog.dom.appendChild(input_title, goog.dom.createDom('p', {'class': "error_message"}, goog.dom.createTextNode(app.errorMessagesTexts["unsupported_this_format"])));
						form.errors.push(false);
						continue;
					}
					//
					form.processInputPusher("ComplexData", dataInput["Identifier"], supportedByApp[0]);
					var button = goog.dom.createDom('button', {id:"button-selectFeatures", type:"button", name: dataInput["Identifier"]}, goog.dom.createTextNode("Use selected"));
					// --def --var if required, must define error for remove submit button.
					var indexB = required ? form.errors.push(false) : form.errors.push(true);
					goog.events.listen(button, goog.events.EventType.CLICK, app.pushFeatures, false, {_this: form, "index": indexB, "mimetype": supportedByApp[2]});
					goog.dom.appendChild(input_title, button);
					if(supportedByApp[1] === "text/xml; subtype=wfs-collection/1.1"){
						var select = form.appendWFSConnection(dataInput["Identifier"], input_title);
						goog.events.listen(select, goog.events.EventType.CHANGE, app.pushAdditionalNode, false, {_this: form, index: indexB});
					}
					continue;
				}else if(dataInput["BoundingBoxData"]){
					var supportedByApp = app.supported_returner(dataInput["BoundingBoxData"]["Supported"], "CRS");
					if(supportedByApp == null){
						goog.dom.appendChild(input_title, goog.dom.createDom('p', {'class': "error_message"}, goog.dom.createTextNode(app.errorMessagesTexts["unsupported_this_crs"])));
						form.errors.push(false);
						continue;
					}
					// ak crs nie je object, tak sa rovna polozke v poli
					/*<wps:Input>
      <ows:Identifier>bounds</ows:Identifier>
      <wps:Data>
        <wps:BoundingBoxData dimensions="2">
          <ows:LowerCorner>21.0 45.0</ows:LowerCorner>
          <ows:UpperCorner>23.0 47.0</ows:UpperCorner>
        </wps:BoundingBoxData>
      </wps:Data>
    </wps:Input>*/
					// --if its value in array and --not object
					if(supportedByApp[0] === supportedByApp[1]){ 
						form.processInputPusher("BoundingBoxData", dataInput["Identifier"], {"CRS": supportedByApp[1]});
					}else{
						form.processInputPusher("BoundingBoxData",dataInput["Identifier"],supportedByApp[0]);
					}
					form.epsg = "EPSG:" + supportedByApp[2]; // --add new epsg for this form 
					app.dynamicBBoxArr.push(dataInput["Identifier"]);
					// --add bounding box to form data.
					// --add transformed extent to form data (EPSG)
					form.formData[dataInput["Identifier"]] = app.calculateTransformedExtent(app.map.get("size"), form.epsg); 
					goog.dom.appendChild(input_title, goog.dom.createDom('p', {'class': "output_message"}, goog.dom.createTextNode(app.errorMessagesTexts["bbox_dynamicaly"])));
					continue;					
				}
			}
			var result_error_title = goog.dom.createDom('div', {'class':"input_title nameid"}, goog.dom.createDom('span', {id: "result_message"}));
			app["resultIds"].length = 0;
			for(var i = 0, ii = description["ProcessOutputs"].length; i < ii; i++){
				var processOutput = description["ProcessOutputs"][i];
				app["resultIds"].push(processOutput["Identifier"]);
				var input_title = goog.dom.createDom('div', {'class':"input_title nameid"});
				goog.dom.appendChild(formE, input_title);
				if(processOutput["ComplexOutput"]){
					app.bboxcomplexOutputWriter.call(form, processOutput, "ComplexOutput", "MimeType", app.errorMessagesTexts["unsupported_format"], app.errorMessagesTexts["mime"], input_title);
				}
				else if(processOutput["BoundingBoxOutput"]){
					app.bboxcomplexOutputWriter.call(form,processOutput, "BoundingBoxOutput", "CRS", app.errorMessagesTexts["unsupported_projection"], app.errorMessagesTexts["projection"], input_title);   
					var result = goog.dom.createDom('p', {id: processOutput["Identifier"], 'class':"clearer"});
					goog.dom.appendChild(input_title, result);
				}
				else if(processOutput['LiteralOutput']){
					form.processInputPusher("LiteralOutput", processOutput["Identifier"], {}); // --FIX if two outputs 
					goog.dom.append(input_title, goog.dom.createDom('span', {'class':"resultId"}, goog.dom.createTextNode(processOutput["Title"])), goog.dom.createDom('span', {'id': processOutput["Identifier"]}));
				}else{
					goog.dom.append(result_error_title, goog.dom.createDom('p', {'class': "error_message"}, goog.dom.createTextNode(app.errorMessagesTexts["notresult"]))); // -- Result is not defined in WPS
				}	
			}
			// --display metadata div
			// -- depracted by form constructor goog.dom.classlist.set(metadata,'metadat');
			// --display form for process
			//goog.dom.classlist.set(formE, 'procesInput');
			this.renderForm(form, app.successUris);
			goog.dom.append(formE, result_error_title, form.submitButton);
			// console.timeEnd("Describe");
		}
		/**
		Call request for describe process.
		@this {app.processConnector}
		*/
		app.describeProcess = function(e){
			var select = e.target;
			// var opt = {
				// formContainer: document.querySelector('#formContainer'),
				// //selectname: select.name
			// };
			if(select.value == "NONE"){
				this.forms.clear();
				return false;
			}
			var divloader = document.querySelector('#loader');
			app.showLoader(divloader);

			// --depractedthis.clearProcesInputCache(); // --delete all in cache
			// create path for proxy.php and set to proxyUri
	    var actualUri = this.baseUri.proxySet(["service", "version", "request", "identifier"], ["WPS", "1.0.0", "DescribeProcess", select.value]).proxyUri;
			var request = new goog.net.XhrIo();
			// send request with callback
			goog.events.listen(request, goog.net.EventType.COMPLETE, function(e){ // call ajax with valid url for this WPS depended on select name
				// console.time("Describe");
				var data = e.target;
				var errorelement = goog.dom.getParentElement(select);
				app.unerror(select, ["xml", "responding"]); // remove error messages from other actions before. // remove text from empty request.
				if(data.isSuccess() && data.getStatus() === 200){ // --success request --call app.successResponse with with callback --call
					app.successResponse([data.getResponseXml()], {
						errelement:errorelement,
						exception:'ExceptionReport',
						exceptiontext:app.errorMessagesTexts["error_exception"],
						notreturnedtext:app.errorMessagesTexts["error_formatDescribe"],
						callbackopt:this
					}, app.renderForm);
				}else{
					app.unsuccessResponse(data, {
					errelement:errorelement,
					notresponding:app.errorMessagesTexts["not_responding"],
					badrequest:app.errorMessagesTexts["error_timeout"] + " " + data.getStatus()
					});
				}
				// --hide loader gif
				app.hideLoader(divloader);
			}, false, this);
			var authUrl = this.baseUri.auth || false;
			if(authUrl){
				request.send(actualUri, null, null, authUrl);
			}else{
				request.send(actualUri/*'http://localhost/geoserver/wps?service=WPS&version=1.0.0&request=DescribeProcess&identifier=JTS%3Abuffer'*/);
			}
		}
		//{ --section execute --name submitting form with inputs values and adding result to html or map
			//{ --section adding and styling layers
		/**
		Add features to map
		@param features {Array.<ol.Feature>}
		@param opt_name {String}
		@return {ol.source.Vector}
		*/
		app.addAndStyleLayer = function(features, opt_name){
			var map = this.map;

			var vsource = new ol.source.Vector({
				features: features,
				projection: map.getView().getProjection().getCode()
				// strategy: ol.loadingstrategy.tile(ol.tilegrid.createXYZ({
					// maxZoom: 19
				// }))
			});
			var vectorLayer = new ol.layer.Vector({
				source: vsource,
				title: opt_name
			});
			map.addLayer(vectorLayer);
			map.getView().fitExtent(vsource.getExtent(), /** @type {ol.Size} */ (map.getSize()));
			return vsource;
		}
			//} --endsection 
		/**
		Read features, extent or string and push new layer to map, or to specifed element with result.
		@param data {Node|Object|string} response in specifed format
		@this {Object}:
			resultEl {element} element to display error
			resultOutEl {element} element to display output (if text...)
			this_ : app
		*/
		app.readFeaturesExtentLiteral = function(data){
			for (var k = 0, kk = this.this_["resultIds"].length; k<kk; k++){ // removing previous outputs
				var id = this.this_["resultIds"][k];
				var resulter = this.inputForm.querySelector('#' + id);
				resulter && resulter.firstChild && goog.dom.removeChildren(resulter);
			}
			var response = this.this_.wps.readExecuteResponse(data); // --return features .... string
			if (!response){
				this.this_.error(this.resultEl, this.this_.errorMessagesTexts['error_formatExecute_bad'], 'xml', 'input_title nameid colored_border');
				return;
			}
			var status = response["Status"];
			if(status){
				if(status["ProcessFailed"]){
						var text = status["ProcessFailed"]["Exception"]["ExceptionText"];
						if(goog.isDef(text)){
							this.this_.error(this.resultEl, text, 'xml', 'input_title nameid colored_border');
							return;
						}else{
							this.this_.error(this.resultEl, this.this_.errorMessagesTexts['error_wpsexecute_failed'], 'xml', 'input_title nameid colored_border');
							return;
						}
					this.this_.error(this.resultEl, this.this_.errorMessagesTexts['error_wpsexecute_failed'], 'xml', 'input_title nameid colored_border');
					return;
				}
			}
			var map = this.this_["map"];
			var laylen = map.getLayers().getLength();
			var processOutputs = response["ProcessOutputs"];
			for (var i = 0, ii = processOutputs.length; i < ii; i++){
				var processOutput = processOutputs[i];
				var Data = processOutput["Data"];
				if(Data){
					var ident = processOutput["Identifier"];
					var resultOutEl = this.inputForm.querySelector('#' + ident);
					var literal = Data["LiteralData"];
					var bbox = Data["BoundingBoxData"];
					var complex = Data["ComplexData"];
					if(literal){
						if(literal["value"] != null){
							goog.dom.append(resultOutEl, goog.dom.createTextNode(literal["value"])); // append to result element
						}else{
							this.this_.error(resultOutEl, this.this_.errorMessagesTexts['emptyLiteralData'], 'xml', 'input_title nameid colored_border');
						}
					}else if(bbox){ 
						var paragraphs = this.this_.extentTextElementsPusher(); // --def ids for non wgs
						goog.dom.append(resultOutEl, paragraphs); // --function append paragraphs to result
						this.this_.extentWriter(bbox, resultOutEl); // --function append coordinates to elements in result
					}else if(complex){
						var complexvalue = complex["value"];
						if (complexvalue){
							if(goog.isArray(complexvalue)){ // --if ol.Extent || array.<ol.Feature>
								if(complexvalue.length < 1 || !(complexvalue[0] instanceof ol.Feature)){ // --if nothing parsed
									this.this_.error(this.resultEl, this.this_.errorMessagesTexts['error_formatExecute_bad'], 'xml', 'input_title nameid colored_border');
									return;
								}else{ // --if array.<ol.Feature>
									this.this_.addAndStyleLayer(complexvalue, ident + laylen);
								}
							}else if(complexvalue instanceof ol.Feature){ // --if ol.Feature
								if(complexvalue.getGeometry() == null){
									this.this_.error(this.resultEl, this.this_.errorMessagesTexts['error_formatExecute_bad'], 'xml', 'input_title nameid colored_border');
									return;
								}
								this.this_.addAndStyleLayer([complexvalue], ident + laylen);
							}else{
								this.this_.error(this.resultEl, this.this_.errorMessagesTexts['error_formatExecute_bad'], 'xml', 'input_title nameid colored_border');
							}
						}else{
							this.this_.error(this.resultEl, this.this_.errorMessagesTexts['error_formatExecute_bad'], 'xml', 'input_title nameid colored_border');
						}
					}
				}else{
					this.this_.error(this.resultEl, this.this_.errorMessagesTexts['error_formatExecute_bad'], 'xml', 'input_title nameid colored_border');
				}
			}	
		}
		/**
		Function which submits a form.
		*/
		app.executeProcess = function(e){
		    //{ -- section --name iterate over forms --depracted iterating if nothing changes - one form
		    // console.time("Execute");
			var form = e.target;
			e.preventDefault();
			var buttonLoader = app.showLoader(form, this.submitButton, "off_div");
			this.addDataToFormData(app.valTextInput, app) // fill form data object with data from inputs.value (text, dropdowns)
			var process = this.writeExecuteObject(); // pri vnorenych trochu inak
			var wpsExResult = app.wps.writeExecute({ // write execute object
					"objectExecute": process,
					"ResponseDocument": true,
					"ResponseDocumentAttributes": {
						"status": false,
						"storeResponseDocument": false,
						"lineage": false
					}
				});
				//{ --section --name Send execute xml to server
			var execute = new goog.net.XhrIo();// --new instance of request object 
			goog.events.listen(execute, goog.net.EventType.COMPLETE, function(e){
				var reqq = e.target;
				var resultElement = form.querySelector('#result_message');
				app.unerror(resultElement, ["xml", "responding"]); // remove error messages from other actions before. // remove text from empty request.
				if(reqq.isSuccess() && reqq.getStatus() === 200){ // --success request --call app.successResponse with with callback --call
					var datas = [reqq.getResponseXml(), api.goog_.trygetResponseJson(reqq.getResponseText()), reqq.getResponseText()]; // -- array to loop trought returned data in response
					app.successResponse(datas, {
						errelement:resultElement, // --fix if we want to use more than one outputs
						errclass: "input_title nameid colored_border",
						exception: "ExceptionReport",
						exceptiontext:app.errorMessagesTexts["error_exception"],
						notreturnedtext:app.errorMessagesTexts["error_formatExecute"],
						callbackopt:{resultEl: resultElement, inputForm: form, this_: app}
					}, app.readFeaturesExtentLiteral);
				}else{
					app.unsuccessResponse(reqq, {
					errelement:resultElement,  
					notresponding:app.errorMessagesTexts["not_responding"],
					badrequest:app.errorMessagesTexts["error_timeout"] + " " + reqq.getStatus()
					});
					/*else{
						 --ready app.unsuccessResponse(data, {
							errelement:form,
							notresponding:appi.errorMessagesTexts["not_responding"],
							badrequest:appi.errorMessagesTexts["error_timeout"] + " " + data.getStatus()
						});
					}*/
					/* --using debug of response
					if(reqq.isSuccess()){
						console.log(reqq.getResponseXml());
					}else{
						console.log("errcode"+reqq.getLastErrorCode() + "status"+reqq.getStatus() + reqq.getStatusText() + "error" + reqq.getLastError());// here goes app.error
					}*/
				}
					app.hideLoader(form, this.submitButton);
			}, false, this);
			// var actualUri = this.uriReturner(parseInt(form.name));
				// proxy changed to accept also route parameter
			var authUri = this.baseUri.auth || false;
			if(authUri){
				var header = {"Content-type": "text/xml; charset=UTF-8;", "Authorization": authUri["Authorization"]};
				execute.send(/*"http://localhost/geoserver/wps"*/this.executeUri, "POST", 
				/*'<wps:Execute xmlns:wps="http://www.opengis.net/wps/1.0.0" version="1.0.0" service="WPS" xsi:schemaLocation="http://www.opengis.net/wps/1.0.0 http://schemas.opengis.net/wps/1.0.0/wpsAll.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:wfs="http://www.opengis.net/wfs" xmlns:ows="http://www.opengis.net/ows/1.1" xmlns:gml="http://www.opengis.net/gml" xmlns:ogc="http://www.opengis.net/ogc" xmlns:wcs="http://www.opengis.net/wcs/1.1.1" xmlns:xlink="http://www.w3.org/1999/xlink"><ows:Identifier xmlns:ows="http://www.opengis.net/ows/1.1">gs:Count</ows:Identifier><wps:DataInputs><wps:Input><ows:Identifier xmlns:ows="http://www.opengis.net/ows/1.1">features</ows:Identifier><wps:Data><wps:ComplexData mimeType="text/xml; subtype=wfs-collection/1.1"><wfs:FeatureCollection xsi:schemaLocation="http://www.opengis.net/wfs http://schemas.opengis.net/wfs/1.0.0/WFS-basic.xsd http://mapserver.gis.umn.edu/mapserver" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ogc="http://www.opengis.net/ogc" xmlns:gml="http://www.opengis.net/gml" xmlns:wfs="http://www.opengis.net/wfs" xmlns:ms="http://mapserver.gis.umn.edu/mapserver"><gml:boundedBy><gml:Box srsName="EPSG:4326"><gml:coordinates>-0.768746,47.003018 3.002191,47.925567</gml:coordinates></gml:Box></gml:boundedBy><gml:featureMember><ms:polygon fid="1"><gml:boundedBy><gml:Box srsName="EPSG:4326"><gml:coordinates>-0.768746,47.003018 0.532597,47.925567</gml:coordinates></gml:Box></gml:boundedBy><ms:msGeometry><gml:MultiPolygon srsName="EPSG:4326"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>-0.318987,47.003018 -0.768746,47.358268 -0.574463,47.684285 -0.347374,47.854602 -0.006740,47.925567 0.135191,47.726864 0.149384,47.599127 0.419052,47.670092 0.532597,47.428810 0.305508,47.443003 0.475824,47.144948 0.064225,47.201721 -0.318987,47.003018 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs><gml:innerBoundaryIs><gml:LinearRing><gml:coordinates>-0.035126,47.485582 -0.035126,47.485582 -0.049319,47.641706 -0.233829,47.655899 -0.375760,47.457196 -0.276408,47.286879 -0.035126,47.485582 </gml:coordinates></gml:LinearRing></gml:innerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ms:msGeometry><ms:ogc_fid>1</ms:ogc_fid><ms:name>My Polygon with hole</ms:name><ms:id>0</ms:id></ms:polygon></gml:featureMember><gml:featureMember><ms:polygon fid="2"><gml:boundedBy><gml:Box srsName="EPSG:4326"><gml:coordinates>1.511919,47.088176 3.002191,47.882988</gml:coordinates></gml:Box></gml:boundedBy><ms:msGeometry><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>1.625463,47.357844 1.511919,47.741057 1.880938,47.882988 2.420275,47.797830 2.789295,47.485582 3.002191,47.457196 2.874453,47.088176 2.178993,47.343651 1.625463,47.357844 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ms:msGeometry><ms:ogc_fid>2</ms:ogc_fid><ms:name>My simple Polygon</ms:name><ms:id>0</ms:id></ms:polygon></gml:featureMember><gml:featureMember><ms:polygon fid="3"><gml:boundedBy><gml:Box srsName="EPSG:4326"><gml:coordinates>0.000000,45.000000 2.000000,47.000000</gml:coordinates></gml:Box></gml:boundedBy><ms:msGeometry><gml:MultiPolygon srsName="EPSG:4326"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>0.000000,45.000000 2.000000,45.000000 2.000000,47.000000 0.000000,47.000000 0.000000,45.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs><gml:innerBoundaryIs><gml:LinearRing><gml:coordinates>0.500000,45.500000 1.500000,45.500000 1.500000,46.500000 0.500000,46.500000 0.500000,45.500000 </gml:coordinates></gml:LinearRing></gml:innerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ms:msGeometry><ms:ogc_fid>3</ms:ogc_fid><ms:name>my polygon with hole</ms:name><ms:id>3</ms:id></ms:polygon></gml:featureMember></wfs:FeatureCollection></wps:ComplexData></wps:Data></wps:Input></wps:DataInputs><wps:ResponseForm><wps:RawDataOutput><ows:Identifier xmlns:ows="http://www.opengis.net/ows/1.1">result</ows:Identifier></wps:RawDataOutput></wps:ResponseForm></wps:Execute>'*/api.customs.xmlToString(wpsExResult), 
				/*'<wps:Execute xmlns:wps="http://www.opengis.net/wps/1.0.0" version="1.0.0" service="WPS" xsi:schemaLocation="http://www.opengis.net/wps/1.0.0 http://schemas.opengis.net/wps/1.0.0/wpsAll.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:wfs="http://www.opengis.net/wfs/1.1.0" xmlns:ows="http://www.opengis.net/ows/1.1" xmlns:gml="http://www.opengis.net/gml" xmlns:ogc="http://www.opengis.net/ogc" xmlns:wcs="http://www.opengis.net/wcs/1.1.1" xmlns:xlink="http://www.w3.org/1999/xlink"><ows:Identifier xmlns:ows="http://www.opengis.net/ows/1.1">gs:Count</ows:Identifier><wps:DataInputs><wps:Input><ows:Identifier xmlns:ows="http://www.opengis.net/ows/1.1">features</ows:Identifier><wps:Data><wps:ComplexData mimeType="text/xml; subtype=wfs-collection/1.1"><wfs:FeatureCollection xmlns:wfs="http://www.opengis.net/wfs" xsi:schemaLocation="http://www.opengis.net/wfs http://schemas.opengis.net/wfs/1.1.0/wfs.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><featureMembers xmlns="http://www.opengis.net/gml" xsi:schemaLocation="http://www.opengis.net/gml http://schemas.opengis.net/gml/3.1.1/profiles/gmlsfProfile/1.0.0/gmlsf.xsd"><wpsapp xmlns="http://localhost/OL3"><geometry><MultiPoint xmlns="http://www.opengis.net/gml"><pointMember><Point><pos>1947445.8788035866 6184656.847417211</pos></Point></pointMember><pointMember><Point><pos>1939076.0242063215 6189548.817227392</pos></Point></pointMember></MultiPoint></geometry><Madari>20</Madari><Slovaci>5</Slovaci></wpsapp></featureMembers></wfs:FeatureCollection></wps:ComplexData></wps:Data></wps:Input></wps:DataInputs><wps:ResponseForm><wps:RawDataOutput><ows:Identifier xmlns:ows="http://www.opengis.net/ows/1.1">result</ows:Identifier></wps:RawDataOutput></wps:ResponseForm></wps:Execute>',*/
					header
				);
			}else{
				execute.send(/*"http://localhost/geoserver/wps"*/this.executeUri, "POST", 
				/*'<wps:Execute xmlns:wps="http://www.opengis.net/wps/1.0.0" version="1.0.0" service="WPS" xsi:schemaLocation="http://www.opengis.net/wps/1.0.0 http://schemas.opengis.net/wps/1.0.0/wpsAll.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:wfs="http://www.opengis.net/wfs" xmlns:ows="http://www.opengis.net/ows/1.1" xmlns:gml="http://www.opengis.net/gml" xmlns:ogc="http://www.opengis.net/ogc" xmlns:wcs="http://www.opengis.net/wcs/1.1.1" xmlns:xlink="http://www.w3.org/1999/xlink"><ows:Identifier xmlns:ows="http://www.opengis.net/ows/1.1">gs:Count</ows:Identifier><wps:DataInputs><wps:Input><ows:Identifier xmlns:ows="http://www.opengis.net/ows/1.1">features</ows:Identifier><wps:Data><wps:ComplexData mimeType="text/xml; subtype=wfs-collection/1.1"><wfs:FeatureCollection xsi:schemaLocation="http://www.opengis.net/wfs http://schemas.opengis.net/wfs/1.0.0/WFS-basic.xsd http://mapserver.gis.umn.edu/mapserver" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ogc="http://www.opengis.net/ogc" xmlns:gml="http://www.opengis.net/gml" xmlns:wfs="http://www.opengis.net/wfs" xmlns:ms="http://mapserver.gis.umn.edu/mapserver"><gml:boundedBy><gml:Box srsName="EPSG:4326"><gml:coordinates>-0.768746,47.003018 3.002191,47.925567</gml:coordinates></gml:Box></gml:boundedBy><gml:featureMember><ms:polygon fid="1"><gml:boundedBy><gml:Box srsName="EPSG:4326"><gml:coordinates>-0.768746,47.003018 0.532597,47.925567</gml:coordinates></gml:Box></gml:boundedBy><ms:msGeometry><gml:MultiPolygon srsName="EPSG:4326"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>-0.318987,47.003018 -0.768746,47.358268 -0.574463,47.684285 -0.347374,47.854602 -0.006740,47.925567 0.135191,47.726864 0.149384,47.599127 0.419052,47.670092 0.532597,47.428810 0.305508,47.443003 0.475824,47.144948 0.064225,47.201721 -0.318987,47.003018 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs><gml:innerBoundaryIs><gml:LinearRing><gml:coordinates>-0.035126,47.485582 -0.035126,47.485582 -0.049319,47.641706 -0.233829,47.655899 -0.375760,47.457196 -0.276408,47.286879 -0.035126,47.485582 </gml:coordinates></gml:LinearRing></gml:innerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ms:msGeometry><ms:ogc_fid>1</ms:ogc_fid><ms:name>My Polygon with hole</ms:name><ms:id>0</ms:id></ms:polygon></gml:featureMember><gml:featureMember><ms:polygon fid="2"><gml:boundedBy><gml:Box srsName="EPSG:4326"><gml:coordinates>1.511919,47.088176 3.002191,47.882988</gml:coordinates></gml:Box></gml:boundedBy><ms:msGeometry><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>1.625463,47.357844 1.511919,47.741057 1.880938,47.882988 2.420275,47.797830 2.789295,47.485582 3.002191,47.457196 2.874453,47.088176 2.178993,47.343651 1.625463,47.357844 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ms:msGeometry><ms:ogc_fid>2</ms:ogc_fid><ms:name>My simple Polygon</ms:name><ms:id>0</ms:id></ms:polygon></gml:featureMember><gml:featureMember><ms:polygon fid="3"><gml:boundedBy><gml:Box srsName="EPSG:4326"><gml:coordinates>0.000000,45.000000 2.000000,47.000000</gml:coordinates></gml:Box></gml:boundedBy><ms:msGeometry><gml:MultiPolygon srsName="EPSG:4326"><gml:polygonMember><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>0.000000,45.000000 2.000000,45.000000 2.000000,47.000000 0.000000,47.000000 0.000000,45.000000 </gml:coordinates></gml:LinearRing></gml:outerBoundaryIs><gml:innerBoundaryIs><gml:LinearRing><gml:coordinates>0.500000,45.500000 1.500000,45.500000 1.500000,46.500000 0.500000,46.500000 0.500000,45.500000 </gml:coordinates></gml:LinearRing></gml:innerBoundaryIs></gml:Polygon></gml:polygonMember></gml:MultiPolygon></ms:msGeometry><ms:ogc_fid>3</ms:ogc_fid><ms:name>my polygon with hole</ms:name><ms:id>3</ms:id></ms:polygon></gml:featureMember></wfs:FeatureCollection></wps:ComplexData></wps:Data></wps:Input></wps:DataInputs><wps:ResponseForm><wps:RawDataOutput><ows:Identifier xmlns:ows="http://www.opengis.net/ows/1.1">result</ows:Identifier></wps:RawDataOutput></wps:ResponseForm></wps:Execute>'*/api.customs.xmlToString(wpsExResult), 
				/*'<wps:Execute xmlns:wps="http://www.opengis.net/wps/1.0.0" version="1.0.0" service="WPS" xsi:schemaLocation="http://www.opengis.net/wps/1.0.0 http://schemas.opengis.net/wps/1.0.0/wpsAll.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:wfs="http://www.opengis.net/wfs/1.1.0" xmlns:ows="http://www.opengis.net/ows/1.1" xmlns:gml="http://www.opengis.net/gml" xmlns:ogc="http://www.opengis.net/ogc" xmlns:wcs="http://www.opengis.net/wcs/1.1.1" xmlns:xlink="http://www.w3.org/1999/xlink"><ows:Identifier xmlns:ows="http://www.opengis.net/ows/1.1">gs:Count</ows:Identifier><wps:DataInputs><wps:Input><ows:Identifier xmlns:ows="http://www.opengis.net/ows/1.1">features</ows:Identifier><wps:Data><wps:ComplexData mimeType="text/xml; subtype=wfs-collection/1.1"><wfs:FeatureCollection xmlns:wfs="http://www.opengis.net/wfs" xsi:schemaLocation="http://www.opengis.net/wfs http://schemas.opengis.net/wfs/1.1.0/wfs.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><featureMembers xmlns="http://www.opengis.net/gml" xsi:schemaLocation="http://www.opengis.net/gml http://schemas.opengis.net/gml/3.1.1/profiles/gmlsfProfile/1.0.0/gmlsf.xsd"><wpsapp xmlns="http://localhost/OL3"><geometry><MultiPoint xmlns="http://www.opengis.net/gml"><pointMember><Point><pos>1947445.8788035866 6184656.847417211</pos></Point></pointMember><pointMember><Point><pos>1939076.0242063215 6189548.817227392</pos></Point></pointMember></MultiPoint></geometry><Madari>20</Madari><Slovaci>5</Slovaci></wpsapp></featureMembers></wfs:FeatureCollection></wps:ComplexData></wps:Data></wps:Input></wps:DataInputs><wps:ResponseForm><wps:RawDataOutput><ows:Identifier xmlns:ows="http://www.opengis.net/ows/1.1">result</ows:Identifier></wps:RawDataOutput></wps:ResponseForm></wps:Execute>',*/
				{"Content-type": "text/xml; charset=UTF-8;"}
				);
			}
				//} --endsection
			// var actualUri = appi.uriReturner(parseInt(form.name));
			// var a = new goog.net.CrossDomainRpc();
			// a.sendRequest(actualUri.proxySet().uri.toString(), "POST",customer.xmlToString(wpsExResult));
		   //alert((selectInteraction.getFeatures()));
			//} --endsection
			// console.timeEnd("Execute");
			return false; 
		}
		//} --endsection
		//} --ensection app
		// var process = wps.describeProcess({
			// proces:'gs:Bounds'
		 // }/*'vec:Query''vec:IntersectionFeatureCollection'*/);
		// //console.log(JSON.stringify(process, null, 4));
	//}
	//CALLING FUNCTIONS (SCRIPT)
	app.getCapabilities(app.divloader.querySelector('form.urlLoader'), "http://gis.fns.uniba.sk/geoserver/wps", "wps_ez", " - specialized");
	app.getCapabilities(app.divloader.querySelector('form.urlLoader'), "http://gis.fns.uniba.sk/geoserver/wps", "wps_vector", " - generic");
	app.getWFS();
    goog.exportSymbol('app.getCapabilities', app.getCapabilities);
/*	 
var test = app.wps.writeExecute({"objectExecute": {
Identifier: "JTS:buffer",
ComplexData: {
 geom:{
 MimeType: "text/xml; subtype=gml/3.1.1",
 value: app.map.getLayers().item(1).getLayers().item(0).getSource().getFeatures()
 }
},
LiteralData: {
 distance:{
 DataType: "xs:double",
 value: 1000
 },
 quadrantSegments:{
 DataType: "xs:int",
 value: 3
 },
 capStyle:{
 DataType: null,
 value: "none"
 }
},
ComplexOutput: {
 result:{MimeType: "text/xml; subtype=gml/3.1.1"},
 result2: {MimeType: "text/xml; subtype=gml/3.1.1"}
 },
LiteralOutput: {result:{uom: "text/xml; subtype=gml/3.1.1"}}
}, "ResponseDocument": true, "ResponseDocumentAttributes": {status: "true"}})
*/


// testing code

app.setInitialState = function(){
	var map = app.map;
	var layers = map.getLayers().getArray();
	var newview = new ol.View(app.WGSviewOpts);
	map.setView(newview);
	for(var i = 0, ii = app.successUris.length; i<ii; i++){
		var processConnector = app.successUris[i];	
		processConnector.setInitialState();
	}
	for(var i = 0, ii = layers.length; i<ii; i++){
		var layer = layers[i];
		if(layer instanceof ol.layer.Vector){
			var title = layer.get("title");
			if(title == 'Contaminated Sites'){
				layer.setStyle(app.points_style);
			}else{
				layer.setStyle(undefined);
			}
		}
	}
	app.EditTool.clearInteractions();
	app.EditTool.clearLayers();
}

goog.exportSymbol('app.setInitialState', app.setInitialState);